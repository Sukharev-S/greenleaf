const {src, dest, parallel, series, watch} = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify-es').default;
const del = require('del');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const rename = require('gulp-rename');
const fileinclude = require('gulp-file-include');
const gutil = require('gulp-util');
const ftp = require('vinyl-ftp');
const sourcemaps = require('gulp-sourcemaps');
const notify = require('gulp-notify');
const svgSprite = require('gulp-svg-sprite');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const ttf2woff = require('gulp-ttf2woff');
const ttf2woff2 = require('gulp-ttf2woff2');
const fs = require('fs');
const tiny = require('gulp-tinypng-compress');
const rev = require('gulp-rev');
const revRewrite = require('gulp-rev-rewrite');
const revdel = require('gulp-rev-delete-original');
const htmlmin = require('gulp-htmlmin');


const LOCAL_PATH = 'C:/OpenServer/domains/greenleaf'

// --------------------------  DEVELOPMENT

// сборка svg в единый спрайт
const svgSprites = () => {
  return src('./src/img/svg/**.svg')
    .pipe(svgSprite({
      mode: {
        stack: {
          sprite: "../sprite.svg" //sprite file name
        }
      },
    }))
    .pipe(dest(`${LOCAL_PATH}/img`))
      .pipe(browserSync.stream());
}

// --- Добавлено для работы с PHP -------------------------

// перенос из папки resources в корень папки app, для php
const resources = () => {
  return src('./src/resources/**')
    .pipe(dest(`${LOCAL_PATH}`))
      .pipe(browserSync.stream());
}
// переименование ключевых HTML файлов в PHP
// const htmlToPhp = () => {
//   return src('C:/OpenServer/domains/greenleaf/php/views/*.html')
//       // .pipe(dest('./app'))
//       // .pipe(dest('C:/OpenServer/domains/greenleaf'))
//       .pipe(rename({
//         extname: '.php'
//       }))
//       .pipe(dest('C:/OpenServer/domains/greenleaf/php/views'))
// }
// очистка директории сайта openServer
// const cleanPhp = () => {
//   return del(['C:/OpenServer/domains/greenleaf/php/views/*'],{force: true})
// }
// удаление собранных HTML
// const delHtml = () => {
//   return del(['C:/OpenServer/domains/greenleaf/php/views/*.html'], {force: true})
// }
// --------------------------------------------------------

// перенос всех изображений в папку './app/img'
const imgToApp = () => {
	return src(['./src/img/**.jpg', './src/img/**.png','./src/img/**.ico', './src/img/*/**.ico', './src/img/**.jpeg',
      './src/img/*.svg', './src/img/*/**.jpg', './src/img/*/**.png', './src/img/*/**.jpeg', './src/img/*/**.gif',
      './src/img/**.gif',])
    .pipe(dest(`${LOCAL_PATH}/img`))
        .pipe(browserSync.stream());
}

// перенос видео в папку './app/video'
const videoToApp = () => {
  return src(['./src/video/**.*', './src/video/*/**.*'])
      .pipe(dest(`${LOCAL_PATH}/video`))
      .pipe(browserSync.stream());
}

// сборка html файлов в один и перезапуск browserSync
// const htmlInclude = () => {
//   return src(['./src/*.html'])
//     .pipe(fileinclude({
//       prefix: '@',
//       basepath: '@file'
//     }))
//     .pipe(dest('C:/OpenServer/domains/greenleaf/php/views'))
//     // .pipe(browserSync.stream());
// }

// перенос всех шрифтов в папку './app/fonts/'
const fonts = () => {
	src('./src/fonts/**.ttf')
		.pipe(ttf2woff())
        .pipe(dest(`${LOCAL_PATH}/fonts/`))
	return src('./src/fonts/**.ttf')
		.pipe(ttf2woff2())
        .pipe(dest(`${LOCAL_PATH}/fonts/`))
        .pipe(browserSync.stream());
}

// маппинг толщины шрифтов по названию файла шрифта
const checkWeight = (fontname) => {
  let weight = 400;
  switch (true) {
    case /Thin/.test(fontname):
      weight = 100;
      break;
    case /ExtraLight/.test(fontname):
      weight = 200;
      break;
    case /Light/.test(fontname):
      weight = 300;
      break;
    case /Regular/.test(fontname):
      weight = 400;
      break;
    case /Medium/.test(fontname):
      weight = 500;
      break;
    case /SemiBold/.test(fontname):
      weight = 600;
      break;
    case /Semi/.test(fontname):
      weight = 600;
      break;
    case /Bold/.test(fontname):
      weight = 700;
      break;
    case /ExtraBold/.test(fontname):
      weight = 800;
      break;
    case /Heavy/.test(fontname):
      weight = 700;
      break;
    case /Black/.test(fontname):
      weight = 900;
      break;
    default:
      weight = 400;
  }
  return weight;
}

const cb = () => {}
let srcFonts = './src/scss/_fonts.scss';
let appFonts = `${LOCAL_PATH}/fonts/`;
const fontsStyle = (done) => {
  let file_content = fs.readFileSync(srcFonts);
  fs.writeFile(srcFonts, '', cb);
  fs.readdir(appFonts, function (err, items) {
    if (items) {
      let c_fontname;
      for (var i = 0; i < items.length; i++) {
				let fontname = items[i].split('.');
				fontname = fontname[0];
        let font = fontname.split('-')[0];
        let weight = checkWeight(fontname);

        if (c_fontname != fontname) {
          fs.appendFile(srcFonts, '@include font-face("' + font + '", "' + fontname + '", ' + weight +');\r\n', cb);
        }
        c_fontname = fontname;
      }
    }
  })
  done();
}

// sourcemaps, минификатор, перенос файлов стилей scss и перезапуск browserSync
const styles = () => {
  return src('./src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'expanded'
    }).on("error", notify.onError()))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(autoprefixer({
      cascade: false,
    }))
    .pipe(cleanCSS({
      level: 2
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(`${LOCAL_PATH}/css`))
      .pipe(browserSync.stream());
}

// сборщик бабль файлов js, sourcemaps, перенос файлов и перезапуск browserSync
const scripts = () => {
  return src('./src/js/main.js')
    .pipe(webpackStream(
      {
        mode: 'development',
        output: {
          filename: 'main.js',
        },
        module: {
          rules: [{
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            }
          }]
        },
      }
    ))
    .on('error', function (err) {
      console.error('WEBPACK ERROR', err);
      this.emit('end'); // Don't stop the rest of the task
    })
    .pipe(sourcemaps.init())
    .pipe(uglify().on("error", notify.onError()))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(`${LOCAL_PATH}/js`))
      .pipe(browserSync.stream());
}

// watchers всех файлов проекта
const watchFiles = () => {
  browserSync.init({
    proxy: "greenleaf/main.php",
    serveStatic: [{
      // dir: "/"
    }]
  });
  watch('./src/scss/**/*.scss', styles);
  watch('./src/js/**/*.js', scripts);
  // watch('./src/html/*.html', htmlInclude);
  // watch('./src/html/*.html', series(htmlInclude, htmlToPhp, delHtml));
  // watch('./src/*.html', series(htmlInclude, htmlToPhp, delHtml));
  watch('./src/resources/**', resources);
  watch('./src/img/**.jpg', imgToApp);
  watch('./src/img/**.jpeg', imgToApp);
  watch('./src/img/**.png', imgToApp);
  watch('./src/img/**.ico', imgToApp);
  watch('./src/video/**.*', videoToApp);
  watch('./src/img/svg/**.svg', svgSprites);
  watch('./src/fonts/**', fonts);
  watch('./src/fonts/**', fontsStyle);
}

const clean = () => {
	return del([`${LOCAL_PATH}/*`])
}

// exports.fileinclude = htmlInclude;
exports.styles = styles;
exports.scripts = scripts;
exports.watchFiles = watchFiles;
exports.fonts = fonts;
exports.fontsStyle = fontsStyle;
exports.resources = resources;

// exports.default = series(clean, parallel(htmlInclude, scripts, fonts, imgToApp, videoToApp, svgSprites),
//     fontsStyle, styles, watchFiles);  // вызывается командой gulp

exports.php = series(parallel(scripts, fonts, resources, imgToApp, videoToApp, svgSprites),
    fontsStyle, styles, watchFiles);  // вызывается командой php - для работы с php кодом

// --------------------------  BUILD

// компрессия изображения на сервисе tinypng (500 в месяц) и перенос в './app/img'
const tinypng = () => {
  return src(['./src/img/**.jpg', './src/img/**.png', './src/img/**.jpeg'])
    .pipe(tiny({
      key: '1jV7Yb8DPWXPqmmbn17vBYWyf36yd9N4',
      sigFile: './app/img/.tinypng-sigs',
      parallel: true,
      parallelMax: 50,
      log: true,
    }))
    .pipe(dest(`${LOCAL_PATH}/img`))
}

// тоже самое только без sourcemaps
const stylesBuild = () => {
  return src('./src/scss/**/*.scss')
    .pipe(sass({
      outputStyle: 'expanded'
    }).on("error", notify.onError()))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(autoprefixer({
      cascade: false,
    }))
    .pipe(cleanCSS({
      level: 2
    }))
    .pipe(dest(`${LOCAL_PATH}/css`))
}

// тоже самое только без sourcemaps
const scriptsBuild = () => {
  return src('./src/js/main.js')
    .pipe(webpackStream(
        {
          mode: 'development',
          output: {
            filename: 'main.js',
          },
          module: {
            rules: [{
              test: /\.m?js$/,
              exclude: /(node_modules|bower_components)/,
              use: {
                loader: 'babel-loader',
                options: {
                  presets: ['@babel/preset-env']
                }
              }
            }]
          },
        }))
      .on('error', function (err) {
        console.error('WEBPACK ERROR', err);
        this.emit('end'); // Don't stop the rest of the task
      })
    .pipe(uglify().on("error", notify.onError()))
    .pipe(dest(`${LOCAL_PATH}/js`))
}

// Добавление кэша к названию файлов для того чтобы в браузере исключить кэширование
const cache = () => {
  // return src(`${LOCAL_PATH}/**/*.{css,js,svg,png,jpg,jpeg,woff,woff2}`, {
  //   base: `${LOCAL_PATH}/`})
  return src(`${LOCAL_PATH}/**/*.{css,js}`, {
    base: `${LOCAL_PATH}/`})
    .pipe(rev())
    .pipe(revdel())
    .pipe(dest(`${LOCAL_PATH}/`))
    .pipe(rev.manifest('rev.json'))
    .pipe(dest(`${LOCAL_PATH}/`));
};

const rewritePHP = () => {
  const manifest = src(`${LOCAL_PATH}/rev.json`);

  return src(`${LOCAL_PATH}/**/**/**/**/**/*.php`)
    .pipe(revRewrite({manifest}))
    .pipe(dest(`${LOCAL_PATH}/`));
}

const rewriteCSS = () => {
  const manifest = src(`${LOCAL_PATH}/rev.json`)
  return src(`${LOCAL_PATH}/**/**/**/*.css`)
      .pipe(revRewrite({manifest}))
      .pipe(dest(LOCAL_PATH))
}

// минификация html файлов
// const htmlMinify = () => {
// 	return src('app/**/*.html')
// 		.pipe(htmlmin({
// 			collapseWhitespace: true
// 		}))
//         .pipe(dest('C:/OpenServer/domains/greenleaf/'));
// }

// exports.cache = series(cache, rewrite);
exports.cache = series(cache, rewriteCSS, rewritePHP)

exports.build = series(clean, parallel(scriptsBuild, fonts, resources, imgToApp, videoToApp, svgSprites),
    fontsStyle, stylesBuild); // вызывается командой build   (убран tinypng)


// --------------------------  DEPLOY

// отправка на хостинг
const deploy = () => {
  let conn = ftp.create({
    host: '127.0.0.1',
    user: 'localadmin',
    password: '12345',
    parallel: 10,
    log: gutil.log
  });

  let globs = [
    `${LOCAL_PATH}/**`,
  ];

  return src(globs, {
      base: LOCAL_PATH,
      buffer: false
    })
    .pipe(conn.newer('greenleaf')) // only upload newer files
    .pipe(conn.dest('greenleaf'));
}

exports.deploy = deploy; // вызывается командой deploy
