<?php
    session_start();
    include_once('init.php');

    $user = authGetUser();              //  Проверка на авторизацию и возврат данных юзера

//    $pageCanonical = HOST . BASE_URL;
    $uri = $_SERVER['REQUEST_URI'];
    $goodUrl = BASE_URL . 'main.php';

    if (strpos($uri, $goodUrl) === 0) {
        $controllerName = 'c_error_404';
    }
    else {
        $routes = include('routes.php');
        $url = $_GET['querysystemurl'] ?? '';

        $routerRes = parseUrl($url, $routes);
        $controllerName = $routerRes['controller'];
        define('URL_PARAMS', $routerRes['params']);

        $urlLen = strlen($url);

        if($urlLen > 0 && $url[$urlLen - 1] == '/') {
            $url = substr($url, 0, $urlLen - 1);
        }
//        $pageCanonical .= $url;
    }

    $path = "php/controllers/$controllerName.php";
//    $pageTitle = $pageContent = '';

    if(!file_exists($path)) {
        $controllerName = 'c_error_404';
        $path = "php/controllers/$controllerName.php";
    }
    include_once($path);
?>