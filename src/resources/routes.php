<?php

return (function() {        // конструкция с анонимной функцией, для сокрытия переменных в области видимости
    $intID = '[1-9]+\d*';
    $charParam = '[aA-zZ_-]+';
    $allCharParam = '[0-9aA-zZ_@.-]+';
    $normUrl = '[0-9aA-zZ_-]+';

    return [
        [
            'test' => "/^$/",
            'controller' => 'c_index'
        ],
        [
            'test' => "/^cart\/($charParam)\/?$/",
            'controller' => 'c_cart',
            'params' => ['tab' => 1]
        ],
        [   // для перехода на страницу профиля
            'test' => "/^profile\/?($charParam)?\/?$/",
            'controller' => 'c_user-profile',
            'params' => ['tab' => 1]
        ],
        [   // для перехода на страницу товара
            'test' => "/^product\/($intID)\/?$/",
            'controller' => 'c_card',
            'params' => ['id' => 1]
        ],
        [   // для перехода на страницу faq
            'test' => "/^faq\/?$/",
            'controller' => 'c_faq'
        ],
        [   // для перехода на страницу about
            'test' => "/^about\/?$/",
            'controller' => 'c_about'
        ],
        [   // для перехода на страницу company
            'test' => "/^company\/?$/",
            'controller' => 'c_company'
        ],
        [   // для перехода на страницу cooperation
            'test' => "/^cooperation\/?$/",
            'controller' => 'c_cooperation'
        ],
        [   // для перехода в нужную категории на страницу каталога с учетом выбранной подкатегории
            'test' => "/^catalog\/category\/($intID)\/?($charParam)?\/?$/",
            'controller' => 'c_catalog',
            'params' => ['id' => 1, 'subcategories' => 2]
        ],
        // выдает инфо товара по строке поиска (в модальном окне и в админке)
        [   // -- DB запрос только JSON ответ
            'test' => "/^search\/($charParam)\/?$/",
            'controller' => 'product/c_search',
            'params' => ['type' => 1]
        ],
        // выдает товар похожий на целевой товар (на странице товара)
        [   // -- DB запрос только JSON ответ
            'test' => "/^similar\/product\/($intID)\/?$/",
            'controller' => 'product/c_similar-product',
            'params' => ['id_product' => 1]
        ],
        // выдает товар в зависимости от выбранной вкладки групп товара (на главной странице)
        [   // -- DB запрос только JSON ответ
            'test' => "/^main-page\/($charParam)\/?$/",
            'controller' => 'product/c_main-page',
            'params' => ['group' => 1]
        ],
        // выдает товар по заданной подкатегории и фильтра сортировки (на странице каталога)
        [   // -- DB запрос только JSON ответ
            'test' => "/^category\/($intID)\/($charParam)\/sort\/($charParam)\/?$/",
            'controller' => 'product/c_sub-category',
            'params' => ['id_category' => 1, 'subcategories' => 2, 'sort' => 3]
        ],
        // выдает товар по заданной категории, а так же списка подкатегорий этой категории (на странице каталога)
        [   // -- DB запрос только JSON ответ
            'test' => "/^category\/($intID)\/?$/",
            'controller' => 'product/c_category',
            'params' => ['id' => 1]
        ],
        // авторизация пользователя
        [   // -- POST запрос
            'test' => "/^login\/?$/",
            'controller' => 'auth/c_login'
        ],
        // регистрация нового пользователя
        [   // -- POST запрос
            'test' => "/^registration\/?$/",
            'controller' => 'auth/c_registration'
        ],
        // восстановление пароля пользователя
        [   // -- POST и GET запрос
            'test' => "/^recover\/?$/",
            'controller' => 'auth/c_recover'
        ],
        // добавление/смена адреса пользователя
        [   // -- POST запрос
            'test' => "/^address-update\/?$/",
            'controller' => 'user/c_change-address'
        ],
        // смена пароля пользователя
        [   // -- POST запрос
            'test' => "/^change-password\/?$/",
            'controller' => 'user/c_change-password'
        ],
        // смена/добавление данных контакта пользователя
        [   // -- POST запрос
            'test' => "/^account-update\/?$/",
            'controller' => 'user/c_change-account'
        ],
        // форма обратной связи
        [   // -- POST запрос
            'test' => "/^captcha\/($charParam)\/?$/",
            'controller' => 'c_captcha',
            'params' => ['action' => 1]
        ],
        // админ панель редактирования данных БД где нужен id для работы с конкретным эелементом
        [   // -- POST и GET запрос
            'test' => "/^admin-panel\/($charParam)\/($charParam)\/($intID)\/?$/",
            'controller' => 'admin-panel/c_admin-main',
            'params' => ['option' => 1, 'item' => 2, 'id' => 3]
        ],
        // админ панель редактирования данных БД
        [   // -- POST и GET запрос
            'test' => "/^admin-panel\/($charParam)\/($charParam)\/?$/",
            'controller' => 'admin-panel/c_admin-main',
            'params' => ['option' => 1, 'item' => 2]
        ],
        // создание нового заказа
        [   // -- POST запрос
            'test' => "/^new-order\/?$/",
            'controller' => 'order/c_order'
        ],
        // стороннее API для получения цен службы доставки
        [   // -- POST запрос
            'test' => "/^delivery-price\/?$/",
            'controller' => 'order/c_delivery-price'
        ],
        // выход из аккаунта пользователя
        [   // -- GET запрос
            'test' => "/^logout\/?$/",
            'controller' => 'auth/c_logout'
        ],
        // активация учетной записи пользователя по почте
        [   // -- GET запрос
            'test' => "/^activation\/?$/",
            'controller' => 'auth/c_activation'
        ]
    ];
})();