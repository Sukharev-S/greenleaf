<?php

// -- Функция получения данных таблицы users на основе id пользователя
    function getUserById(string $id) : ?array {
        $sql = "SELECT id_user, login, email, phone, first_name, last_name, middle_name, 
                country, city, street, postcode, password, is_admin
                FROM users 
                WHERE id_user = :id";
        $query = dbQuery($sql, ['id' => $id]);
        $user = $query -> fetch();
        return $user === false ? null : $user;
    }

// -- Функция получения id пользователя по его eMail-у
    function getIdUserByEmail(string $email) : ?string {
        $sql = "SELECT id_user
                FROM users 
                WHERE email = :email";
        $query = dbQuery($sql, ['email' => $email]);
        $userId = $query -> fetch();
        return $userId === false ? null : $userId['id_user'];
    }

// -- Функция создание нового пользователя и возврат его id
    function addNewUser(string $email, string $login, string $password) : ?string {
        $sql = "INSERT users (email, login, password) VALUES (:email, :login, :password)";
        $params = ['email' => $email, 'login' => $login, 'password' => $password];
        dbQuery($sql, $params);
        $db = dbInstance();
        $lastIdUser = $db -> lastInsertId();
        return $lastIdUser === false ? null : $lastIdUser;
    }

// -- Функция изменения адреса у конкретного пользователя по id
    function updateUserAddress(string $id, string $country, string $city, string $street, int $postcode) : bool {
        $sql = "UPDATE users 
                SET country = :country,
                    city = :city,
                    street = :street,
                    postcode = :postcode
                WHERE id_user = :id";
        $params = ['id' => $id, 'country' => $country, 'city' => $city, 'street' => $street, 'postcode' => $postcode];
        dbQuery($sql, $params);
        return true;
    }

// -- Функция изменения пароля у конкретного пользователя по id
    function updateUserPassword(string $id, string $password) : bool {
    $sql = "UPDATE users 
            SET password = :password
            WHERE id_user = :id";
    $params = ['id' => $id, 'password' => $password];
    dbQuery($sql, $params);
    return true;
}

// -- Функция изменения данных аккаунты у конкретного пользователя по id
    function updateUserAccount(string $id, string $first_name, string $last_name, string $middle_name, string $phone) : bool {
    $sql = "UPDATE users 
            SET first_name = :first_name,
                last_name = :last_name,
                middle_name = :middle_name,
                phone = :phone
            WHERE id_user = :id";
    $params = ['id' => $id, 'first_name' => $first_name, 'last_name' => $last_name, 'middle_name' => $middle_name, 'phone' => $phone];
    dbQuery($sql, $params);
    return true;
}