<?php

    // Выдает информацию о выбранном html элементе (для дальнейших его изменений)
    function getHtmlElementInfo(string $element_name) : ?array {
        $sql = "SELECT id_element, element_text, element_class, element_description
                FROM html_elements
                WHERE element_name = :element_name";
        $query = dbQuery($sql, ['element_name' => $element_name]);
        $info = $query -> fetchAll();
        return $info[0] === false ? null : $info[0];
    }

    // Изменяет текст html элемента по названию этого элемента
    function updateHtmlElementText(string $id_element, string $element_text) : bool {
        $sql = "UPDATE html_elements 
                SET element_text = :element_text
                WHERE id_element = :id_element";
        $params = ['id_element' => $id_element, 'element_text' => $element_text];
        dbQuery($sql, $params);
        return true;
    }