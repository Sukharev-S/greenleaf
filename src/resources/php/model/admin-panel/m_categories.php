<?php

    // Выдает краткое описание всех категорий (для дальнейших изменений)
    function getShortDescCategories() : ?array {
        $sql = "SELECT categories.name as name, description_short as short, id_category as id
                FROM categories
                WHERE parent_id = 0";
        $query = dbQuery($sql);
        $info = $query -> fetchAll();
        return $info === false ? null : $info;
    }

    // Изменяет текст краткого описания конкретной категории
    function updateShortDescCategory(string $id_category, string $text) : bool {
        $sql = "UPDATE categories
                SET description_short = :text
                WHERE id_category = :id_category";
        $params = ['id_category' => $id_category, 'text' => $text];
        dbQuery($sql, $params);
        return true;
    }

    // Выдает краткое описание всех категорий (для дальнейших изменений)
    function getFullDescCategories() : ?array {
        $sql = "SELECT categories.name as name, description, id_category as id
                FROM categories
                WHERE parent_id = 0";
        $query = dbQuery($sql);
        $info = $query -> fetchAll();
        return $info === false ? null : $info;
    }

    // Изменяет текст краткого описания конкретной категории
    function updateFullDescCategory(string $id_category, string $text) : bool {
        $sql = "UPDATE categories
                SET description = :text
                WHERE id_category = :id_category";
        $params = ['id_category' => $id_category, 'text' => $text];
        dbQuery($sql, $params);
        return true;
    }

    // Выдает все категории для формирования полного списка списков категорий
    function getAllCategories() : ?array {
        $sql = "SELECT id_category, name, parent_id
                FROM categories";
        $query = dbQuery($sql);
        $categories = $query -> fetchAll();
        return $categories === false ? null : $categories;
    }

    // Выдает ID всех категорий причастных к продукту
    function getAllCategoriesByProduct(string $id_product) : ?array {
        $sql = "SELECT id_category
                FROM productsandcategories
                WHERE productsandcategories.id_product = :id";
        $params = ['id' => $id_product];
        $query = dbQuery($sql, $params);
        $categories = $query -> fetchAll();
        return $categories === false ? null : $categories;
    }