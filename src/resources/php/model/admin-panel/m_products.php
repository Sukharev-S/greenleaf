<?php

    // Выдает полную информацию обо всех продуктах
    function getAllProductsInfo() : ?array {
    // запрос на основную выборку по товару
        $sql = "SELECT products.id_product AS id, 
                   products.article as article,
                   products.name AS title,
                   products.price,
                   products.old_price,
                   products.weight as weight,
                   products.volume as volume,
                   products.height as height,
                   products.width as width,
                   products.depth as depth,
                   products.shelf_life as life,
                   products.quantity_stock as stock,
                   products.quantity_ordered as quantity_ordered,
                   products.date_delivery as date_delivery,
                   products.in_package as package,
                   products.description as description,
                   products.description_short as short,
                   products.scope as scope,
                   products.recommendation as recommendation,
                   products.structure as structure,
                   products.search_words as search_words,       
                   products.is_popular as is_popular,
                   products.is_new as is_new,
                   products.is_sale as is_sale,
                   products.index_popular as index_popular,
                   products.is_active as is_active,
                   products.main_image AS main_image
                FROM products
                LIMIT 6";
        $query = dbQuery($sql);
        $products = $query -> fetchAll();
        return $products === false ? null : $products;
    }

    // Выдает полную информацию об одном продукте
    function getOneProductInfo(string $id) : ?array {
        $sql = "SELECT products.id_product AS id, 
                   products.article as article,
                   products.name AS title,
                   products.price,
                   products.old_price,
                   products.weight as weight,
                   products.volume as volume,
                   products.height as height,
                   products.width as width,
                   products.depth as depth,
                   products.shelf_life as life,
                   products.quantity_stock as stock,
                   products.quantity_ordered as quantity_ordered,
                   products.date_delivery as date_delivery,
                   products.in_package as package,
                   products.description as description,
                   products.description_short as short,
                   products.scope as scope,
                   products.recommendation as recommendation,
                   products.structure as structure,
                   products.search_words as search_words,       
                   products.is_popular as is_popular,
                   products.is_new as is_new,
                   products.is_sale as is_sale,
                   products.index_popular as index_popular,
                   products.is_active as is_active,
                   products.main_image AS main_image
                FROM products
                WHERE products.id_product = :id";
        $params = ['id' => $id];
        $query = dbQuery($sql, $params);
        $product = $query -> fetch();
        return $product === false ? null : $product;
    }

    // Выдает цену продукта по его id
    function getProductPrice(string $id) : ?int {
    $sql = "SELECT products.price
            FROM products
            WHERE products.id_product = :id";
    $params = ['id' => $id];
    $query = dbQuery($sql, $params);
    $price = $query -> fetch();
    $price = intval($price['price']);
    return $price === false ? null : $price;
    }

    // Ищет подходящие продукты по запросу поисковой строки
    function getAllProductsFound(string $word) : ?array {
        $sql = "SELECT products.id_product AS id_product, 
                   products.article as article,
                   products.name AS name,
                   products.price,
                   products.old_price,
                   products.description_short as short,      
                   products.is_popular as is_popular,
                   products.is_new as is_new,
                   products.is_sale as is_sale,
                   products.index_popular as index_popular,
                   products.main_image AS main_image
                FROM products
                WHERE products.name LIKE CONCAT('%', :word, '%') OR products.article LIKE CONCAT('%', :word, '%')";
        $params = ['word' => $word];
        $query = dbQuery($sql, $params);
        $info = $query -> fetchAll();
        return $info === false ? null : $info;
    }

    // Изменяет характеристики товара
    function updateOneProduct(array $paramsPost) : bool {
        $sql = "UPDATE products
                SET article = :article,
                    date_delivery = :date_delivery,
                    depth = :depth,
                    description = :description,
                    height = :height,
                    index_popular = :index_popular,
                    is_active = :is_active,
                    is_new = :is_new,
                    is_popular = :is_popular,
                    is_sale = :is_sale,
                    shelf_life = :life,
                    name = :name,
                    old_price = :old_price,
                    in_package = :package,
                    price = :price,
                    quantity_ordered = :quantity_ordered,
                    recommendation = :recommendation,
                    scope = :scope,
                    description_short = :short,
                    quantity_stock = :stock,
                    structure = :structure,
                    volume = :volume,
                    weight = :weight,
                    width = :width
                WHERE id_product = :id_product";
        $params = ['id_product', 'article', 'date_delivery', 'depth', 'description', 'height', 'index_popular',
            'is_active', 'is_new', 'is_popular', 'is_sale', 'life', 'name', 'old_price', 'package','price',
            'quantity_ordered', 'recommendation', 'scope', 'short', 'stock', 'structure', 'volume', 'weight', 'width'];
        $temp_arr = array_combine($params, $paramsPost);
        $counter = 0;
        // если значения нет, то для числовых полей надо передать null
        foreach ($temp_arr as $val) {
            if ($val === '') {
                $sql = preg_replace('/:' . $params[$counter] . '/', 'null', $sql);
                unset($temp_arr[$params[$counter]]);
            }
            $counter++;
        }
        dbQuery($sql, $temp_arr);
        return true;
    }

    // Добавляет новый товар
    function addOneProduct(array $paramsPost) : ?string {
        $sql = "INSERT INTO products (article, date_delivery, depth, description, height, index_popular, is_active, is_new,
                          is_popular, is_sale, shelf_life, name, old_price, in_package, price, quantity_ordered, recommendation,
                          scope, description_short, quantity_stock, structure, volume, weight, width)
                    VALUES (:article, :date_delivery, :depth, :description, :height, :index_popular, :is_active, :is_new, 
                            :is_popular, :is_sale, :life, :name, :old_price, :package, :price, :quantity_ordered, :recommendation, 
                            :scope, :short, :stock, :structure, :volume, :weight, :width)";
        $params = ['article', 'date_delivery', 'depth', 'description', 'height', 'index_popular',
            'is_active', 'is_new', 'is_popular', 'is_sale', 'life', 'name', 'old_price', 'package','price',
            'quantity_ordered', 'recommendation', 'scope', 'short', 'stock', 'structure', 'volume', 'weight', 'width'];
        $temp_arr = array_combine($params, $paramsPost); // создает новый массив, где $params как ключи, массив $paramsPost как значения
        $counter = 0;
        // если значения нет, то для числовых полей надо передать null
        foreach ($temp_arr as $val) {
            if ($val === '') {
                $sql = preg_replace('/:' . $params[$counter] . '/', 'null', $sql);
                unset($temp_arr[$params[$counter]]);
            }
            $counter++;
        }
        dbQuery($sql, $temp_arr);
        $db = dbInstance();
        $lastId = $db -> lastInsertId();
        return  $lastId === false ? null : $lastId;
    }

    // Удаление всех категорий у данного продукта
    function deleteCategoriesFromProduct (string $id_product) : bool {
        $sql = "DELETE FROM productsandcategories
                WHERE id_product = :id";
        $params = ['id' => $id_product];
        dbQuery($sql, $params);
        return true;
    }

    // Добавляет одну категорию данному продкуту
    function addCategoryToProduct (string $id_product, string $id_category) : bool {
        $sql = "INSERT INTO productsandcategories (id_product, id_category)
                VALUES (:id_product, :id_category)";
        $params = ['id_product' => $id_product, 'id_category' => $id_category];
        dbQuery($sql, $params);
        return true;
    }

    // Выдает похожие продукты целевому продукту, полная инфа без учета is_active
    function getAllSimilarProducts(string $id_product) : ?array {
        // запрос на выборку похожих продуктов
        $sql_similar = "SELECT products.id_product, products.article, products.main_image, products.name
                        FROM (
                            SELECT DISTINCT product_similar.id_product_similar AS similar, product_similar.id_product
                            FROM products, product_similar
                            WHERE products.id_product = product_similar.id_product AND products.id_product = :id
                        ) AS a 
                        INNER JOIN products ON products.id_product = a.similar";
        $query = dbQuery($sql_similar, ['id' => $id_product]);
        $similar = $query -> fetchAll();
        return $similar === false ? null : $similar;
    }

    // Удаление конкретного похожего товара у данного продукта
    function deleteSimilarFromProduct (string $id_product) : bool {
        $sql = "DELETE FROM product_similar
                WHERE id_product = :id_product";
        $params = ['id_product' => $id_product];
        dbQuery($sql, $params);
        return true;
    }

    // Добавляет один похожий товар данному продкуту
    function addSimilarToProduct (string $id_product, string $id_similar) : bool {
        $sql = "INSERT INTO product_similar (id_product, id_product_similar)
                VALUES (:id_product, :id_similar)";
        $params = ['id_product' => $id_product, 'id_similar' => $id_similar];
        dbQuery($sql, $params);
        return true;
    }