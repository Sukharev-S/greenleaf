<?php

// Выдает всю информацию о выбранном вопросе и ответе по его ID
function getAllFaqInfo() : ?array {
    $sql = "SELECT id_faq, location_number, question, answer
            FROM faq
            ORDER BY location_number ASC";
    $query = dbQuery($sql);
    $info = $query -> fetchAll();
    return $info === false ? null : $info;
}