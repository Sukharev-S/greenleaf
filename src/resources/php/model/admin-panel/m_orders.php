<?php

// Ищет подходящие ордера по запросу поисковой строки
    function getAllOrdersFound(string $word) : ?array {
        $sql = "SELECT id_order, date_placed, sum_total_rub, method_delivery, sum_delivery, orders.id_status, orders.is_deleted,
           statuses.status_name, users.first_name, users.middle_name, users.last_name, users.phone, users.email, users.date_reg, 
           users.country, users.city, users.street, users.postcode, user_info
                FROM orders
                INNER JOIN statuses ON statuses.id_status = orders.id_status
                LEFT JOIN users ON orders.id_user = users.id_user
                WHERE id_order LIKE CONCAT('%', :word, '%') OR 
                      users.last_name LIKE CONCAT('%', :word, '%')
                ORDER BY id_order";
        $params = ['word' => $word];
        $query = dbQuery($sql, $params);
        $info = $query -> fetchAll();
        return $info === false ? null : $info;
    }

// Выдает список статусов ордера
    function getOrderStatusList() : ?array {
        $sql = "SELECT id_status, status_name
                FROM statuses
                ORDER BY id_status";
        $query = dbQuery($sql);
        $statuses = $query -> fetchAll();
        return $statuses === false ? null : $statuses;
    }

// Меняет статус ордеру по ID ордера
    function updateStatusOrder(string $id_order, string $id_status) : bool {
        $sql = "UPDATE orders
                SET id_status = :id_status
                WHERE id_order = :id_order";
        $params = ['id_order' => $id_order, 'id_status' => $id_status];
        dbQuery($sql, $params);
        return true;
    }