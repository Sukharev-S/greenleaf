<?php

// Добавляет товар в заказ (добавление в таблицу ordersAndProducts)
    function addProductToOrder(string $id_order, string $id_product, string $quantity, string $price, string $sum_rub) : bool {
        $sql = "INSERT INTO ordersandproducts (id_order, id_product, quantity, price, sum_rub)
               VALUES (:id_order, :id_product, :quantity, :price, :sum_rub)";
        $params = [
            'id_order' => $id_order,
            'id_product' => $id_product,
            'quantity' => $quantity,
            'price' => $price,
            'sum_rub' => $sum_rub,
        ];
        dbQuery($sql, $params);
        return true;
    }

// Добавляет новый заказ (добавление в таблицу order)
    function addOrder(?string $id_user,
                      string $sum_total_rub,
                      string $sum_delivery,
                      string $method_delivery,
                      ?string $user_info,
                      ?string $notesAdmin)  : ?string {
        $sql = "INSERT INTO orders (id_user, sum_total_rub, sum_delivery, method_delivery, user_info, notes_admin)
               VALUES (:id_user, :sum_total_rub, :sum_delivery, :method_delivery, :user_info, :notes_admin)";
        $params = [
            'id_user' => $id_user,
            'sum_total_rub' => $sum_total_rub,
            'sum_delivery' => $sum_delivery,
            'method_delivery' => $method_delivery,
            'user_info' => $user_info,
            'notes_admin' => $notesAdmin,
        ];
        dbQuery($sql, $params);
        $db = dbInstance();
        $lastId = $db -> lastInsertId();
        return  $lastId === false ? null : $lastId;
    }

// Выдает timestamp созданного заказа по его id
    function getDatePlacedOrder(string $id) : ?string {
        $sql = "SELECT orders.date_placed
                FROM orders
                WHERE orders.id_order = :id";
        $params = ['id' => $id];
        $query = dbQuery($sql, $params);
        $date = $query -> fetch();
        return $date['date_placed'] === false ? null : $date['date_placed'];
    }

// Выдает все заказы пользователя по его id
    function getOrdersByUser(string $id_user) : ?array {
        $sql = "SELECT id_order, date_placed, sum_total_rub, method_delivery, sum_delivery,
                       orders.id_status, statuses.status_name, orders.user_info
                FROM orders, statuses
                WHERE orders.id_user = :id and statuses.id_status = orders.id_status";
        $query = dbQuery($sql, ['id' => $id_user]);
        $orders = $query -> fetchAll();
        return $orders === false ? null : $orders;
    }

// Выдает все товары данного заказа по его id
    function getProductsByOrder(string $id_order) : ?array {
        $sql = "SELECT ordersandproducts.id_product, products.name, ordersandproducts.quantity, ordersandproducts.price, ordersandproducts.sum_rub
                FROM ordersandproducts, products
                WHERE ordersandproducts.id_order = :id and products.id_product = ordersandproducts.id_product";
        $query = dbQuery($sql, ['id' => $id_order]);
        $products = $query -> fetchAll();
        return $products === false ? null : $products;
    }

// -- Выдает данные заказа по его id
function getOrderInfo(string $id) : ?array {
    $sql = "SELECT  id_user, date_placed, sum_total_rub, method_delivery, sum_delivery, 
                    orders.id_status, statuses.status_name, orders.user_info
                FROM orders, statuses
                WHERE id_order = :id";
    $query = dbQuery($sql, ['id' => $id]);
    $orderInfo = $query -> fetch();
    return $orderInfo === false ? null : $orderInfo;
}