<?php

function addNewSession(int $idUser, string $token) : bool {
    $sql = "INSERT sessions (id_user, token) VALUES (:uid, :token)";
    $params = ['uid' => $idUser, 'token' => $token];
    dbQuery($sql, $params);
    return true;
}

function getSessionByToken(string $token) : ?array {
    $sql = "SELECT * FROM sessions WHERE token=:token";
    $query = dbQuery($sql, ['token' => $token]);
    $session = $query -> fetch();
    return $session === false ? null : $session;
}