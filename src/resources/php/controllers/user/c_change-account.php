<?php

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $first_name = cleaner($_POST['first_name']);
        $last_name = cleaner($_POST['last_name']);
        $middle_name = cleaner($_POST['middle_name']);
        $phone = cleaner($_POST['phone']);

        if (!empty($first_name) && !empty($last_name) && !empty($middle_name) && !empty($phone)) {
            if (validateFirstName($first_name)) {
                if (validateLastName($last_name)) {
                    if (validateMiddleName($middle_name)) {
                        if (validatePhone($phone)) {

                            if (updateUserAccount($user['id_user'], $first_name, $last_name, $middle_name, $phone)) {
                                $responseMessage = ['is_ok' => 1, 'message' => 'Данные учетной записи успешно сохранены'];
                            } else $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка изменения данных учетной записи'];
                            $responseMessage['dataInfo'] = $_POST;

                        } else $responseMessage = ['is_ok' => 0, 'message' => 'Телефон указан некорректно'];
                    } else $responseMessage = ['is_ok' => 0, 'message' => 'Отчество указано некорректно'];
                } else $responseMessage = ['is_ok' => 0, 'message' => 'Фамилия указана некорректно'];
            } else $responseMessage = ['is_ok' => 0, 'message' => 'Имя указано некорректно'];
        } else $responseMessage = ['is_ok' => 0, 'message' => 'Заполните все поля формы'];

        echo json_encode($responseMessage);
    }
    exit();