<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $responseMessage = ['is_ok' => 0, 'message' => 'Данные формы не верны!'];

    $methodDelivery = cleaner($_POST['method_delivery']);
    $sumDelivery = cleaner($_POST['sum_delivery']);
    $cartItems = json_decode($_POST['cart_items']);

    $email = cleaner($_POST['email']);
    $phone = cleaner($_POST['phone']);
    $fullName = cleaner($_POST['full_name']);
    $firstName = cleaner($_POST['first_name']);
    $lastName = cleaner($_POST['last_name']);
    $middleName = cleaner($_POST['middle_name']);
    $country = cleaner($_POST['country']);
    $city = cleaner($_POST['city']);
    $street = cleaner($_POST['street']);
    $postcode = cleaner($_POST['postcode']);
    $notesAdmin = validateNotes(cleaner($_POST['notes_admin']));

    $productsInfo = [];             // массив данных для заполнения таблицы ordersAndProducts
    $sum_total_rub = 0;             // общая сумма ордера, вместе с доставкой
    $lastIdOrder = '';              // номер последнего созданного заказа
    $user_info = null;              // массив данных анонимного пользователя

    // если у пользователя не заполнены данные на постоянной основе, то заполняем после первого заказа их
    $accountInfo = doesAccountInfoExist($user);                 // проверяем заполнены ли перс. данные у пользователя
    $addressInfo = doesAddressExist($user);                     // проверяем заполнен ли адрес у пользователя

    if ($methodDelivery === 'pickup') {                         // если пользователь выбрал самовывоз
        if (is_null($user)) {                                   // если пользователь аноним
            if (validateFullName($fullName) && validateEmail($email) && validatePhone($phone)) {
                $user_info = json_encode([
                    'full_name' => $fullName,
                    'email' => $email,
                    'phone' => $phone
                ]);
            } else exit();
        } else {                                                // если пользователь авторизован
            if (validateFirstName($firstName) && validateLastName($lastName) && validateMiddleName($middleName)
                && validateEmail($email) && validatePhone($phone)) {
                if ($accountInfo) {                             // если у пользователя заполнены данные аккаунта
                    if (($user['first_name'] !== $firstName)
                        || ($user['last_name'] !== $lastName)
                        || ($user['middle_name'] !== $middleName)
                        || ($user['email'] !== $email)
                        || ($user['phone']) !== $phone) {       // если данные из формы не совпадают с уже имеющимися
                        $user_info = json_encode([
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'middleName' => $middleName,
                            'email' => $email,
                            'phone' => $phone
                        ]);
                    }
                } else {                                        // если данные аккаунта не заполнены полностью
                    updateUserAccount($user['id_user'], $firstName, $lastName, $middleName, $phone);
                    if ($user['email'] !== $email) {            // если введенный email отличен от создавший акк
                        $user_info = json_encode([
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'middleName' => $middleName,
                            'email' => $email,
                            'phone' => $phone
                        ]);
                    }
                }
            } else exit();
        }
    } elseif ($methodDelivery === 'delivery') {                 // если пользователь выбрал доставку
        if (validateFirstName($firstName) && validateLastName($lastName) && validateMiddleName($middleName)
            && validateEmail($email) && validatePhone($phone) && validateCountry($country)
            && validateCity($city) && validateStreet($street) && validatePostcode($postcode)) {
            if (is_null($user)) {                               // если пользователь аноним
                $user_info = json_encode([
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'middleName' => $middleName,
                    'email' => $email,
                    'phone' => $phone,
                    'country' => $country,
                    'city' => $city,
                    'street' => $street,
                    'postcode' => $postcode
                ]);
            } else {                                            // если пользователь авторизован
                if (!$accountInfo) {                            // если данные аккаунта неполные
                    updateUserAccount($user['id_user'], $firstName, $lastName, $middleName, $phone);
                }
                if (!$addressInfo) {                            // если данные адреса неполные
                    updateUserAddress($user['id_user'], $country, $city, $street, $postcode);
                }
                $user = getUserById($user['id_user']);          // обновляем данные по юзеру
                if (($user['first_name'] !== $firstName)
                    || ($user['last_name'] !== $lastName)
                    || ($user['middle_name'] !== $middleName)
                    || ($user['email'] !== $email)
                    || ($user['phone']) !== $phone
                    || ($user['country'] !== $country)
                    || ($user['city'] !== $city)
                    || ($user['street'] !== $street)
                    || ($user['postcode'] !== $postcode)) {     // если данные из формы не совпадают с уже имеющимися
                    $user_info = json_encode([
                        'firstName' => $firstName,
                        'lastName' => $lastName,
                        'middleName' => $middleName,
                        'email' => $email,
                        'phone' => $phone,
                        'country' => $country,
                        'city' => $city,
                        'street' => $street,
                        'postcode' => $postcode
                    ]);
                }
            }
        } else exit();
    } else {
        $responseMessage = ['is_ok' => 0, 'message' => 'Не верно указан метод доставки!'];
        echo json_encode($responseMessage);
        exit();
    }

// переборка входящего списка товаров в нужные для таблицы значения
    if (!empty($cartItems) && is_array($cartItems)) {
        foreach ($cartItems as $product) {
            $productPrice = getProductPrice(strval($product -> id));
            $sum_rub = $productPrice * $product -> quantity;
            $sum_total_rub += $sum_rub;
            array_push($productsInfo, [
                'id' => $product -> id,
                'title' => $product -> title,
                'quantity' => $product -> quantity,
                'price' => $productPrice,
                'sum_rub' => $sum_rub
            ]);
        }
    } else {
        $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка в данных товара!'];
        echo json_encode($responseMessage);
        exit();
    }
    $sum_total_rub += intval($sumDelivery);

// создание нового заказа на основе входящих данных
    $lastIdOrder = addOrder($user['id_user'], $sum_total_rub, strval($sumDelivery), $methodDelivery, $user_info, $notesAdmin);
    foreach ($productsInfo as $productInfo) {       // заполнение заказа продуктами
        $id_product = $productInfo['id'];
        $quantity = $productInfo['quantity'];
        $price = $productInfo['price'];
        $sum_rub = $productInfo['sum_rub'];
        addProductToOrder($lastIdOrder, $id_product, $quantity, $price, $sum_rub);
    }

// подготовка данных для ответа
    $date_placed_full = getDatePlacedOrder($lastIdOrder);
    $date_placed = preg_split('/\s+/', $date_placed_full)[0];
    $deliveryInfo = $methodDelivery === 'delivery'
        ? ['country' => $country, 'city' => $city,'street' => $street, 'postcode' => $postcode]
        : [];
    $orderInfo = [
        'id_order' => $lastIdOrder,
        'date_placed' => $date_placed,
        'sum_total_rub' => $sum_total_rub,
        'method_delivery' => $methodDelivery,
        'sum_delivery' => $sumDelivery,
        'products_info' => $productsInfo,
        'delivery_info' => $deliveryInfo
    ];

// сам готовый ответ
    $responseMessage = ['is_ok' => 1, 'order_info' => $orderInfo];
    echo json_encode($responseMessage);

// подготовка данных для отправки почты
    $userName = (is_null($user) && ($methodDelivery === 'pickup')) ? $fullName : $lastName.' '.$firstName.' '.$middleName;
    $productsInfoStringAdmin = '';
    $productsInfoStringClient = '';
    $deliveryHTMLString = '';
    $methodDeliveryRus = $methodDelivery === 'pickup' ? 'самовывоз' : 'почтовая служба';
    if ($methodDelivery === 'delivery') {            // заполнение строки адреса клиента при доставке
        $deliveryHTMLString = "
                <div><strong>Доставка по адресу:</strong></div>
                <div style='margin-left: 10px'>Страна: <em><strong>$country</strong></em></div>
                <div style='margin-left: 10px'>Населенный пункт: <em><strong>$city</strong></em></div>
                <div style='margin-left: 10px'>Улица, дом, кв.: <em><strong>$street</strong></em></div>
                <div style='margin-left: 10px'>ИНДЕКС: <em><strong>$postcode</strong></em></div>
            ";
    }
    foreach ($productsInfo as $productInfo) {       // заполнение заказа продуктами
        $id_product = $productInfo['id'];
        $productName = $productInfo['title'];
        $quantity = $productInfo['quantity'];
        $sum_rub = $productInfo['sum_rub'];
        $productsInfoStringAdmin = $productsInfoStringAdmin.
            "<li><a href='https://гринлиф-экотовары.рф/product/$id_product' 
                    style='text-transform: uppercase; font-size: 12px;'>$productName</a>___$quantity шт.</li>";
        $productsInfoStringClient = $productsInfoStringClient.
            "<li><a href='https://гринлиф-экотовары.рф/product/$id_product' 
                    style='text-transform: uppercase; font-size: 12px;'>$productName</a>___$quantity шт.___$sum_rub.00 руб.</li>";
    }

// отправка почты клиенту
    include('php/controllers/mail/order-created-client-mail.php');

// отправка почты администратору
    include('php/controllers/mail/order-created-admin-mail.php');

} else echo "Некорректный метод запроса, должен быть POST!";

exit();
