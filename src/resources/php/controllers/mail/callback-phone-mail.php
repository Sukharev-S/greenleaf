<?php
// --- Письмо для администратора сайта о заказе звонка

$mail -> ClearAddresses();

// -- Кому отправить
$mail -> addAddress(EMAIL_ADMIN);

// -- Тема письма
$mail -> Subject = "Клиент просит перезвонить!";

// --- Тело письма
$body = "   <h2>Поступила заявка на звонок.</h2>
            <div><strong>Дата поступления заявки:</strong> ".$timeIsNow."</div>
            <div><strong>Клиент:</strong> ".$fullName."</div>
            <div><strong>Телефон:</strong> ".$phone."</div>";

$mail -> Body = $body;

if (!$mail -> send()) {
    $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка отправки почты!'];
} else {
    $responseMessage = ['is_ok' => 1, 'message' => 'Почта отправлена успешно!'];
}