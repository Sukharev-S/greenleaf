<?php
// --- Письмо для клиента сайта об изменении статуса заказа

// -- Кому отправить
$mail -> addAddress($userInfoForEmail['email']);

// -- Тема письма
$mail -> Subject = "Заказу №_".$id_order." присвоен статус ".$orderInfo['status_name'];

// --- Тело письма
$body = "
            <h2>Здравствуйте!</h2>
            <p>
                <strong>Заказ №_".$id_order."</strong> созданный ".$orderInfo['date_placed'].", изменил свой статус на 
                <strong style='text-transform: uppercase'>".$orderInfo['status_name']."</strong>.</p>
            <div>Состав заказа:
                <div>Клиент: ".$userName."</div>
                <div>Метод доставки: ".$methodDeliveryRus."</div>
                <div>Заказанный товар:</div>
                <ul>
                    ".$productsInfoStringClient."
                </ul>
                <div>Доставка: ".$orderInfo['sum_delivery']." руб.</div>
                
                ".$deliveryHTMLString."
                <div style='margin-top: 10px'>
                    <strong>Общая сумма(с учетом доставки): </strong>  ".$orderInfo['sum_total_rub']." руб.
                </div>
            </div>
            
            <div style='margin-top: 40px'>Это письмо сгенерировано автоматически, 
                если хотите с нами связаться, то вот наши контакты:</div>
             <div><em>Телефон:</em> ".PHONE_ADMIN."</div>
             <div><em>Почта:</em> ".EMAIL_ADMIN."</div>
            ";

$mail -> Body = $body;

// Отправляем
if (!$mail -> send()) {
    $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка отправки почты!'];
} else {
    $responseMessage = ['is_ok' => 1, 'message' => 'Почта отправлена успешно!'];
}
