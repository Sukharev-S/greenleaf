<?php

    // -- Кому отправить
    $mail -> addAddress($email);

    // -- Тема письма
    $mail -> Subject = 'Верификация почтового ящика';

    // --- Тело письма
    $body = "<h1>Верификация почтового ящика!</h1>
             <span>Добро пожаловать в наш онлайн магазин GreenLeaf, пожалуйста, подтвердите свой адрес электронной почты
             перейдя по <a href='".HOST."/activation?hash=".$hash."&email=".$email."'>ссылке</a>.</span>
             
             <div style='margin-top: 40px'>Это письмо сгенерировано автоматически, 
                если хотите с нами связаться, то вот наши контакты:</div>
             <div><em>Телефон:</em> ".PHONE_ADMIN."</div>
             <div><em>Почта:</em> ".EMAIL_ADMIN."</div>";

    $mail -> Body = $body;

    // Отправляем письмо, если ошибка отправки, то правим сообщение на ошибочное
    if (!($mail -> send())) $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка отправки верификационного письма'];