<?php
// --- Письмо для администратора сайта о новом заказе

$mail -> ClearAddresses();

// -- Кому отправить
$mail -> addAddress(EMAIL_ADMIN);

// -- Тема письма
$mail -> Subject = "Поступил новый заказ №_".$lastIdOrder." !";

// --- Тело письма
$body = "
            <h2>Заказ №_".$lastIdOrder.":</h2>
            <div><strong>Заказ сделан:</strong> ".$date_placed_full."</div>
            <div><strong>Клиент:</strong> ".$userName."</div>
            <div><strong>Телефон:</strong> ".$phone."</div>
            <div><strong>eMail:</strong> ".$email."</div>
            <div><strong>Метод доставки:</strong> ".$methodDeliveryRus."</div>
            ".$deliveryHTMLString."
            <div><strong>Заказанный товар:</strong></div>
            <ul>
                ".$productsInfoStringAdmin."
            </ul>
            <div><strong>Общая сумма(с учетом доставки): </strong>  ".$sum_total_rub.".00 руб.</div>
            ";

$mail -> Body = $body;
if (!$mail -> send()) {
    $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка отправки почты!'];
} else {
    $responseMessage = ['is_ok' => 1, 'message' => 'Почта отправлена успешно!'];
}