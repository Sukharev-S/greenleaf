<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $email = cleaner($_GET['email']);
    $hash = cleaner($_GET['hash']);
    if (isset($_GET['email']) && isset($_GET['hash']) && validateEmail($email)) {
        if (is_string($email) && is_string($hash)) {
            clearFailHashAndUser();
            $userId = getUserByVerifyHash($email, $hash);     // если hash не истек по времени, то возвращает id юзера
            if (isset($userId)) {
                isVerifiedUserUpdate($email);
                $token = substr(bin2hex(random_bytes(128)), 0, 128);
                addNewSession($userId, $token);
                $_SESSION['token'] = $token;
                //        echo "Учетная запись успешно активирована";
                deleteVerifyHashAfterActivation($hash);
                header("Location: /profile/account");
                exit();
            } else echo "К сожалению срок подтверждения Email адреса истек. Пожалуйста, пройдите регистрацию заново";
        } else echo "Некорректные входные параметры!";
    } else echo "Некорректные входные параметры!";
}