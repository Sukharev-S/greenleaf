<?php

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $email = cleaner($_POST['email']);
        $password = $_POST['password'];

        if (!empty($email) && !empty($password)) {
            if (validateEmail($email)) {
                if (validatePassword($password)) {

                    if (isEmailAlreadyExist($email)) {
                        $responseMessage = ['is_ok' => 0, 'message' => 'Такой аккаунт уже существует, введите другой email адрес'];
                    } else {
                        $passwordHash = password_hash($password, PASSWORD_BCRYPT); // хэширование пароля
                        $lastIdUser = addNewUser($email, $email, $passwordHash);     // добавление нового юзера и сразу получение его id
                        $hash = md5($email . bin2hex(random_bytes(50)));  // генерация хэша для верификации почты
                        addNewVerifyHash($lastIdUser, $hash);                 // добавление хэша в БД под id нового юзера

                        $urlMail = substr($email, strripos($email, "@"));   // вырезание адреса почтового сервиса
                        $responseMessage = ['is_ok' => 1, 'message' => $urlMail]; // на перед присваиваем успех с инф. адреса

                        include('php/controllers/mail/verification-mail.php');
                    }

                } else $responseMessage = ['is_ok' => 0, 'message' => 'Пароль должен содержать минимум 8 символов и одно число'];
            } else $responseMessage = ['is_ok' => 0, 'message' => 'Некорректно указан email адрес'];
        } else $responseMessage = ['is_ok' => 0, 'message' => 'Заполните все поля формы'];

        echo json_encode($responseMessage);
    } else echo "Некорректный метод запроса, должен быть POST!";
    exit();
