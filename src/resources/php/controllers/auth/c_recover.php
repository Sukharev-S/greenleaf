<?php
// В случае не подтвержденного eMail адреса, при восстановлении пароля производится активация, т.к.
// уже осуществлен вход на почту зарегистрированного аккаунта

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $email = cleaner($_GET['email']);
    $hash = cleaner($_GET['hash']);
    if (isset($_GET['email']) && isset($_GET['hash']) && validateEmail($email)) {
        if (is_string($email) && is_string($hash)) {
            clearFailHashAndUser();                             // очищаем БД от старых\истекших пользователей и hash
            $userId = getUserIdByRecoverHashAndEmail($email, $hash);      // если hash не истек по времени(существует), то возвращает id юзера
            $isRecovery = isset($userId);                       // если юзер найден, то продолжаем процедуру восстановления
            if ($isRecovery) {
                if (!isUserActivated($userId)) isVerifiedUserUpdate($email);    // если он имеет не активированную почту, то активируем
            }
            include('php/views/recover.php');
            exit();
        } else echo "Некорректные входные параметры!";
    } else echo "Некорректные входные параметры!";
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $responseMessage = ['is_ok' => 0, 'message' => 'Неверные параметры запроса'];

    $email = cleaner($_POST['email']);
    if (!empty($email) && validateEmail($email)) {      // если полученный email проходит валидацию
        if (isEmailAlreadyExist($email)) {              // если такой email существует
            $userId = getIdUserByEmail($email);         // ищем пользователя в БД по его email
            deleteRecoverHashByUserId($userId);         // удаляем все предыдущие hash у этого пользователя
            $hash = md5($email . bin2hex(random_bytes(50)));    // генерируем новый recover hash
            addNewRecoverHash($userId, $hash);                              // пишем этот recover hash в БД

            $urlMail = substr($email, strripos($email, "@"));   // вырезание адреса почтового сервиса
            $responseMessage = ['is_ok' => 1, 'message' => $urlMail]; // на перед присваиваем успех с информацией адреса почты

            include('php/controllers/mail/recover-password-mail.php');
        } else $responseMessage = ['is_ok' => 0, 'message' => 'Аккаунт с таким Email адресом не зарегистрирован, возможно вы ошиблись?'];
    } else $responseMessage = ['is_ok' => 0, 'message' => 'Некорректно указан email адрес'];

    echo json_encode($responseMessage);
    exit();
}