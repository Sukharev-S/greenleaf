<?php

    $responseJSON = [];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $search_word = cleaner($_POST['search_string']);
        if (is_string($search_word) && !empty($search_word)
            && isNormalLength($search_word, 1, 50)) {
            if (URL_PARAMS['type'] === 'product') {
                $responseJSON = getAllProductsFound($search_word);
            }
            if (URL_PARAMS['type'] === 'order') {
                $orders = getAllOrdersFound($search_word);
                $responseJSON['orders'] = [];
                foreach ($orders as $order) {
                    $order['products'] = getProductsByOrder($order['id_order']);
                    array_push($responseJSON['orders'], $order);
                    $responseJSON['statusList'] = getOrderStatusList();
                }
            }
        }
    }

    echo json_encode($responseJSON);