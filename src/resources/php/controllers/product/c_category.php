<?php

    if (is_string(URL_PARAMS['id']) && !empty(URL_PARAMS['id'])) {
        $id_product = intval(cleaner(URL_PARAMS['id']));
        if ($id_product > 0) {
            $responseJSON = [
                "products" => getProductsByCategory(URL_PARAMS['id']), // выборка продуктов нужной категории
                "subCategories" => getSubCategories(URL_PARAMS['id']), // выборка подкатегорий нужной категории
            ];
            echo json_encode($responseJSON);
        } else echo "Некорректные входные параметры!";
    } else echo "Некорректные входные параметры!";