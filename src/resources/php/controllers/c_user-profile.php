<?php

// Переменная $address содержит
//    [country] => ''
//    [city] => ''
//    [street] => ''
//    [postcode] => 456456

    if (isset($user)) {
        $tab = cleaner(URL_PARAMS['tab']);

        $accountInfo = doesAccountInfoExist($user);         // проверяем заполнены ли перс. данные у пользователя
        $addressInfo = doesAddressExist($user);             // проверяем заполнен ли адрес у пользователя
        $ordersInfo = getOrdersByUser($user['id_user']);    // запрашиваем информацию по заказам пользователя

        if (empty($tab)) {                      // если нет прямого перехода на tab
            if (!$accountInfo) {                // если ПД не заполнены
                $tab = 'account';
            } elseif (!$addressInfo) {          // если адрес не заполнен
                $tab = 'address';
            } else $tab = 'orders';             // если все заполнено
            header("Location: /profile/".$tab);
        } elseif ($tab !== 'account' && $tab !== 'address' && $tab !== 'orders' && $tab !== 'admin-panel') { // если не адрес по вкладкам
            include('php/controllers/c_error_404.php');
            exit();
        } else {
            if ($user['is_admin']) {
                // Разбивка всех категорий на двумерный массив (используется в админ панели)
                $allCategories = getAllCategories();
                $categoriesArrDim[0] = [];
                $count = 0;
                foreach ($allCategories as $category) {
                    if ($category['parent_id'] === '0') {
                        $categoriesArrDim[$count] = $category;
                        $categoriesArrDim[$count]['subCategory'] = [];
                    } else array_push($categoriesArrDim[$category['parent_id'] - 1]['subCategory'], $category);
                    $count++;
                }
            }

            include('php/views/user-profile.php');
        }
    } else {
        echo 'Авторизуйтесь';
    }