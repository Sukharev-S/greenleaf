<?php

    $id_product = URL_PARAMS['id'];
    $product = getProductOne($id_product);
    $cardUrl = true;                   //  для активации навигационной ссылки в блоке header

// Тело ответа, содержиться в $product
//    [id] => 1
//    [article] => ASD001
//    [title] => моющее средство для кухни
//    [price] => 480.00
//    [old_price] =>
//    [weight] => 1000
//    [volume] => ''
//    [height] => ''
//    [width] => ''
//    [depth] => ''
//    [life] => ''
//    [stock] => 3
//    [package] => 1
//    [description] => ''
//    [description_short] => ''
//    [scope] => ''
//    [recommendation] => ''
//    [structure] => ''
//    [category_id] => 1
//    [category] => Чистящие средства для дома
//    [subcategory_id] => 10
//    [subcategory] => Для кухни
//    [subcategory_translit] => Dlya_kuhni
//    [is_popular] => 1
//    [is_new] => 0
//    [is_sale] => 0
//    [popular] => 0
//    [is_active] => 1
//    [main_image] => /img/products/ASD001_1.jpg
//    [images] => Array (
//      [0] => /img/products/ASD001_1.jpg
//      [1] => /img/products/ASD001_2.jpg
//      [2] => /img/products/ASD001_3.jpg )
//    [videos] => Array ( )
//    [certificates] => Array ( )
//    [similar] => Array (
//      [0] => ASD002
//      [1] => DAC004
//      [2] => DAC017
//      [3] => KAB001 )

    include('php/views/card.php');
