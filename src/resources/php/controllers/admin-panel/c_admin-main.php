<?php

if ($user['is_admin']) {
    $responseMessage = ['is_ok' => 0, 'message' => 'Неверные параметры запроса'];

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {

        if ((URL_PARAMS['option'] === 'promotion') && (is_string(URL_PARAMS['item']))) {
            $responseJSON = getHtmlElementInfo(URL_PARAMS['item']);
            if (!is_null($responseJSON)) {
                $responseMessage = ['is_ok' => 1, 'message' => 'Элемент найден'];
                $responseMessage['dataInfo'] = $responseJSON;
            } else $responseMessage = ['is_ok' => 0, 'message' => 'Эелемент по запрошенным данным не найден'];
        }

        if (URL_PARAMS['option'] === 'category') {
            if (URL_PARAMS['item'] === 'short_desc') $responseJSON = getShortDescCategories();
            if (URL_PARAMS['item'] === 'full_desc') $responseJSON = getFullDescCategories();
            if (!is_null($responseJSON)) {
                $responseMessage = ['is_ok' => 1, 'message' => 'Категории найдены'];
                $responseMessage['dataInfo'] = $responseJSON;
            } else $responseMessage = ['is_ok' => 0, 'message' => 'Категории по запрошенным данным не найдены'];
        }

        if (URL_PARAMS['option'] === 'product') {
            if (URL_PARAMS['item'] === 'all_products') $responseJSON = getAllProductsInfo();
            if (URL_PARAMS['item'] === 'one_product') {
                $id_product = cleaner(URL_PARAMS['id']);
                if (!empty($id_product) && is_string($id_product)) {
                    $responseJSON = getOneProductInfo($id_product);
                    $responseJSON['categories_by_product'] = getAllCategoriesByProduct($id_product);
                    $responseJSON['similar'] = getAllSimilarProducts($id_product);
                }
            }
            if (!is_null($responseJSON)) {
                $responseMessage = ['is_ok' => 1, 'message' => 'Запрос выполнен успешно!'];
                $responseMessage['dataInfo'] = $responseJSON;
            } else $responseMessage = ['is_ok' => 0, 'message' => 'При выполнении запросы возникла ошибка!'];
        }
    }


    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // форма promotion изменение текста html элемента
        if ((URL_PARAMS['option'] === 'promotion') && (URL_PARAMS['item'] === 'text')) {
            $id_element = intval(cleaner($_POST['id_element']));
            $text = cleaner($_POST['text']);
            if (mb_strlen($text) > 105) {
                $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка! Текст должен быть не более 105 символов!'];
            } else {
                if (updateHtmlElementText($id_element, $text)) {
                    $responseMessage = ['is_ok' => 1, 'message' => 'Текст успешно обновлен!'];
                }
            }
        } // ----

        // форма category изменение текста короткого описания категории
        if ((URL_PARAMS['option'] === 'category') && (URL_PARAMS['item'] === 'short_desc')) {
            $id_element = intval(cleaner($_POST['id_element']));
            $text = cleaner($_POST['text']);
            if (mb_strlen($text) > 300) {
                $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка! Текст должен быть не более 300 символов!'];
            } else {
                if (updateShortDescCategory($id_element, $text)) {
                    $responseMessage = ['is_ok' => 1, 'message' => 'Короткое описание категории успешно обновлено!'];
                }
            }
        } // ----

        // форма category изменение текста полного описания категории
        if ((URL_PARAMS['option'] === 'category') && (URL_PARAMS['item'] === 'full_desc')) {
            $id_element = intval(cleaner($_POST['id_element']));
            $text = cleaner($_POST['text']);
            if (mb_strlen($text) > 1000) {
                $responseMessage = ['is_ok' => 0, 'message' => 'Ошибка! Текст должен быть не более 1000 символов!'];
            } else {
                if (updateFullDescCategory($id_element, $text)) {
                    $responseMessage = ['is_ok' => 1, 'message' => 'Полное описание категории успешно обновлено!'];
                }
            }
        } // ----

        // форма adm-product__blanc изменение одного продукта всех параметров
        if ((URL_PARAMS['option'] === 'product') && (URL_PARAMS['item'] === 'one_product' || URL_PARAMS['item'] === 'add_product')) {

            $name = cleaner($_POST['name']);
            $price = cleaner($_POST['price']);
            $old_price = cleaner($_POST['old_price']);
            $article = mb_strtoupper(cleaner($_POST['article']));
            $stock = cleaner($_POST['stock']);
            $quantity_ordered = cleaner($_POST['quantity_ordered']);
            $date_delivery = cleaner($_POST['date_delivery']);
            $life = cleaner($_POST['life']);
            $package = cleaner($_POST['package']);
            $weight = cleaner($_POST['weight']);
            $volume = cleaner($_POST['volume']);
            $height = cleaner($_POST['height']);
            $width = cleaner($_POST['width']);
            $depth = cleaner($_POST['depth']);
            $short = cleaner($_POST['short']);
            $description = cleaner($_POST['description']);
            $structure = cleaner($_POST['structure']);
            $recommendation = cleaner($_POST['recommendation']);
            $scope = cleaner($_POST['scope']);
            $index_popular = cleaner($_POST['index_popular']);
            $is_new = ($_POST['is_new'] === 'on') ? 1 : 0;
            $is_popular = ($_POST['is_popular'] === 'on') ? 1 : 0;
            $is_sale = ($_POST['is_sale'] === 'on') ? 1 : 0;
            $is_active = ($_POST['is_active'] === 'on') ? 1 : 0;
            $id_product = cleaner($_POST['id_product']);
            $categories = cleaner($_POST['id_categories']);
            $similar = cleaner($_POST['id_similar']);

            // Название
            if (empty($name)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Название товара не может быть пустым!']);
                exit();
            } else {
                if (!isNormalLength($name, 4, 85)) {
                    echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Некорректная величина названия товара! Должно быть от 4 до 85 символов!']);
                    exit();
                } else {
                    if (!preg_match(PATTERN_NAME_GAP, $name)) {
                        echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Некорректное название товара! Должны быть 
                        использованы только буквы алфавита и знак "-".']);
                        exit();
                    }
                }
            }
            // Актуальная цена
            if (!empty($price) && !preg_match('/^[0-9.]+$/', $price) || (!isNormalLength($price, 0, 10))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указана актуальная цена товара!']);
                exit();
            }
            // Старая цена
            if (!empty($old_price) && !preg_match('/^[0-9.]+$/', $old_price) || (!isNormalLength($old_price, 0, 10))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указана старая цена товара!']);
                exit();
            }
            // Артикль
            if ((!empty($article) && !preg_match('/^[0-9A-Z]+$/', $article)) || (!empty($article) && !isNormalLength($article, 6, 10))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указан артикль!']);
                exit();
            } elseif (empty($article)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Артикль не должен быть пустым! Если его нет, то введите NOT001 и т.д.']);
                exit();
            }
            // Кол-во в наличии
            if ((!empty($stock) && !preg_match(PATTERN_NUMBER, $stock)) || (!isNormalLength($stock, 0, 6))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указано кол-во шт. в наличии!']);
                exit();
            }
            // Кол-во заказанного товара
            if ((!empty($quantity_ordered) && !preg_match(PATTERN_NUMBER, $quantity_ordered)) || (!isNormalLength($quantity_ordered, 0, 6))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указано кол-во заказанного товара!']);
                exit();
            }
            // Дата доставки
            if ((!empty($date_delivery) && !preg_match('/^[0-9\-]+$/', $date_delivery))
                || (!empty($date_delivery) && !isNormalLength($date_delivery, 10, 10))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Дата указана неверно! Формат даты: ДД-ММ-ГГГГ']);
                exit();
            } else {
                $test_data_ar = explode('-', $date_delivery);
                //если дата не введена в корректном формате d.m.Y (checkdate(месяц, день, год))
                if (!@checkdate($test_data_ar[1], $test_data_ar[0], $test_data_ar[2]) && !empty($date_delivery)) {
                    echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Такой даты не существует!']);
                    exit();
                } elseif (!empty($date_delivery)) $date_delivery = date("Y-m-d", strtotime($date_delivery)); // переворачиваем для записи в БД
            }
            // Срок годности
            if (!empty($life) && !preg_match(PATTERN_NUMBER, $life) || (!isNormalLength($life, 0, 3))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указан срок годности! Указывать необходимо в месяцах.']);
                exit();
            }
            // Кол-во в упаковке
            if ((!empty($package) && !preg_match(PATTERN_NUMBER, $package)) || (!isNormalLength($package, 0, 6))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указано кол-во шт. в упаковке!']);
                exit();
            }
            // Вес
            if ((!empty($weight) && !preg_match(PATTERN_NUMBER, $weight)) || (!isNormalLength($weight, 0, 6))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указан вес товара!']);
                exit();
            }
            // Объем
            if ((!empty($volume) && !preg_match(PATTERN_NUMBER, $volume)) || (!isNormalLength($volume, 0, 6))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указан объем товара!']);
                exit();
            }
            // Высота
            if ((!empty($height) && !preg_match(PATTERN_NUMBER, $height)) || (!isNormalLength($height, 0, 10))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указана высота товара!']);
                exit();
            }
            // Ширина
            if ((!empty($width) && !preg_match(PATTERN_NUMBER, $width)) || (!isNormalLength($width, 0, 6))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указана ширина товара!']);
                exit();
            }
            // Глубина
            if ((!empty($depth) && !preg_match(PATTERN_NUMBER, $depth)) || (!isNormalLength($depth, 0, 10))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указана глубина товара!']);
                exit();
            }
            // Краткое описание
            if (!empty($short) && !isNormalLength($short, 10, 350)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверная длина краткого описания товара! Длина должна быть от 10 до 350 символов.']);
                exit();
            }
            // Полное описание
            if (!empty($description) && !isNormalLength($description, 10, 1000)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверная длина описания товара! Длина должна быть от 10 до 1000 символов.']);
                exit();
            }
            // Состав
            if (!empty($structure) && !isNormalLength($structure, 10, 1000)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверная длина состава товара! Длина должна быть от 10 до 1000 символов.']);
                exit();
            }
            // Рекомендации
            if (!empty($recommendation) && !isNormalLength($recommendation, 10, 1000)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверная длина рекомендаций товара! Длина должна быть от 10 до 1000 символов.']);
                exit();
            }
            // Сфера применения
            if (!empty($scope) && !isNormalLength($scope, 10, 1000)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверная длина сферы применения товара! Длина должна быть от 10 до 1000 символов.']);
                exit();
            }
            // Индекс популярности
            if ((!empty($index_popular) && !preg_match(PATTERN_NUMBER, $index_popular))
                || ((intval($index_popular) < 0) || (intval($index_popular) > 100))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указан индекс популярности. Должен быть от 0 до 100!']);
                exit();
            } elseif (empty($index_popular)) $index_popular = 0;
            // Категории
            if (empty($categories) || !preg_match('/^[0-9,]+$/', $categories)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Не выбрана категория товара!']);
                exit();
            }
            // Похожие товары
            if (!empty($similar) && !preg_match('/^[0-9,]+$/', $similar)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Обратитесь к администратору сайта! Сбой работы с похожими товарами.']);
                exit();
            }

            if (URL_PARAMS['item'] === 'one_product') {
                $params = [$id_product, $article, $date_delivery, $depth, $description, $height, $index_popular, $is_active,
                    $is_new, $is_popular, $is_sale, $life, $name, $old_price, $package, $price, $quantity_ordered,
                    $recommendation, $scope, $short, $stock, $structure, $volume, $weight, $width];
                // проверка на дубликат артикля
                $duplicatesArticle = getAllProductsFound($article);
                if (count($duplicatesArticle) > 0 && strcasecmp($duplicatesArticle[0]['id_product'], $id_product) !== 0) {
                    echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Такой артикль уже существует, исправьте 
                    артикли на одном из товаров и повторите снова. Артикли не должны повторяться!']);
                    exit();
                }
                updateOneProduct($params);
                $responseMessage = ['is_ok' => 1, 'message' => 'Все изменения сохранены успешно!'];
            }

            if (URL_PARAMS['item'] === 'add_product') {
                $params = [$article, $date_delivery, $depth, $description, $height, $index_popular, $is_active,
                    $is_new, $is_popular, $is_sale, $life, $name, $old_price, $package, $price, $quantity_ordered,
                    $recommendation, $scope, $short, $stock, $structure, $volume, $weight, $width];
                // проверка на дубликат артикля
                $duplicatesArticle = getAllProductsFound($article);
                if (count($duplicatesArticle) > 0) {
                    echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Такой артикль уже существует, исправьте 
                    артикли на одном из товаров и повторите снова. Артикли не должны повторяться!']);
                    exit();
                }
                $id_product = addOneProduct($params);
                $responseMessage = ['is_ok' => 1, 'message' => 'Новый товар добавлен успешно!'];
            }

            // полное удаление и добавление категорий соотв-ее товару
            deleteCategoriesFromProduct($id_product);
            $categories = explode(',', $_POST['id_categories']);
            foreach ($categories as $category) {
                addCategoryToProduct($id_product, $category);
            }

            // полное удаление и добавление похожих товаров этому товару
            if (!empty($similar)) {
                $similar = explode(',', $_POST['id_similar']);
                deleteSimilarFromProduct($id_product);
                foreach ($similar as $id_similar) {
                    addSimilarToProduct($id_product, $id_similar);
                }
            }


        } // ----

        // проверка правильности ввода в поле добавление нового similar продукта в список с similar
        if ((URL_PARAMS['option'] === 'product') && (URL_PARAMS['item'] === 'add-similar')) {
            $article = mb_strtoupper(cleaner($_POST['search_word']));
            // Артикль
            if ((!empty($article) && !preg_match('/^[0-9A-Z]+$/', $article)) || (!empty($article) && !isNormalLength($article, 6, 10))) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Неверно указан артикль!']);
                exit();
            }
            $responseJSON = getAllProductsFound($article);
            if (empty($responseJSON)) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! Такого артикля не существует']);
                exit();
            }
            if (count($responseJSON) > 1) {
                echo json_encode(['is_ok' => 0, 'message' => 'Ошибка! С таким артиклем найдено более одного товара, исправьте артикли на одном из них и повторите снова. Артикли не должны повторяться!']);
                exit();
            }
            $responseMessage = ['is_ok' => 1, 'message' => 'Похожий продукт добавлен успешно. Не забудте сохранить изменения!'];
            $responseMessage['dataInfo'] = $responseJSON[0];
        }

        // изменение статуса заказа с отправкой почтовых уведомлений
        if ((URL_PARAMS['option'] === 'order') && (URL_PARAMS['item'] === 'status')) {
            $id_order = intval(cleaner($_POST['id_order']));
            $id_status = intval(cleaner($_POST['id_status']));

            if (updateStatusOrder($id_order, $id_status)) {
                $responseMessage = ['is_ok' => 1, 'message' => 'Статус заказа успешно изменен!'];

                // подготовка данных для отправки почты
                $userInfoForEmail = [];
                $productsInfoStringAdmin = '';
                $productsInfoStringClient = '';
                $orderInfo = getOrderInfo($id_order);           // данные из таблицы "orders"
                if (is_null($orderInfo['user_info'])) {         // если поле "user_info" таблицы orders не заполнено (значит клиент точно авторизован)
                    $user = getUserById($orderInfo['id_user']);     // данные из таблицы "users"
                    $userName = $user['last_name'].' '.$user['first_name']. ' '.$user['middle_name'];
                    $userInfoForEmail = [
                        'name' => $userName,
                        'email' => $user['email'],
                        'phone' => $user['phone'],
                        'country' => $user['country'],
                        'city' => $user['city'],
                        'street' => $user['street'],
                        'postcode' => $user['postcode'],
                    ];
                } else {    // если поле "user_info" заполнено, значит данные клиента изменены или он не авторизован
                    $userInfo = json_decode($orderInfo['user_info']);
                    if (!is_null($orderInfo['id_user'])) {      // если клиент авторизован, значит у него нет поля "full_name"
                        $userName = $userInfo -> lastName.' '.$userInfo -> firstName. ' '.$userInfo -> middleName;
                    } else {        // при самовывозе у неавторизованного клиента есть поле "full_name"
                        $userName = ($orderInfo['method_delivery'] === 'pickup')
                            ? $userInfo -> full_name
                            : $userInfo -> lastName.' '.$userInfo -> firstName. ' '.$userInfo -> middleName;
                    }
                    $userInfoForEmail = [
                        'name' => $userName,
                        'email' => $userInfo -> email,
                        'phone' => $userInfo -> phone,
                        'country' => $userInfo -> country,
                        'city' => $userInfo -> city,
                        'street' => $userInfo -> street,
                        'postcode' => $userInfo -> postcode,
                    ];
                }
                $methodDeliveryRus = $orderInfo['method_delivery'] === 'pickup' ? 'самовывоз' : 'почтовая служба';
                if ($orderInfo['method_delivery'] === 'delivery') {            // заполнение строки адреса клиента при доставке
                    $deliveryHTMLString = "
                        <div><strong>Доставка по адресу:</strong></div>
                        <div style='margin-left: 10px'>Страна: <em><strong>".$userInfoForEmail['country']."</strong></em></div>
                        <div style='margin-left: 10px'>Населенный пункт: <em><strong>".$userInfoForEmail['city']."</strong></em></div>
                        <div style='margin-left: 10px'>Улица, дом, кв.: <em><strong>".$userInfoForEmail['street']."</strong></em></div>
                        <div style='margin-left: 10px'>ИНДЕКС: <em><strong>".$userInfoForEmail['postcode']."</strong></em></div>
                    ";
                }
                $productsInfo = getProductsByOrder($id_order);
                foreach ($productsInfo as $productInfo) {       // заполнение заказа продуктами
                    $id_product = $productInfo['id_product'];
                    $productName = $productInfo['name'];
                    $quantity = $productInfo['quantity'];
                    $sum_rub = $productInfo['sum_rub'];
                    $productsInfoStringAdmin = $productsInfoStringAdmin.
                        "<li><a href='https://гринлиф-экотовары.рф/product/".$id_product."' 
                    style='text-transform: uppercase; font-size: 12px;'>".$productName."</a>___".$quantity." шт.</li>";
                    $productsInfoStringClient = $productsInfoStringClient.
                        "<li><a href='https://гринлиф-экотовары.рф/product/".$id_product."' 
                    style='text-transform: uppercase; font-size: 12px;'>".$productName."</a>___".$quantity." шт.___".$sum_rub." руб.</li>";
                }

                // отправка почты клиенту
                include('php/controllers/mail/order-status-changed-client-mail.php');
                // отправка почты администратору
                include('php/controllers/mail/order-status-changed-admin-mail.php');
            } else $responseMessage = ['is_ok' => 0, 'message' => 'Что-то пошло не так :( Обратитесь к программисту!'];
        }
    }

    echo json_encode($responseMessage);
}