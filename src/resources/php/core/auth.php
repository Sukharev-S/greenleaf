<?php

// Функция авторизации пользователя
    function authGetUser() : ?array {
        $user = null;
        $token = $_SESSION['token'] ?? $_COOKIE['token'] ?? null;   // ищем токен в сессии и в куках

        if ($token !== null) {                              //  если нашли какой-нибудь токен
            $session = getSessionByToken($token);           //  ищем этот токен в таблице сессий

            if ($session !== null) {                        //  если такой токен найден в БД
                $user = getUserById($session['id_user']);   //  ищем юзера по этому токену в БД
            } else {                                        //  если такого токена нет в БД
                unset($_SESSION['token']);                  //  удаляем сессию
                unset($_COOKIE['token']);                   //  удаляем куку token
                setcookie('token', null, -1, '/');
            }
        }
        return $user;
    }

// Функция проверки заполненности данных адреса пользователя
    function doesAddressExist(?array $user) : bool {
        if (isset($user)) {
            return (is_null($user['country'])
                || is_null($user['city'])
                || is_null($user['street'])
                || is_null($user['postcode'])) ? false : true;
        } else return false;
    }

// Функция проверки заполненности данных аккаунта пользователя
    function doesAccountInfoExist(?array $user) : bool {
        if (isset($user)) {
            return (is_null($user['first_name'])
                || is_null($user['last_name'])
                || is_null($user['middle_name'])
                || is_null($user['email'])
                || is_null($user['phone'])) ? false : true;
        } else return false;
    }

// Функция очистки БД от старых хэшей (восстановления пароля и активации аккаунта) и не активированных юзеров(72часа)
    function clearFailHashAndUser() {
        deleteVerifyHashByTime();       // удаление просроченных хэшей для активации почты
        deleteRecoverHashByTime();      // удаление просроченных хэшей для активации почты
        deleteUsersByTime();            // удаление неактивированных пользователей
    }