
<? include('php/views/parts/common/html-head.php') ?>

    <main class="error-page">
        <div class="error-page__wrapper">
            <h1 class="error-page__number">Ошибка 404</h1>
            <h2 class="error-page__title">Страница не найдена</h2>
            <div class="error-page__text">К сожалению, страницы
                (<span class="error-page__info"><?=$lastParamUrl?></span>) по адресу
                <span class="error-page__info">"<?=$fullUrl?>"</span> нет на нашем сайте.</div>
            <div class="error-page__text">Возможно, вы ввели неправильный адрес или страница была удалена с сервера.</div>
            <div class="error-page__ref-text">Вы можете
                <a href="/" class="error-page__ref main-link">вернуться на главную страницу</a>
            </div>
        </div>
        <div class="overlay"></div>
    </main>
    </div> <!-- /.site-container -->
</body>
</html>
