<div class="hero-catalog">
    <div class="container container-narrow hero-catalog__container">
        <div class="hero-catalog__slider" data-start-slide="<?=$currentIdCategory?>">
            <button class="hero-prev-btn btn-reset">
                <svg class="">
                    <use xlink:href="/img/sprite.svg#prev-arrow"></use>
                </svg>
            </button>
            <button class="hero-next-btn btn-reset">
                <svg class="">
                    <use xlink:href="/img/sprite.svg#next-arrow"></use>
                </svg>
            </button>
            <div class="swiper-wrapper">

                <? foreach($categories as $category) { ?>
                    <div class="swiper-slide">
                        <div class="catalog-slide" data-category="<?=$category['id']?>">
                            <div class="catalog-slide__about">
                                <h2 class="catalog-slide__title"><?=$category['name']?></h2>
                                <div class="catalog-slide__description">
                                    <span class="catalog-slide__short-desc"><?=$category['desc_short']?></span>
                                    <span class="catalog-slide__details is-reduce is-delete"><?=$category['description']?></span>
                                    <a class="catalog-slide__show-details js-catalog-slide__more">Подробнее..</a>
                                    <a class="catalog-slide__show-details js-catalog-slide__hidden is-delete">Свернуть</a>
                                </div>
                                <button class="catalog-slide__btn-show btn-reset btn-default" type="submit">Показать категорию</button>
                            </div>
                            <div class="catalog-slide__image-wrap">
                                <img class="catalog-slide__image" src="<?=$category['main_img']?>" alt="<?=$category['name']?>">
                            </div>
                        </div>
                    </div> <!--/.swiper-slide -->
                <? } ?>

            </div> <!-- /.swiper-wrapper -->
        </div> <!-- /.hero-catalog__slider -->
        <div class="swiper-pagination hero-pag"></div>
    </div> <!-- /.hero-catalog__container -->

</div>