<div class="card-top">
    <div class="container container-narrow">
        <div class="card-top__left">
            <div class="card-slider">
                <div class="card-slider__thumbs">
                    <? foreach ($product['images'] as $image) { ?>
                        <div class="card-slider__thumb-wrapper">
                            <div class="card-slider__thumb">
                                <img src="<?= $image?>" alt="изображение товара">
                            </div>
                        </div>
                    <? } ?>
                </div>
                <div class="card-slider__main">
                    <img class="card-slider__main-img card-slider__main-img--two card-slider__main-img--hidden" src="<?=$product['main_image']?>" alt="изображение продукта">
                    <img class="card-slider__main-img card-slider__main-img--one" src="<?=$product['main_image']?>" alt="изображение продукта">
                    <? if(!$product['is_active']) { ?>
                        <div class="card-slider__not-sale">Снят с продаж</div>
                    <? } ?>
                    <span class="card-info__prop product-prop">
                        <? if ($product['is_new']) { ?>
                            <img class="card-info__prop-item" src="/img/prop_new.png" alt="Иконка новинки">
                        <? } ?>
                        <? if ($product['is_popular']) { ?>
                            <img class="card-info__prop-item" src="/img/prop_popular.png" alt="Иконка популярного товара">
                        <? } ?>
                        <? if ($product['is_sale']) { ?>
                            <img class="card-info__prop-item" src="/img/prop_hot-price.png" alt="Иконка товара по распродаже">
                        <? } ?>

                    </span>
                </div> <!-- /.card-slider__main -->
            </div> <!-- /.card-slider -->
        </div> <!-- /.card-top__left -->
        <div class="card-top__right">
            <div class="card-info" data-id="<?= $product['id'] ?>">
                <h3 class="card-info__title"><?=$product['title']?></h3>
                <div class="card-info__price info-price">
                    <? if ($product['price']) { ?>
                        <span class="js-conventional-unit info-price__conventional">₽</span>
                        <div class="info-price__current"><?=$product['price']?></div>
                    <? } else { ?>
                        <div class="info-price__current">Ожидается</div>
                    <? } ?>
                    <? if ($product['old_price']) { ?>
                        <span class="js-conventional-unit info-price__conventional-old">₽</span>
                        <div class="info-price__old"><?=$product['old_price']?></div>
                    <? } ?>
                </div>

                <div class="card-info__short-description">
                    <?=$product['description_short']?>
                </div>

                <div class="card-info__quantity-article">
                    <div class="card-info__stock-quantity">
                        <span class="card-info__label">Кол-во в наличии: </span>
                        <span class="card-info__value"><?=$product['stock']?></span>
                    </div>
                    <div class="card-info__article">
                        <span class="card-info__label">Артикул:</span>
                        <span class="card-info__value"><?=$product['article']?></span>
                    </div>
                </div>

                <? if ((float)$product['price'] == 0) {
                    $dis = 'disabled';
                } else { $dis = '';} ?>
                <div class="card-info__stepper stepper">
                    <button type="button" class="btn-reset stepper__btn stepper__btn--minus stepper__btn--disabled" aria-label="минус" <?= $dis ?>>
                        <svg class="">
                            <use xlink:href="/img/sprite.svg#minus"></use>
                        </svg>
                    </button>
                    <input type="text" class="stepper__input" value="1" maxlength="5" <?= $dis ?>>
                    <button type="button" class="btn-reset stepper__btn stepper__btn--plus" aria-label="плюс" <?= $dis ?>>
                        <svg class="">
                            <use xlink:href="/img/sprite.svg#plus"></use>
                        </svg>
                    </button>
                </div> <!-- /.stepper -->
                <button class="card-info__btn card-info__btn--tocart btn-default" <?= $dis ?>>Добавить в корзину</button>
                <button class="card-info__btn card-info__btn--tocheckout btn-default" <?= $dis ?>>Купить сейчас</button>

                <div class="card-info__bottom card-bottom">
                    <ul class="card-info__list">
                        <li class="card-info__item card-info__expiration">
                            <span class="card-info__label">Срок годности: </span>
                            <? if ($product['life']) { ?>
                                <span class="card-info__value"><?=$product['life']?></span>
                                мес.
                            <? } ?>
                        </li>
                        <li class="card-info__item card-info__volume">
                            <span class="card-info__label">Вес в упаковке: </span>
                            <? if ($product['weight']) {?>
                                <span class="card-info__value"><?=$product['weight']?></span>
                                , грамм
                            <? } ?>
                        </li>
                        <li class="card-info__item card-info__weight">
                            <span class="card-info__label">Объем: </span>
                            <? if ($product['volume']) {?>
                                <span class="card-info__value"><?=$product['volume']?></span>
                                , мл
                            <? } ?>
                        </li>
                        <li class="card-info__item card-info__package">
                            <span class="card-info__label">Количество в упаковке: </span>
                            <? if ($product['package']) {?>
                                <span class="card-info__value"><?=$product['package']?></span>
                                , шт
                            <? } ?>
                        </li>
                        <li class="card-info__item card-info__dimensions">
                            <span class="card-info__label">Размеры: </span>
                            <? if ($product['height'] && $product['width'] && $product['depth']) {?>
                                <span class="card-info__value"><?=$product['height']?> x <?=$product['width']?> x <?=$product['depth']?></span>
                                , мм
                            <? } ?>
                        </li>
                    </ul>

                    <div class="card-bottom__social">
                        <? include('php/views/parts/common/social-networks.php') ?>
                    </div>
                </div> <!-- /.card-bottom -->

            </div> <!-- /.card-info -->
        </div> <!-- /.card-top__right -->
        <div class="card-info__category">
            <a href="/" class="main-link">Главная</a>
            <span>
                        <svg class="card-info__arrow-right">
                            <use xlink:href="/img/sprite.svg#angle-right"></use>
                        </svg>
                    </span>
            <a href="/catalog/category/<?=$product['category_id']?>" class="main-link"><?=$product['category']?></a>
            <span>
                        <svg class="card-info__arrow-right">
                            <use xlink:href="/img/sprite.svg#angle-right"></use>
                        </svg>
                    </span>
            <a href="/catalog/category/<?=$product['category_id']?>/<?=$product['subcategory_translit']?>" class="main-link"><?=$product['subcategory']?></a>
        </div> <!-- /.card-info__category -->
    </div> <!-- /.container -->
</div> <!-- /.card-top -->