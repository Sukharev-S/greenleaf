<div class="image-modal">
    <div class="image-modal__content">
        <a class="image-modal__distinct-window" href="" target="_blank">Открыть в отдельном окне</a>
        <div class="image-modal__wrap-img">
            <img class="image-modal__img" src="" alt="">
        </div>
    </div>
</div>