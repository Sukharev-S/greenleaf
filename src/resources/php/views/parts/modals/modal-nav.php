<section class="nav-modal">
    <div class="nav-modal__content">
        <button class="nav-modal__close btn-reset">
            <svg class="nav-modal__close-icon modal__close">
                <use xlink:href="/img/sprite.svg#close" aria-label="закрыть бургер меню"></use>
            </svg>
        </button>
        <div class="nav-modal__top">
            <ul class="nav-modal__list">
                <li class="nav-modal__item">
                    <a href="/" class="main-link nav-modal__link" aria-label="Переход на главную страницу">Главная</a>
                </li>
                <li class="nav-modal__item">
                    <a href="/catalog/category/1" class="main-link nav-modal__link" aria-label="Переход в магазин">Магазин</a>
                </li>
                <li class="nav-modal__item">
                    <a href="/company" class="main-link nav-modal__link" aria-label="Переход на страницу доставка">О компании</a>
                </li>
                <li class="nav-modal__item">
                    <a href="/cooperation" class="main-link nav-modal__link" aria-label="Переход на страницу сотрудничество">Сотрудничество</a>
                </li>
                <li class="nav-modal__item">
                    <a href="/about" class="main-link nav-modal__link" aria-label="Переход на страницу о нас">О нас</a>
                </li>
                <li class="nav-modal__item nav-modal__cart">
                    <a href="/cart/shopping" class="main-link nav-modal__link" aria-label="Переход в корзину">Корзина</a>
                </li>
            </ul>
        </div> <!-- /.nav-modal__top -->
        <div class="nav-modal__bottom">
            <? include('php/views/parts/common/social-networks.php') ?>
            <a href="/catalog/category/1" class="hero__to-shop button">
                <span class="hero__green">Green</span>Leaf
            </a>
        </div> <!-- /.nav-modal__bottom -->
    </div> <!-- /.nav-modal__content -->
</section>