<section class="cart-modal">
    <div class="cart-modal__content">
        <div class="cart-modal__top">
            <h3 class="cart-modal__title">Продуктов в корзине: <span class="cart-modal__title-quantity">0</span></h3>
            <button class="cart-modal__close btn-reset">
                <svg class="cart-modal__close-icon modal__close">
                    <use xlink:href="/img/sprite.svg#close" aria-label="закрыть всплывающее окно корзины"></use>
                </svg>
            </button>
        </div> <!-- /.cart-modal__top -->
        <div class="cart-modal__empty">
            <svg class="cart-modal__empty-img">
                <use xlink:href="/img/sprite.svg#cart-empty" aria-label="корзина пуста"></use>
            </svg>
            <span class="cart-modal__empty-text">Корзина пуста.</span>
        </div>
        <ul class="cart-modal__list is-hidden"></ul>
        <div class="cart-modal__bottom is-hidden">
            <div class="cart-modal__total-wrapper">
                <span class="cart-modal__total">Итого:</span>
                <div class="cart-modal__counter-wrap">
                    <span class="js-conventional-unit">₽</span>
                    <span class="cart-modal__counter">₽0.00</span>
                </div>
            </div>
            <a href="/cart/checkout" class="cart-modal__order-btn btn-reset" aria-label="перейти к оформлению заказа">Оформление заказа</a>
            <a href="/cart/shopping" class="cart-modal__view-btn btn-reset" aria-label="перейти в корзину">Просмотр корзины</a>
        </div> <!-- /.cart-modal__bottom -->
        <div class="cart-modal__ad is-hidden">
            <span class="cart-modal__ad-text"><?=getHtmlElementInfo('cart-modal__ad')['element_text']?></span>
        </div> <!-- /.cart-modal__ad -->
    </div> <!-- /.cart-modal__content -->
</section> <!-- /.cart-modal -->