<section class="authorization-modal">
    <div class="authorization-modal__into-wrapper">
        <? include('php/views/parts/common/callback-message.php') ?>
        <button class="authorization-modal__close btn-reset">
            <svg class="authorization-modal__close-icon modal__close">
                <use xlink:href="/img/sprite.svg#close" aria-label="закрыть всплывающее окно авторизации"></use>
            </svg>
        </button>
        <div class="authorization-modal__content-wrapper">
            <div class="authorization-modal__top">
                <a href="#" class="authorization-modal__tab authorization-modal__tab--active js-tab-login">Вход</a>
                <a href="#" class="authorization-modal__tab js-tab-reg">Регистрация</a>
            </div> <!-- /.authorization-modal__top -->
            <div class="authorization-modal__content">

            <div class="authorization-modal__tab-block login-block">
                <form action="#" class="login-block__form js-modal-login">
                    <input type="text"
                           class="login-block__email form-field"
                           required="required"
                           placeholder="Введите Email"
                           name="email"
                           autocomplete="on">
                    <input type="password"
                           class="login-block__password form-field"
                           required="required"
                           placeholder="Введите пароль"
                           name="password"
                           autocomplete="on">
                    <div class="login-block__remember-wrapper">
                        <label class="login-block__remember-label" for="remember_me">
                            <input class="login-block__remember-checkbox" name="remember" checked id="remember_me" type="checkbox">
                            <span class="login-block__remember-text">Запомнить меня</span>
                        </label>
                        <a href="#" class="login-block__lost main-link">Забыли пароль?</a>
                    </div>
                    <button class="login-block__submit btn-default" type="submit" aria-label="Войти">Войти</button>
                </form>
                <button class="login-block__create-acc btn-default" aria-label="создание аккаунта">У нас впервые?</button>
            </div><!-- /.authorization-modal__login-block -->

            <div class="authorization-modal__tab-block reg-block authorization-modal__tab-block--fade">
                <form action="#" class="reg-block__form js-modal-registration">
                    <input type="text"
                           class="reg-block__email form-field"
                           required="required"
                           placeholder="Введите Email"
                           name="email">
                    <input type="password"
                           class="reg-block__password form-field"
                           required="required"
                           placeholder="Введите желаемый пароль"
                           name="password"
                           autocomplete="off">
                    <div class="reg-block__policy">
                        <p>Ваши личные данные будут использоваться для поддержки вашего опыта работы с этим веб-сайтом,
                            для управления доступом к вашей учетной записи и для других целей, описанных в нашей
                            <br /><a href="#" class="reg-block__policy-ref main-link js-policy-ref">политике конфиденциальности</a>.
                        </p>
                    </div>
                    <button class="reg-block__submit btn-default" aria-label="регистрация">Присоединиться</button>
                </form>
                <button class="reg-block__have-acc btn-default" aria-label="если аккаунт уже существует">Уже есть кабинет</button>
            </div><!-- /.authorization-modal__login-block -->

            <div class="authorization-modal__tab-block recover-block authorization-modal__tab-block--fade">
                <form action="#" class="recover-block__form js-modal-recover">
                    <input type="text"
                           class="recover-block__email form-field"
                           required="required"
                           placeholder="Введите Email от аккаунта"
                           name="email"
                           autocomplete="on">
                    <div class="recover-block__about-text">
                        <p>На этот Email будет выслана информация для восстановления забытого пароля.
                            После нажатия на кнопку мы начнем поиск, а Вы будете переадресованы на сайт Вашего почтового сервиса.</p>
                    </div>
                    <button class="recover-block__submit btn-default" type="submit" aria-label="Сбросить пароль">Восстановить</button>
                </form>
            </div><!-- /.authorization-modal__recover-block -->

        </div> <!-- /.authorization-modal__content -->
        </div>
    </div> <!-- /.authorization-modal__into-wrapper -->
</section> <!-- /.authorization-modal -->