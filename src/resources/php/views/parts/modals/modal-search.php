<section class="search-modal">
    <div class="search-modal__content-wrapper">
        <div class="search-modal__content">
            <button class="search-modal__close btn-reset">
                <svg class="search-modal__close-icon modal__close">
                    <use xlink:href="/img/sprite.svg#close" aria-label="закрыть окно поиска"></use>
                </svg>
            </button>
            <form action="#" class="search-modal__form form-search">
                <span class="form-search__title">Что вы хотите найти?</span>
                <input class="form-search__field form-field"
                       type="search"
                       placeholder="Введите имя товара или его артикль"
                       name="search_string"
                       autocomplete="off">
                <div class="js-search-loader is-delete">
                    <? include('php/views/parts/common/loader.php') ?>
                </div>
                <button class="form-search__submit" type="submit" aria-label="поиск">
                    <svg class="form-search__search-icon">
                        <use xlink:href="img/sprite.svg#search" aria-label="search icon"></use>
                    </svg>
                </button>
                <div class="form-search__results-container">
                    <div class="form-search__results-inner" data-bar_search>
                        <div class="form-search__empty form-search__empty--hidden">Совпадений не найдено..</div>
                    </div> <!-- /.form-search__results-inner -->
                </div> <!-- /.form-search__results-container -->
            </form>
        </div> <!-- /.search-modal__content -->
    </div> <!-- /.search-modal__content-wrapper -->
</section> <!-- /.search-modal -->