<section class="tab-content__tab checkout <? echo $tab === 'checkout' ? 'tab-content__tab--active' : 'is-delete'?>" data-section="#checkout">
    <h2 class="visually-hidden">Оформление</h2>
    <div class="checkout__messages messages">
        <? if (!isset($user)) { ?>
            <div class="checkout__advantages checkout-advantages">
                <h4 class="checkout-advantages__title">Присоединившись к нам вы сможете:</h4>
                <ul class="checkout-advantages__list">
                    <li class="checkout-advantages__item">Отслеживать статус заказа</li>
                    <li class="checkout-advantages__item">Просматривать историю заказов</li>
                    <li class="checkout-advantages__item">Сохранять корзину товаров при входе с любого
                        устройства будь то планшет, смартфон или компьютер</li>
                    <li class="checkout-advantages__item">Автоматически применять персональную скидку</li>
                    <li class="checkout-advantages__item">Гораздо быстрее совершать заказы, путем
                        автоматического ввода имени, адреса и другой контактной информации</li>
                </ul>
            </div>
            <div class="messages__item messages__customer js-messages__customer">
                Нажмите, чтобы присоединиться
            </div>
        <? } ?>

        <div class="messages__item messages__coupon js-messages__coupon">
            Есть купон? Нажмите здесь, чтобы ввести код купона
        </div>
        <form class="checkout__form-coupon form-coupon" action="#">
            <p class="form-coupon__description">Если у вас есть код купона, примените его ниже.</p>
            <div class="form-coupon__coupon coupon">
                <input class="coupon__text" type="text" name="coupon-code" value="" placeholder="Код купона">
                <input class="coupon__apply-btn btn-reset" type="submit" name="coupon-apply" value="Применить купон">
            </div>
        </form> <!-- /.checkout__form-coupon -->
        <div class="messages__item messages__error">
            <ul class="messages__error-list"></ul>
        </div> <!-- /.messages__error -->
    </div>
    <div class="checkout__content-wrapper">

        <div class="checkout__form-wrapper">
            <? if (!isset($user)) { ?>
                <form class="checkout__form checkout-form checkout-form--active" id="pickup">
                    <h3 class="checkout-form__title">Пожалуйста представьтесь</h3>
                    <div class="form__item">
                            <label class="checkout-form__label form__label" for="pickup-full_name">Как к Вам обращаться? *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="pickup-full_name"
                                   name="full_name">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                    <div class="form__item">
                        <label class="checkout-form__label form__label" for="pickup-email">Email *</label>
                        <input type="text"
                               class="checkout-form__input form-field js-required"
                               id="pickup-email"
                               name="email">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="checkout-form__label form__label" for="pickup-phone">Телефон *</label>
                        <input type="text"
                               class="checkout-form__input form-field js-required"
                               id="pickup-phone"
                               placeholder="Например: 8-999-999-99-99"
                               name="phone">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                </form> <!-- /.#pickup(form) -->
                <form class="checkout__form checkout-form is-delete" id="delivery">
                    <h3 class="checkout-form__title">Введите данные доставки</h3>
                    <div class="form__item">
                        <label class="checkout-form__label form__label" for="delivery-first_name">Имя *</label>
                        <input type="text"
                               class="checkout-form__input form-field js-required"
                               id="delivery-first_name"
                               name="first_name">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="checkout-form__label form__label" for="delivery-last_name">Фамилия *</label>
                        <input type="text"
                               class="checkout-form__input form-field js-required"
                               id="delivery-last_name"
                               name="last_name">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="checkout-form__label form__label" for="delivery-middle_name">Отчество *</label>
                        <input type="text"
                               class="checkout-form__input form-field js-required"
                               id="delivery-middle_name"
                               name="middle_name">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="checkout-form__label form__label" for="delivery-email">Email *</label>
                        <input type="text"
                               class="checkout-form__input form-field js-required"
                               id="delivery-email"
                               name="email">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="checkout-form__label form__label" for="delivery-phone">Телефон *</label>
                        <input type="text"
                               class="checkout-form__input form-field js-required"
                               id="delivery-phone"
                               placeholder="Например: 8-999-999-99-99"
                               name="phone">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="address-form__label form__label" for="delivery-country">Страна *</label>
                        <input type="text"
                               class="address-form__field form-field js-required"
                               id="delivery-country"
                               placeholder="Например: Россия"
                               value="Россия"
                               name="country">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="address-form__label form__label" for="delivery-city">Населенный пункт *</label>
                        <input type="text"
                               class="address-form__field form-field js-required"
                               id="delivery-city"
                               placeholder="Например: г.Барнаул"
                               name="city">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="address-form__label form__label" for="delivery-street">Улица, дом, кв. *</label>
                        <input type="text"
                               class="address-form__field form-field js-required"
                               id="delivery-street"
                               placeholder="Например: ул.Ленина 33, кв. 42"
                               name="street">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                    <div class="form__item">
                        <label class="address-form__label form__label" for="delivery-postcode">ИНДЕКС *</label>
                        <input type="text"
                               class="address-form__field form-field js-required"
                               id="delivery-postcode"
                               placeholder="Например: 666666"
                               name="postcode">
                        <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                        <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                    </div>
                </form> <!-- /.#delivery(form) -->
            <? } else { ?>
                <div class="checkout__data-wrapper js-checkout__data-wrapper">
                    <? ($accountInfo) ? include('php/views/parts/common/profile-data/account-data.php') : null ?>
                    <? ($addressInfo) ? include('php/views/parts/common/profile-data/address-data.php') : null ?>
                </div>
                <div class="checkout__data-edit-wrapper">
                    <label class="checkout__data-edit js-checkout__data-edit custom-checkbox">
                        <input type="checkbox" class="custom-checkbox__input visually-hidden">
                        <?= ($accountInfo || $addressInfo)
                            ? '<span class="custom-checkbox__text">Изменить мои данные для этого заказа</span>'
                            : '' ?>
                    </label>
                    <div class="checkout__data-edit js-checkout__data-edit-none is-reduce-mh">
                        Некоторые данные для автозаполнения отсутствуют, пожалуйста введите вручную:
                    </div>
                </div>
                <div class="checkout__form-wrapper-auth">
                    <form class="checkout__form checkout-form checkout-form--active
                        <?= ($accountInfo) ? "is-reduce-mh" : "" ?>" id="pickup">
                        <h3 class="checkout-form__title">Пожалуйста представьтесь</h3>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="pickup-first_name">Имя *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="pickup-first_name"
                                   name="first_name"
                                   value="<?= ($accountInfo) ? $user['first_name'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="pickup-last_name">Фамилия *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="pickup-last_name"
                                   name="last_name"
                                   value="<?= ($accountInfo) ? $user['last_name'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="pickup-middle_name">Отчество *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="pickup-middle_name"
                                   name="middle_name"
                                   value="<?= ($accountInfo) ? $user['middle_name'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="pickup-email">Email *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="pickup-email"
                                   name="email"
                                   value="<?= $user['email'] ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="pickup-phone">Телефон *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="pickup-phone"
                                   placeholder="Например: 8-999-999-99-99"
                                   name="phone"
                                   value="<?= ($accountInfo) ? $user['phone'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                    </form> <!-- /.#pickup(form) -->
                    <form class="checkout__form checkout-form
                     <?= ($addressInfo) ? "is-reduce-mh" : "is-delete" ?>" id="delivery">
                        <h3 class="checkout-form__title">Введите данные доставки</h3>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="delivery-first_name">Имя *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="delivery-first_name"
                                   name="first_name"
                                   value="<?= ($accountInfo) ? $user['first_name'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="delivery-last_name">Фамилия *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="delivery-last_name"
                                   name="last_name"
                                   value="<?= ($accountInfo) ? $user['last_name'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="delivery-middle_name">Отчество *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="delivery-middle_name"
                                   name="middle_name"
                                   value="<?= ($accountInfo) ? $user['middle_name'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="delivery-email">Email *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="delivery-email"
                                   name="email"
                                   value="<?= $user['email'] ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="checkout-form__label form__label" for="delivery-phone">Телефон *</label>
                            <input type="text"
                                   class="checkout-form__input form-field js-required"
                                   id="delivery-phone"
                                   placeholder="Например: 8-999-999-99-99"
                                   name="phone"
                                   value="<?= ($accountInfo) ? $user['phone'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="address-form__label form__label" for="delivery-country">Страна *</label>
                            <input type="text"
                                   class="address-form__field form-field js-required"
                                   id="delivery-country"
                                   placeholder="Например: Россия"
                                   value="Россия"
                                   name="country">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="address-form__label form__label" for="delivery-city">Населенный пункт *</label>
                            <input type="text"
                                   class="address-form__field form-field js-required"
                                   id="delivery-city"
                                   placeholder="Например: г.Барнаул"
                                   name="city"
                                   value="<?= ($addressInfo) ? $user['city'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="address-form__label form__label" for="delivery-street">Улица, дом, кв. *</label>
                            <input type="text"
                                   class="address-form__field form-field js-required"
                                   id="delivery-street"
                                   placeholder="Например: ул.Ленина 33, кв. 42"
                                   name="street"
                                   value="<?= ($addressInfo) ? $user['street'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                        <div class="form__item">
                            <label class="address-form__label form__label" for="delivery-postcode">ИНДЕКС *</label>
                            <input type="text"
                                   class="address-form__field form-field js-required"
                                   id="delivery-postcode"
                                   placeholder="Например: 666666"
                                   name="postcode"
                                   value="<?= ($addressInfo) ? $user['postcode'] : ""; ?>">
                            <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                            <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                        </div>
                    </form> <!-- /.#delivery(form) -->
                </div>
            <? } ?>
        </div> <!-- /.checkout__form-wrapper -->

        <div class="checkout__order-message order-message is-delete"></div>

        <div class="checkout__check check">
            <h3 class="check__title">Продукты</h3>
            <ul class="check__product-list"></ul> <!-- /.check__product-list -->
            <div class="check__subtotal check__line">
                <div class="check__subtotal-title">Предварительно</div>
                <div class="check__subtotal-wrap">
                    <span class="js-conventional-unit">₽</span>
                    <span class="check__subtotal-sum">0.00</span>
                </div>
            </div>
            <div class="check__shipping check__line is-reduce-mh">
                <div class="check__shipping-title">Доставка
                    <p class="check__delivery-weight js-delivery-weight"></p>
                </div>
                <div class="check__shipping-sum js-sum-delivery">₽ 0.00</div>
            </div>
            <div class="check__total check__line">
                <div class="check__total-title">Итого</div>
                <div class="check__total-wrap">
                    <span class="js-conventional-unit">₽</span>
                    <span class="check__total-sum">0.00</span>
                </div>
            </div>
            <div class="check__delivery delivery">
                <ul class="delivery__list">
                    <li class="delivery__item">
                        <label class="delivery__pickup custom-checkbox">
                            <input type="radio"
                                   class="custom-checkbox__input visually-hidden"
                                   name="delivery"
                                   value="pickup"
                                   checked>
                            <span class="custom-checkbox__text">Самовывоз</span>
                            <span class="delivery__about">Товары из заказа будут забронированы для Вас в
                                                течении 5 дней и ожидать по нашему адресу</span>
                        </label>
                    </li>
                    <li class="delivery__item">
                        <label class="delivery__delivery custom-checkbox">
                            <input type="radio"
                                   class="custom-checkbox__input visually-hidden"
                                   name="delivery"
                                   value="delivery">
                            <span class="custom-checkbox__text">Доставка</span>
                            <span class="delivery__about">Заказ будет обработан и отправлен по указанному
                                                Вами адресу доставки</span>
                        </label>
                    </li>
                </ul>
            </div> <!-- /.delivery -->
            <div class="check__about-policy">Ваши личные данные будут использоваться для обработки
                вашего заказа, поддержки вашего опыта на этом веб-сайте и для других целей, описанных в нашей
                <a href="#" class="check__policy-link js-policy-ref" target="_blank">политике конфиденциальности</a>.</div>
            <label class="check__policy custom-checkbox">
                <input type="checkbox" class="custom-checkbox__input visually-hidden">
                <span class="custom-checkbox__text">Я прочитал и согласен с условиями этого сайта</span>
            </label>
            <div class="check__to-checkout">
                <button type="submit"
                        class="check__place-order-btn btn-reset btn-default"
                        formaction="#"
                        formmethod="POST">Разместить заказ</button>
            </div>
        </div> <!-- /.checkout__check -->

    </div> <!-- /.checkout__content-wrapper -->
</section> <!-- /.checkout -->