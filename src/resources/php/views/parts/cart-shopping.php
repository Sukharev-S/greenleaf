<section class="tab-content__tab shopping <? echo $tab === 'shopping' ? 'tab-content__tab--active' : 'is-delete'?>" data-section="#shopping">
    <h2 class="visually-hidden">Корзина товаров</h2>
    <div class="shopping__messages messages">
        <div class="messages__item messages__undo">
            <div class="messages__undo-text-wrap">Продукт "<span class="messages__undo-text"></span>” удален.
                <button class="messages__undo-btn btn-reset">Вернуть?</button>
            </div>
        </div> <!-- /.messages__undo -->
        <div class="messages__item messages__empty is-reduce">
            <div class="messages__empty-text">Ваша корзина пока еще пуста.</div>
            <div class="messages__empty-return">
                <a href="/catalog/category/1" class="messages__empty-btn btn-reset btn-default">Вернуться в магазин</a>
            </div>
        </div> <!-- /.messages__empty -->
    </div> <!-- /.messages -->
    <div class="shopping__content-wrapper">
        <form class="shopping__form shopping-form" action="#">
            <div class="shopping-form__headers">
                <div class="shopping-form__thead shopping-form__thumbnail">Продукт</div>
                <div class="shopping-form__thead shopping-form__name"></div>
                <div class="shopping-form__thead shopping-form__price">Цена за ед.</div>
                <div class="shopping-form__thead shopping-form__quantity">Количество</div>
                <div class="shopping-form__thead shopping-form__subtotal">Итого</div>
                <div class="shopping-form__thead shopping-form__remove"></div>
            </div>
            <div class="shopping-form__cart-list"></div>
            <div class="shopping-form__actions">
                <div class="shopping-form__coupon coupon">
                    <input class="coupon__text" type="text" name="coupon-code" value="" placeholder="Код купона">
                    <input class="coupon__apply-btn btn-reset btn-default" type="submit" name="coupon-apply" value="Применить купон">
                    <div class="coupon__message is-reduce"></div>
                </div>
                <div class="shopping-form__continue">
                    <a href="/catalog/category/1" class="shopping-form__continue-btn btn-reset btn-default">Вернуться в магазин</a>
                </div>
            </div>
        </form> <!-- /.shopping-form -->
        <div class="shopping__check check">
            <h2 class="check__title">Расчет стоимости</h2>
            <div class="check__subtotal check__line">
                <div class="check__subtotal-title">Сумма</div>
                <div class="check__subtotal-wrap">
                    <span class="js-conventional-unit">₽</span>
                    <span class="check__subtotal-sum">0.00</span>
                </div>
            </div>
            <div class="check__shipping check__line">
                <div class="check__shipping-title">Доставка
                    <p class="check__delivery-weight js-delivery-weight"></p>
                </div>
                <div class="check__shipping-sum">Стоимость доставки рассчитывается при оформлении заказа.</div>
            </div>
            <div class="check__total check__line">
                <div class="check__total-title">Итого</div>
                <div class="check__total-wrap">
                    <span class="js-conventional-unit">₽</span>
                    <span class="check__total-sum">0.00</span>
                </div>
            </div>
            <div class="check__to-checkout">
                <a href="#" class="check__to-checkout-btn btn-reset btn-default">Продолжить оформление</a>
            </div>
        </div> <!-- /.shopping__check -->
    </div> <!-- /.shopping__content-wrapper -->
</section> <!-- /.shopping -->