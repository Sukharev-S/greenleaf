<section class="banners">
    <h2 class="visually-hidden">Banners</h2>
    <div class="container banners__container">
        <ul class="banners__list">

            <? foreach($categories as $category) { ?>
                <li class="banners__item">
                <article class="banners__article banners-article">
                    <a class="banners-article__ref" href="/catalog/category/<?=$category['id']?>"></a>
                    <img class="banners-article__img" src="<?=$category['main_img']?>" alt="<?=$category['name']?>">
                    <h3 class="banners-article__title"><?=$category['name']?></h3>
                    <span class="banners-article__about"><?=$category['desc_short']?></span>
                </article>
            </li>
            <? } ?>

        </ul>
    </div>
</section>