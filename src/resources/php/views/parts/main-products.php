<section class="main-products">
    <h2 class="visually-hidden">Товары greenleaf на главной странице</h2>
    <div class="container main-products__container">
        <ul class="main-products__nav">
            <li class="main-products__item">
                <button class="main-products__btn main-products__btn--current btn-reset main-link"
                        data-filter="best-sellers">Популярное</button>
            </li>
            <li class="main-products__item">
                <button class="main-products__btn btn-reset main-link"
                        data-filter="new-products">Новинки</button>
            </li>
            <li class="main-products__item">
                <button class="main-products__btn btn-reset main-link"
                        data-filter="sale-products">Акции</button>
            </li>
        </ul>
        <ul class="main-products__list products-grid"></ul>
        <div class="main-products__btn-center">
            <button class="btn-reset main-products__more">
                <span>Посмотреть еще</span>
                <svg>
                    <use xlink:href="/img/sprite.svg#down-arrow"></use>
                </svg>
            </button>
        </div>
    </div>
</section>