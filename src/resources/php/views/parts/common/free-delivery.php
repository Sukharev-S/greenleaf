<div class="free-delivery">
    <span class="free-delivery__text"><?=getHtmlElementInfo('free-delivery')['element_text']?></span>
    <button class="free-delivery__btn btn-reset">
        <svg class="free-delivery__btn-icon modal__close">
            <use xlink:href="/img/sprite.svg#close" aria-label="close"></use>
        </svg>
    </button>
</div> <!-- /.free-delivery -->