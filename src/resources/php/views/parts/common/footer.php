<footer class="footer">
	<div class="container footer__container">
		<div class="footer__content">
			<div class="footer__top">
				<div class="footer__column">
					<h3 class="footer__title">Клиентам</h3>
					<ul class="footer__list">
						<li class="footer__item"><a href="#" class="footer__link js-link-question">Задать вопрос</a></li>
						<li class="footer__item"><a href="#" class="footer__link js-link-callback">Заказать звонок</a></li>
						<li class="footer__item"><a href="/faq" class="footer__link">Вопросы и ответы</a></li>
					</ul>
				</div>
				<div class="footer__column">
					<h3 class="footer__title">Контакты</h3>
					<ul class="footer__list">
						<li class="footer__item">
							<a href="tel:+79236442429" class="footer__link">
								<strong>Телефон:</strong>
								<span class="footer__contact-data">+7 923 644 2429</span>
							</a>
						</li>
						<li class="footer__item">
							<a href="../../../../main.php" class="footer__link">
								<strong>Почта:</strong>
								<span class="footer__contact-data">melich.i@mail.ru</span>
							</a>
						</li>
						<li class="footer__item">
							<a href="https://yandex.ru/maps/-/CCUuY0qU3C" class="footer__link" target="_blank">
								<strong>Адрес:</strong>
								<span class="footer__contact-data">с.Кулунда пр.Мемориальный 16</span>
							</a>
						</li>
						<li class="footer__item"><strong>Время работы:</strong>
							<ul class="footer__schedule">
								<li>
									<strong>пн - сб:</strong>
									<span class="footer__contact-data">с 12:00 до 20:00</span>
								</li>
								<li>
									<strong>вс:</strong>
									<span class="footer__contact-data">с 14:00 до 18:00</span>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="footer__column footer__callback js-forms-callback">
                    <? include('php/views/parts/common/callback-message.php') ?>
					<h3 class="footer__title">Обратная связь</h3>
					<form action="#" class="footer__form footer-form footer-form--active" name="callback">
						<span class="footer-form__text">Хотите как можно быстрее что-то уточнить?</span>
						<span class="footer-form__text">Закажите обратный звонок и мы вам сразу же перезвоним!</span>
                        <div class="form__item">
                            <label class="footer-form__field">
                                <input class="form-field js-required"
                                       type="text"
                                       name="phone"
                                       placeholder="Ваш номер телефона">
                                <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                                <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                            </label>
                        </div>
                        <div class="form__item">
                            <label class="footer-form__field">
                                <input class="form-field js-required"
                                        type="text"
                                        name="full_name"
                                        placeholder="Как к Вам обращаться">
                                <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                                <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
                            </label>
                        </div>
						<div>
                            <button class="footer-form__btn btn-reset btn-default" type="submit">Заказать звонок</button>
                        </div>
					</form><!-- /.footer__form -->
					<form action="#" class="footer__form footer-form is-delete" name="question">
						<span class="footer-form__text">У вас появился вопрос?</span>
						<span class="footer-form__text">Напишите нам и мы вам ответим незамедлительно!</span>
						<textarea class="footer-form__question form-field js-required"
								  name="text"
								  placeholder="Задайте свой вопрос здесь"
								  rows="4"></textarea>
						<label class="footer-form__field">
							<input class="form-field js-required"
								   type="text"
								   name="email"
								   placeholder="Ваш Email">
						</label>
						<label class="footer-form__field">
							<input class="form-field js-required"
								   type="text"
								   name="full_name"
								   placeholder="Как к Вам обращаться">
						</label>
						<div><button class="footer-form__btn btn-reset btn-default" type="submit">Задать вопрос</button></div>
					</form><!-- /.footer__form -->
				</div>
			</div>
			<div class="footer__bottom">
				<div class="footer__left">
					<ul class="footer__list footer__list--row">
						<li class="footer__item">
                            <a href="#" class="footer__link footer__link--row js-policy-ref">Политика конфиденциальности</a>
                        </li>
						<li class="footer__item">
                            <a href="#" class="footer__link footer__link--row">Отследить ваш заказ</a>
                        </li>
					</ul>
				</div>
				<div class="footer__right">
                    <? include('php/views/parts/common/social-networks.php') ?>
				</div>
			</div>
		</div>
	</div>
    <? include('php/views/parts/common/copyright.php') ?>
</footer>
