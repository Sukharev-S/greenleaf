<div class="address-data <?=is_null($addressInfo) ? "waves" : "" ?>">
    <div class="address-data__item">
        <span class="address-data__label">Страна:</span>
        <span class="address-data__info" data-info-set="country"><?=$user['country'] ?></span>
    </div>
    <div class="address-data__item">
        <span class="address-data__label">Населенный пункт:</span>
        <span class="address-data__info" data-info-set="city"><?=$user['city'] ?></span>
    </div>
    <div class="address-data__item">
        <span class="address-data__label">Улица, дом, кв.:</span>
        <span class="address-data__info" data-info-set="street"><?=$user['street'] ?></span>
    </div>
    <div class="address-data__item">
        <span class="address-data__label">ИНДЕКС:</span>
        <span class="address-data__info" data-info-set="postcode"><?=$user['postcode'] ?></span>
    </div>
</div> <!-- /.address-data -->