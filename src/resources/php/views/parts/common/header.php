<header class="header <?=$mainUrl ? '' : 'catalog-header' ?>">
    <div class="container header__container <?=$mainUrl ? 'header-home-narrow' : 'container-narrow' ?>">
        <button tabindex="1" class="burger btn-reset header__burger">
            <span class="burger__line"></span>
        </button>
        <nav class="nav header__nav">
            <ul class="nav__list">
                <li class="nav__item">
                    <a href="/" class="main-link nav__link <?=$mainUrl ? 'nav__link--current' : '' ?>" data-url="main">Главная</a>
                </li>
                <li class="nav__item">
                    <a href="/catalog/category/1" class="main-link nav__link <?=$catalogUrl ? 'nav__link--current' : '' ?>" data-url="catalog">Магазин</a>
                </li>
                <? if ($cardUrl) { ?>
                    <li class="nav__item">
                        <a href="" class="main-link nav__link nav__link--current">Каталог</a>
                    </li>
                <? } ?>
                <li class="nav__item">
                    <a href="/company" class="main-link nav__link <?=$companyUrl ? 'nav__link--current' : '' ?>">О компании</a>
                </li>
                <li class="nav__item">
                    <a href="/cooperation" class="main-link nav__link <?=$cooperationUrl ? 'nav__link--current' : '' ?>">Сотрудничество</a>
                </li>
                <li class="nav__item">
                    <a href="/about" class="main-link nav__link <?=$aboutUrl ? 'nav__link--current' : '' ?>">О нас</a>
                </li>
            </ul>
        </nav>
<!--        <div class="header__logo">-->
<!--            <img src="/img/logo.png" alt="Логотип">-->
<!--        </div>-->
        <ul class="shop-nav header__shop-nav">
            <li class="shop-nav__item">
                <a href="#search" class="shop-nav__link shop-nav__link--search" aria-label="начать поиск">
                    <svg>
                        <use xlink:href="/img/sprite.svg#search"></use>
                    </svg>
                </a>
            </li>
            <li class="shop-nav__item shop-nav__user js-shop-nav__user">
                <? if ($user) {?>
                    <div class="shop-nav__link" aria-label="попап пользователя">
                        <svg>
                            <use xlink:href="/img/sprite.svg#user"></use>
                        </svg>
                    </div>
                    <span class="shop-nav__user-name">
                        <? if (is_null($user['first_name']) || is_null($user['last_name'])) {
                            echo $user['email'];
                        } else {
                            echo $user['first_name'] . ' ' . mb_substr($user['last_name'], 0, 1) . '.';
                        } ?></span>
                    <div class="custom-select__dropdown js-shop-nav__user--dropdown">
                        <ul class="custom-select__list">
                            <? if ($user['is_admin']) { ?>
                                <li class="custom-select__item"><a class="custom-select__item-link" href="/profile/admin-panel">Админ-панель</a></li>
                            <? } ?>
                            <li class="custom-select__item"><a class="custom-select__item-link" href="/profile/orders">Заказы</a></li>
                            <li class="custom-select__item"><a class="custom-select__item-link" href="/profile/address">Адрес</a></li>
                            <li class="custom-select__item"><a class="custom-select__item-link" href="/profile/account">Детали аккаунта</a></li>
                            <li class="custom-select__item"><a class="custom-select__item-link" href="/logout">Выход</a></li>
                        </ul>
                    </div>
                <? } else { ?>
                    <a href="#user" class="shop-nav__link shop-nav__link--user" aria-label="авторизоваться">
                        <svg>
                            <use xlink:href="/img/sprite.svg#user"></use>
                        </svg>
                    </a>
                <? } ?>
            </li>
            <li class="shop-nav__item">
                <a href="#cart" class="cart shop-nav__link" aria-label="открыть корзину">
                    <svg class="cart__icon">
                        <use xlink:href="/img/sprite.svg#cart"></use>
                    </svg>
                    <span class="cart__quantity quantity">0</span>
                </a>
                <div class="message-to-cart"></div>
            </li>
        </ul>

        <div class="overlay"></div>
        <? include('php/views/parts/modals/modal-nav.php') ?>
        <? include('php/views/parts/modals/modal-search.php') ?>
        <? include('php/views/parts/modals/modal-authorization.php') ?>
        <? include('php/views/parts/modals/modal-cart.php') ?>
        <? include('php/views/parts/modals/modal-policy.php') ?>
        <? include('php/views/parts/modals/modal-image.php') ?>
        <div class="js-global-loader is-delete">
            <? include('php/views/parts/common/loader.php') ?>
        </div>

    </div>
</header>