<div class="admin-panel__message">Текст успешно обновлен!</div>

<section class="tab-content__tab admin-panel <? echo $tab === 'admin-panel' ? 'tab-content__tab--active' : 'is-delete'?>" data-section="#admin-panel">
    <h2 class="visually-hidden">Админ панель</h2>
    <div class="profile-content__messages prof-messages">
        <div class="prof-messages__item">
            <div class="prof-messages__text">Панель администратора. Здесь можно редактировать данные элементов магазина</div>
        </div>
    </div> <!-- /.prof-messages -->
    <div class="admin-panel__main-choice main-choice">
        <div class="main-choice__option main-choice__category">
            <h3 class="main-choice__title">Категории</h3>
            <ul class="main-choice__list">
                <li class="main-choice__item custom-select__item" data-option="category" data-item="short_desc">Изменить краткое описание</li>
                <li class="main-choice__item custom-select__item" data-option="category" data-item="full_desc">Изменить полное описание</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="category" data-item="name_category">Изменить название категории</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="category" data-item="name_subcategory">Изменить название подкатегории</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="category" data-item="img_category">Изменить изображение категории</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="category" data-item="add_category">Добавить категорию</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="category" data-item="add_subcategory">Добавить подкатегорию</li>
            </ul>
        </div>
        <div class="main-choice__option main-choice__product">
            <h3 class="main-choice__title">Товары</h3>
            <ul class="main-choice__list">
                <li class="main-choice__item custom-select__item" data-option="product" data-item="one_product">Редактирование одного товара</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="product" data-item="all_products">Массовое редактирование товаров</li>
                <li class="main-choice__item custom-select__item" data-option="product" data-item="add_product">Добавить новый товар</li>
            </ul>
        </div>
        <div class="main-choice__option main-choice__promotion">
            <h3 class="main-choice__title">Реклама</h3>
            <ul class="main-choice__list">
                <li class="main-choice__item custom-select__item" data-option="promotion" data-item="free-delivery">Изменить бегущую строку сверху страницы</li>
                <li class="main-choice__item custom-select__item" data-option="promotion" data-item="cart-modal__ad">Изменить строку в модальном окне корзины</li>
            </ul>
        </div>
        <div class="main-choice__option main-choice__order">
            <h3 class="main-choice__title">Работа с заказами</h3>
            <ul class="main-choice__list">
                <li class="main-choice__item custom-select__item" data-option="order" data-item="change_status_order">Изменить статус заказа</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="order" data-item="active_orders">Показать активные заказы</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="order" data-item="closed_orders">Показать завершенные заказы</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="order" data-item="all_orders">Показать все заказы</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="order" data-item="pickup_ship_orders">Показать заказы с самовывозом</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="order" data-item="active_ship_orders">Показать заказы с активной доставкой</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="order" data-item="all_sell_products">Показать список всех проданых товаров</li>
                <li class="main-choice__item custom-select__item item-is-off" data-option="order" data-item="all_clients">Показать всех клиентов</li>
            </ul>
        </div>
    </div> <!-- /.main-choice -->
    <div class="admin-panel__content">

        <article class="adm-content is-reduce-mh" data-option="promotion">
            <div class="adm-content__row">
                <div class="adm-content__header adm-content__col-1">Элемент</div>
                <div class="adm-content__header adm-content__col-2">Текст</div>
                <div class="adm-content__header adm-content__col-3"></div>
            </div>
            <form class="adm-content__row js-promotion-form" action="#">
                <div class="adm-content__cell adm-content__col-1 js-promotion-element-name"></div>
                <div class="adm-content__cell adm-content__col-2">
                    <input class="adm-content__text js-promotion-element-text"
                           type="text"
                           name="text">
                </div>
                <div class="adm-content__cell adm-content__col-3">
                    <button class="btn-reset btn-default adm-content__btn" type="submit">Изменить</button>
                </div>
                <input class="js-promotion__id-element" type="hidden" name="id_element" value="">
            </form>
        </article> <!--/.adm-content promotion-->

        <article class="adm-content is-reduce-mh" data-option="category">
        </article> <!--/.adm-content category-->

        <article class="adm-content adm-product is-reduce-mh" data-option="product">

            <div class="adm-search is-reduce-mh js-adm-search-product">
                <h2 class="adm-search__title"> Поиск товара для редактирования</h2>
                <form class="adm-search__form js-adm-search-product__form">
                    <input class="adm-search__text js-adm-search-product__field" type="text" name="search_string">
                    <div class="adm-result__count-block  js-adm-result__count-block">Найдено товара:
                        <span class="adm-result__count js-adm-result__count"></span>
                    </div>
                    <div class="js-adm-search-product-loader is-delete">
                        <? include('php/views/parts/common/loader.php') ?>
                    </div>
                </form>
                <div class="adm-result adm-result__product">
                    <ul class="adm-result__product-list"></ul>
                </div>
            </div>

            <form class="adm-product__blanc blanc is-reduce" data-action>
                <div class="blanc__item blanc__title">
                    <input class="blanc__title-input blanc__input js-blanc-title-input js-blanc__name"
                           type="text"
                           name="name"
                           value="">
                </div>
                <div class="blanc__item blanc__active">
                    <label class="custom-checkbox blanc__active-label">
                        <input type="checkbox" name="is_active" class="custom-checkbox__input visually-hidden">
                        <span class="custom-checkbox__text blanc__active-text">Товар активен</span>
                    </label>
                </div>
                <div class="blanc__item blanc__similar">
                    <label class="blanc__similar-label blanc__label">Рекомендуем так же</label>
                    <div class="blanc__similar-form-add">
                        <input class="blanc__similar-input blanc__input js-blanc-similar-input" type="text">
                        <button class="blanc__similar-add-btn btn-reset btn-default">Добавить</button>
                    </div>
                    <ul class="blanc__similar-list"></ul>
                </div>
                <div class="blanc__item blanc__price">
                    <div class="blanc__label-wrap">
                        <label class="blanc__price-label blanc__label">Актуальная цена
                            <input type="text"
                                   name="price"
                                   class="blanc__price-input blanc__input js-blanc__price"
                                   value="">
                        </label>
                    </div>
                    <div class="blanc__label-wrap">
                        <label class="blanc__price-label blanc__label">Предыдущая цена
                            <input type="text"
                                   name="old_price"
                                   class="blanc__price-input blanc__input js-blanc__old-price"
                                   value="">
                        </label>
                    </div>
                </div>
                <div class="blanc__item blanc__exist">
                    <div class="blanc__label-wrap blanc__exist-stock">
                        <label class="blanc__exist-label blanc__label">В наличии, шт.
                            <input type="text"
                                   name="stock"
                                   class="blanc__exist-input blanc__input js-blanc__stock"
                                   value="">
                        </label>
                    </div>
                    <div class="blanc__label-wrap blanc__exist-order">
                        <label class="blanc__exist-label blanc__label">Заказано, шт.
                            <input type="text"
                                   name="quantity_ordered"
                                   class="blanc__exist-input blanc__input js-blanc__quantity_ordered"
                                   value="">
                        </label>
                    </div>
                    <div class="blanc__label-wrap blanc__exist-date">
                        <label class="blanc__exist-label blanc__label">Дата поступления, (дд.мм.гггг)
                            <input type="text"
                                   name="date_delivery"
                                   class="blanc__exist-input blanc__input js-blanc__date_delivery"
                                   value="">
                        </label>
                    </div>
                </div>
                <div class="blanc__item blanc__life">
                    <div class="blanc__label-wrap">
                        <label class="blanc__life-label blanc__label">Срок годности, мес.
                            <input type="text"
                                   name="life"
                                   class="blanc__life-input blanc__input js-blanc__life"
                                   value="">
                        </label>
                    </div>
                    <div class="blanc__label-wrap">
                        <label class="blanc__life-label blanc__label">В упаковке, шт.
                            <input type="text"
                                   name="package"
                                   class="blanc__life-input blanc__input js-blanc__package"
                                   value="">
                        </label>
                    </div>
                </div>
                <div class="blanc__item blanc__weight">
                    <div class="blanc__label-wrap">
                        <label class="blanc__weight-label blanc__label">Вес, гр.
                            <input type="text"
                                   name="weight"
                                   class="blanc__weight-input blanc__input js-blanc__weight"
                                   value="">
                        </label>
                    </div>
                    <div class="blanc__label-wrap">
                        <label class="blanc__weight-label blanc__label">Объем (жидкость), мл.
                            <input type="text"
                                   name="volume"
                                   class="blanc__weight-input blanc__input js-blanc__volume"
                                   value="">
                        </label>
                    </div>
                </div>
                <div class="blanc__item blanc__hwd">
                    <div class="blanc__label-wrap">
                        <label class="blanc__hwd-label blanc__label">Высота, мм.
                            <input type="text"
                                   name="height"
                                   class="blanc__hwd-input blanc__input js-blanc__height"
                                   value="">
                        </label>
                    </div>
                    <div class="blanc__label-wrap">
                        <label class="blanc__hwd-label blanc__label">Ширина, мм.
                            <input type="text"
                                   name="width"
                                   class="blanc__hwd-input blanc__input js-blanc__width"
                                   value="">
                        </label>
                    </div>
                    <div class="blanc__label-wrap">
                        <label class="blanc__hwd-label blanc__label">Глубина, мм.
                            <input type="text"
                                   name="depth"
                                   class="blanc__hwd-input blanc__input js-blanc__depth"
                                   value="">
                        </label>
                    </div>
                </div>
                <div class="blanc__item blanc__article">
                    <label class="blanc__article-label blanc__label">Артикль
                        <input type="text"
                               name="article"
                               class="blanc__article-input blanc__input js-blanc__article"
                               value="">
                    </label>
                    <label class="custom-checkbox blanc__article-label">
                        <input type="checkbox" name="is_popular" class="custom-checkbox__input visually-hidden">
                        <span class="custom-checkbox__text blanc__article-text">Популярный</span>
                    </label>
                    <label class="custom-checkbox blanc__article-label">
                        <input type="checkbox" name="is_new" class="custom-checkbox__input visually-hidden">
                        <span class="custom-checkbox__text blanc__article-text">Новинка</span>
                    </label>
                    <label class="custom-checkbox blanc__article-label">
                        <input type="checkbox" name="is_sale" class="custom-checkbox__input visually-hidden">
                        <span class="custom-checkbox__text blanc__article-text">Акционный</span>
                    </label>

                    <label class="blanc__article-label blanc__label">Индекс популярности
                        <input type="text"
                               name="index_popular"
                               class="blanc__article-input blanc__input js-blanc__index-popular"
                               value="0">
                    </label>
                </div>
                <div class="blanc__item blanc__categories">
                    <label class="blanc__categories-label blanc__label">Принадлежность к категории</label>
                    <ul class="blanc__categories-list">
                        <? foreach($categoriesArrDim as $category) { ?>
                            <li class="blanc__categories-item">
                                <div class="blanc__categories-name"><?=$category['name']?></div>
                                <ul class="blanc__categories-sublist" data-id-cat="<?=$category['id_category']?>">
                                    <? foreach($category['subCategory'] as $subcategory) { ?>
                                        <li class="blanc__categories-subitem">
                                            <label class="custom-checkbox blanc__categories-label">
                                                <input type="checkbox"
                                                       value="<?=$subcategory['id_category']?>"
                                                       class="custom-checkbox__input visually-hidden">
                                                <span class="custom-checkbox__text blanc__categories-text"><?=$subcategory['name']?></span>
                                            </label>
                                        </li>
                                    <? } ?>
                                </ul>
                            </li>
                        <? } ?>
                    </ul>
                </div>
                <div class="blanc__item blanc__short">
                    <label class="blanc__short-label blanc__label">Краткое описание товара</label>
                    <textarea class="blanc__longtext blanc__input js-blanc__short"
                              name="short"
                              rows="4"
                              wrap="soft"></textarea>
                </div>
                <div class="blanc__item blanc__description">
                    <label class="blanc__description-label blanc__label">Полное описание товара</label>
                    <textarea class="blanc__longtext blanc__input js-blanc__description"
                              name="description"
                              rows="4"
                              wrap="soft"></textarea>
                </div>
                <div class="blanc__item blanc__structure">
                    <label class="blanc__structure-label blanc__label">Состав</label>
                    <textarea class="blanc__longtext blanc__input js-blanc__structure"
                              name="structure"
                              rows="4"
                              wrap="soft"></textarea>
                </div>
                <div class="blanc__item blanc__recommendation">
                    <label class="blanc__recommendation-label blanc__label">Рекомендации</label>
                    <textarea class="blanc__longtext blanc__input js-blanc__recommendation"
                              name="recommendation"
                              rows="4"
                              wrap="soft"></textarea>
                </div>
                <div class="blanc__item blanc__scope">
                    <label class="blanc__scope-label blanc__label">Сфера применения</label>
                    <textarea class="blanc__longtext blanc__input js-blanc__scope"
                              name="scope"
                              rows="4"
                              wrap="soft"></textarea>
                </div>
<!--                <div class="blanc__item blanc__videos">Видео</div>-->
<!--                <div class="blanc__item blanc__photos">Фото</div>-->
<!--                <div class="blanc__item blanc__certificates">Сертификаты</div>-->
                <input class="js-blanc-id-product" name="id_product" type="hidden">
                <button class="blanc__save-btn btn-reset btn-default">Сохранить</button>
            </form><!--/.adm-product__blanc blanc -->

            <form class="adm-product__blanc-all">
<!--                <div class="adm-content__header adm-content__col-1">Название товара</div>-->
<!--                <div class="adm-content__header adm-content__col-2">Артикль</div>-->
<!--                <div class="adm-content__header adm-content__col-3">Цена</div>-->
<!--                <div class="adm-content__header adm-content__col-4">Старая цена</div>-->
<!--                <div class="adm-content__header adm-content__col-11">Кол-во в наличии</div>-->
<!--                <div class="adm-content__header adm-content__col-12">Кол-во заказано</div>-->
<!--                <div class="adm-content__header adm-content__col-13">Дата поставки</div>-->
<!--                <div class="adm-content__header adm-content__col-14">Срок годности</div>-->
<!--                <div class="adm-content__header adm-content__col-15">Вес</div>-->
<!--                <div class="adm-content__header adm-content__col-16">Обьем</div>-->
<!--                <div class="adm-content__header adm-content__col-17">В упаковке шт.</div>-->
<!--                <div class="adm-content__header adm-content__col-18">Высота</div>-->
<!--                <div class="adm-content__header adm-content__col-19">Ширина</div>-->
<!--                <div class="adm-content__header adm-content__col-20">Глубина</div>-->
<!--                <div class="adm-content__header adm-content__col-21">Популярный?</div>-->
<!--                <div class="adm-content__header adm-content__col-22">Новинка?</div>-->
<!--                <div class="adm-content__header adm-content__col-23">Акционный?</div>-->
<!--                <div class="adm-content__header adm-content__col-24">Индекс популярности</div>-->
<!--                <div class="adm-content__header adm-content__col-25">Активный?</div>-->

<!--                <div class="blanc-all__item blanc-all__title">-->
<!--                    <input class="blanc-all__title-input blanc__input js-blanc-all-title-input js-blanc-all__name"-->
<!--                           type="text"-->
<!--                           name="name"-->
<!--                           value="Моющее средство для кухни">-->
<!--                </div>-->
            </form>

        </article> <!--/.adm-content product -->

        <article class="adm-content adm-order is-reduce-mh" data-option="order">
            <div class="adm-search is-reduce-mh js-adm-search-order">
                <h2 class="adm-search__title"> Поиск заказа по номеру или клиенту</h2>
                <form class="adm-search__form js-adm-search-order__form">
                    <input class="adm-search__text js-adm-search-order__field" type="text" name="search_string">
                    <div class="adm-result__count-block js-adm-result__count-block">Найдено заказов:
                        <span class="adm-result__count js-adm-result__count"></span>
                    </div>
                    <div class="js-adm-search-order-loader is-delete">
                        <? include('php/views/parts/common/loader.php') ?>
                    </div>
                </form>
                <div class="adm-result adm-result__order">
                    <ul class="adm-result__order-list"></ul>
                </div>
            </div>
        </article> <!--/.adm-content order-->


    </div> <!-- /.admin-panel__content -->
</section> <!-- /.admin-panel -->