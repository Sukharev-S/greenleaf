<? include('php/views/parts/common/html-head.php') ?>

<? include('php/views/parts/common/free-delivery.php') ?>
<? include('php/views/parts/common/header.php') ?>

<main class="main cooperation--page">
    <section class="cooperation" id="cooperation">
        <div class="container cooperation__container">
            <h2 class="cooperation__title main-title page-title">Сотрудничество с компанией <?include('php/views/parts/common/logo-in-text.php')?></h2>

            <div class="cooperation__block cooperation__block-1">
                <div class="cooperation__text-wrap">
                    <div class="cooperation__text">
                        <p>Мощная производственная корпорация предлагает своим партнёрам участвовать в организации сети фирменных магазинов под брэндом <?include('php/views/parts/common/logo-in-text.php')?>. При этом у Вас есть возможность выбрать наиболее подходящий вид сотрудничества:</p>
                        <ul class="cooperation__type-list cooperation__list">
                            <li class="cooperation__type-item">открыть собственный эко-маркет (традиционный бизнес).</li>
                            <li class="cooperation__type-item">участвовать в построении партнёрской сети (сетевой маркетинг);</li>
                            <li class="cooperation__type-item">быть клиентом компании и пользоваться продукцией по ценам со скидкой 50%;</li>
                        </ul>
                        <div class="cooperation__image">
                            <img src="/img/cooperation/business_men.jpg" alt="бизнессмены">
                        </div>
                        <p>Для активных партнёров <?include('php/views/parts/common/logo-in-text.php')?> предоставляет большие возможности сделать карьеру в компании и получать доход с товарооборота сети: клиентов и фирменных магазинов. В сотрудничестве с компанией вы сможете зарабатывать 10-50 тысяч рублей в месяц и больше, а также экономить на потреблении товаров повседневного спроса, так как партнёрская цена на продукцию ниже цен массмаркета - 50% стоимости товара партнёры компании оплачивают купонами из личного кабинета на сайте.</p>
                        <p>Эко-продукция <?include('php/views/parts/common/logo-in-text.php')?> - это качество продукции премиум класса, имеются все международные сертификаты ISO, JMP, халяльные.</p>
                        <p>Доставка продукции: стартовые наборы рассылаются бесплатно по всему миру, по России последующие поставки на сумму от 40.000 рублей бесплатно.</p>
                        <p>Инновационный маркетинг-план: по всем позициям превосходит планы известных косметических и продуктовых компаний - 71% от мирового товарооборота компания отдаёт в сеть на выплату вознаграждений; действует накопительная система, в которой баллы не «сгорают», а ранги закрепляются навсегда; нет обязательного ежемесячного личного и группового товарооборота. Уже на первом уровне квалификации «Менеджер» ваш доход будет 100.000 - 250.000 рублей. Каждый год компанией проводится 3 промоушена на путешествия и 1 на получение автомобиля.</p>
                        <p>Ваши мечты могут стать реальностью в сотрудничестве с <?include('php/views/parts/common/logo-in-text.php')?>!<br> Присоединяйтесь к нашей команде. Мы готовы ответить на ваши вопросы и помочь Вам сделать первые шаги к цели.</p>
                    </div> <!-- /.cooperation__text -->
                </div> <!-- /.cooperation__text-wrap -->
            </div> <!-- /.cooperation__block -->

            <div class="cooperation__block cooperation-employer">
                <div class="cooperation__text-wrap">
                    <div class="cooperation__text">
                        <div class="cooperation__business">
                            <h3 class="cooperation__block-title">Традиционный предприниматель</h3>
                            <div class="cooperation__business-place">
                                <p>Откройте новый мини-маркет или торговую точку под брендом <?include('php/views/parts/common/logo-in-text.php')?>.</p>
                                <p>Или начните с витрины <?include('php/views/parts/common/logo-in-text.php')?>. Возможно вы откроете целую розничную сеть и сервисный центр(мелкий опт) в своем городе.</p>
                            </div> <!-- /.cooperation__create-place -->
                            <h4>Что такое бизнес с <?include('php/views/parts/common/logo-in-text.php')?> для предпринимателей:</h4>
                            <ul class="cooperation__business-list cooperation__list">
                                <li class="cooperation__business-item">Сотрудничество с крупным и надежным производителем</li>
                                <li class="cooperation__business-item">Широчайший ассортимент товаров первой необходимости премиум класса по очень привлекательным ценам</li>
                                <li class="cooperation__business-item">Законность и легальность бизнеса: компания заходит на рынок со всей необходимой документацией и сертификатами на продукцию, платит налоги в странах своего присутствия</li>
                                <li class="cooperation__business-item">Уникальная возможность для начинающих предпринимателей: можно начать бизнес с небольшим стартовым капиталом в 500-1000 долларов</li>
                                <li class="cooperation__business-item">Для крупных бизнесменов: возможность открытия сети брендовых магазинов <?include('php/views/parts/common/logo-in-text.php')?></li>
                            </ul>
                            <div>
                                <div class="cooperation__image">
                                    <img src="/img/cooperation/business_products.jpg" alt="витрины с товарами">
                                </div>
                                <p>Выбор товаров огромен - тысячи уникальных позиций по доступным ценам массмаркета. Предлагая продукцию <?include('php/views/parts/common/logo-in-text.php')?>, вы удовлетворите спрос широкого круга потребителей - каждый найдет свой товар.</p>
                                <p>Здесь нет постоянных взносов и роялти. Зато есть 3 вида дохода. И розничный только один из них.</p>
                                <p>Для того, чтобы подключиться к франчайзингу <?include('php/views/parts/common/logo-in-text.php')?> необходимо приобрести стартовый набор от компании. <strong>Стоимость набора 22 600 рублей.</strong></p>
                                <p>В этот набор (их 2 вида) включены топовые продукты от компании<?include('php/views/parts/common/logo-in-text.php')?>. С помощью этого набора вы можете ознакомиться с качеством продукции <?include('php/views/parts/common/logo-in-text.php')?>.</p>
                                <p><strong>Покупка стартового набора = покупка франшизы. </strong>Вам будут доступны дилерские цены. И вы сможете приобретать продукцию по партнерским ценам.</p>
                                <p>Как классический предприниматель, выплаты по маркетинг-плану вы можете рассматривать в виде приятных бонусов.</p>
                                <p>Более подробно о франшизе расскажем вам в личной беседе.</p>
                            </div>
                        </div> <!-- /.cooperation__business -->
                    </div> <!-- /.cooperation__text -->
                </div> <!-- /.cooperation__text-wrap -->
            </div> <!-- /.cooperation__block -->

            <div class="cooperation__block">
                <div class="cooperation__text-wrap">
                    <div class="cooperation__text">
                        <div class="cooperation__marketing">
                            <h3 class="cooperation__block-title">Преимущества маркетинга</h3>
                            <p><?include('php/views/parts/common/logo-in-text.php')?> только открывает европейские рынки и формирует лидерский состав.</p>
                            <p>Сейчас у вас есть уникальная возможность быть первым в своем городе!</p>
                            <p>Вы можете купить стартовый набор один раз и пользоваться отличной продукцией по цене производителя.</p>
                            <p>Кроме этого вы можете получать дополнительный пассивный доход, приобретая бизнес место в системе <?include('php/views/parts/common/logo-in-text.php')?>.</p>
                            <ul class="cooperation__list">
                                <li>Нет обязательного ежемесячного личного товарооборота.</li>
                                <li>Баллы накапливаются и не сгорают. </li>
                                <li>Работают авто бонус, жилищная программа, путешествия за счет компании</li>
                                <li>Квалификации подтверждать не требуется. Вышел на уровень - он твой.</li>
                                <li>Регулярные выплаты на карту вашего банка</li>
                                <li>Онлайн-платформа, которая помогает вам строить ваш бизнес</li>
                                <li>Промоушены на бесплатную поездку в Китай каждые полгода (торжественные встречи с руководителями, поощерение лидеров, экскурсии, посещение центра производства и флагманского магазина компании при штаб-квартире в Су Чжоу) </li>
                            </ul>
                            <p>Сетевая модель <?include('php/views/parts/common/logo-in-text.php')?> это бинарный маркетиг, который предполагает несколько бонусов и допускает только 2 регистрации в первую линию. Компенсационный план компании возвращает <strong>90 процентов прибыли</strong> от продуктов их дистрибьюторам. </p>
                            <p>Это значит, что один человек может зарегистрировать под себя только 2 человек, а остальные регистрируются под других людей в структуре.</p>
                            <div class="cooperation__image">
                                <img src="/img/cooperation/marketing_peoples.jpg" alt="общение менеджеров">
                            </div>
                            <p>Таким образом, если вы регистрируетесь под активного спонсора, то будут идти переливы от него (будет регистрировать под вас). Это может быть существенной поддержкой для новичков. В идеале если привлечь всего 2-х сильных партнеров, то можно выйти на существенный пассивный доход. <strong>Доход Менеджера - $1000 при структуре 80+80</strong></p>
                            <p>Сегмент MLM, готовился целых 5 лет. За эти годы специалисты <?include('php/views/parts/common/logo-in-text.php')?> изучали все сетевые компании. Они взяли лучшее, и убрали недостатки, которые мешают развиваться. Важно было создать не просто новую сетевую компанию, а создать ЛУЧШУЮ! </p>
                            <p>В настоящее время компания стремится к глобальному расширению и, следовательно, ищет лидеров в области продуктового бизнеса. И если вам подходит такое предложение, тогда пристегните ремни, пригласите как можно больше единомышленников, и добро пожаловать!</p>
                            <p>Используйте эту возможность, присоединяйтесь! Вы увидите, как начнет меняться ваша жизнь, как она начнет подниматься на новый уровень профессиональный, личностный и материальный!</p>
                        </div> <!-- /.cooperation__marketing -->
                    </div> <!-- /.cooperation__text -->
                </div> <!-- /.cooperation__text-wrap -->
            </div> <!-- /.cooperation__block -->

            <div class="cooperation__block">
                <div class="cooperation__text-wrap">
                    <div class="cooperation__text">
                        <div class="cooperation__benefits">
                            <h3 class="benefits__main-title cooperation__block-title">После регистрации вы получите</h3>
                            <ul class="benefits__list">
                                <li class="benefits__item">
                                    <div class="benefits__image">
                                        <span class="benefits__number">1</span>
                                        <img src="/img/cooperation/leaf-icon.png" alt="листочек">
                                    </div>
                                    <div class="benefits__text-wrap">
                                        <h5 class="benefits__title">Возможность</h5>
                                        <div class="benefits__text">приобретать товары мировых стандартов качества со скидкой до 50%</div>
                                    </div>
                                </li>
                                <li class="benefits__item">
                                    <div class="benefits__image">
                                        <span class="benefits__number">2</span>
                                        <img src="/img/cooperation/leaf-icon.png" alt="листочек">
                                    </div>
                                    <div class="benefits__text-wrap">
                                        <h5 class="benefits__title">Финансовую независимость</h5>
                                        <div class="benefits__text">повышение качества жизни, возможность строить свою карьеру</div>
                                    </div>
                                </li>
                                <li class="benefits__item">
                                    <div class="benefits__image">
                                        <span class="benefits__number">3</span>
                                        <img src="/img/cooperation/leaf-icon.png" alt="листочек">
                                    </div>
                                    <div class="benefits__text-wrap">
                                        <h5 class="benefits__title">Все привилегии</h5>
                                        <div class="benefits__text">подарки и бонусы, возможность путешествовать за счет компании</div>
                                    </div>
                                </li>
                                <li class="benefits__item">
                                    <div class="benefits__image">
                                        <span class="benefits__number">4</span>
                                        <img src="/img/cooperation/leaf-icon.png" alt="листочек">
                                    </div>
                                    <div class="benefits__text-wrap">
                                        <h5 class="benefits__title">Свободный график работы</h5>
                                        <div class="benefits__text">что позволяет строить планы по своему усмотрению</div>
                                    </div>
                                </li>
                                <li class="benefits__item">
                                    <div class="benefits__image">
                                        <span class="benefits__number">5</span>
                                        <img src="/img/cooperation/leaf-icon.png" alt="листочек">
                                    </div>
                                    <div class="benefits__text-wrap">
                                        <h5 class="benefits__title">Постоянную поддержку</h5>
                                        <div class="benefits__text">и обучение от личного наставника и команды</div>
                                    </div>
                                </li>

                            </ul>
                        </div> <!-- /.cooperation__benefits -->
                    </div> <!-- /.cooperation__text -->
                </div> <!-- /.cooperation__text-wrap -->
            </div> <!-- /.cooperation__block -->


        </div> <!-- /.cooperation__container -->
    </section> <!-- /.cooperation -->
</main>

<? include('php/views/parts/common/footer.php') ?>
<? include('php/views/parts/common/to-top.php') ?>

<? include('php/views/parts/common/html-end.php') ?>


