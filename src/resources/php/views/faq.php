<? include('php/views/parts/common/html-head.php') ?>

    <? include('php/views/parts/common/free-delivery.php') ?>
    <? include('php/views/parts/common/header.php') ?>
    <main class="main faq-page">
        <section class="faq" id="faq">
            <div class="container faq__container">
                <h2 class="faq__title main-title page-title">Вопросы и ответы</h2>
                <div class="faq__list">

                <? foreach ($faq as $faqItem) {?>
                    <article class="faq__item">
                        <div class="faq__item-header">
                            <h3 class="faq__item-title"><?= $faqItem['question'] ?></h3>
                            <div class="faq__item-arrow-wrap">
                                <svg class="faq__item-arrow">
                                    <use xlink:href="/img/sprite.svg#arrow_down"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="faq__item-answer">
                            <div class="faq__item-answer-wrap">
                                <p><?= $faqItem['answer'] ?></p>
                            </div>
                        </div>
                    </article>
                <? } ?>

                </div> <!-- /.faq__list -->
            </div> <!-- /.faq__container -->
        </section> <!-- /.faq -->
    </main>
    <? include('php/views/parts/common/footer.php') ?>
    <? include('php/views/parts/common/to-top.php') ?>

<? include('php/views/parts/common/html-end.php') ?>