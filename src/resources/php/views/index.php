<? include('php/views/parts/common/html-head.php') ?>

    <? include('php/views/parts/common/header.php') ?>
    <main class="main-index">
        <div class="hero-wrapper">
            <? include('php/views/parts/hero.php') ?>
        </div>
        <? include('php/views/parts/banners-categories.php') ?>
        <? include('php/views/parts/main-products.php') ?>
        <? include('php/views/parts/banner-greenleaf.php') ?>
    </main>
    <? include('php/views/parts/common/footer.php') ?>
    <? include('php/views/parts/common/to-top.php') ?>
    <?// include('php/views/parts/marketing.php') ?>

<? include('php/views/parts/common/html-end.php') ?>