<? include('php/views/parts/common/html-head.php') ?>

    <? include('php/views/parts/common/free-delivery.php') ?>
    <? include('php/views/parts/common/header.php') ?>
    <main class="main catalog-page">
        <? include('php/views/parts/hero-catalog.php') ?>
        <section class="catalog">
            <h2 class="visually-hidden">Catalog</h2>
            <? include('php/views/parts/catalog-categories.php') ?>
            <div class="catalog-content">
                <div class="container container-narrow">
                    <? include('php/views/parts/catalog-filter.php') ?>
                    <? include('php/views/parts/catalog-grid.php') ?>
                </div>
            </div>
        </section>
    </main>
    <? include('php/views/parts/common/footer.php') ?>
    <? include('php/views/parts/common/to-top.php') ?>

<? include('php/views/parts/common/html-end.php') ?>