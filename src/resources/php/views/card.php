<? include('php/views/parts/common/html-head.php') ?>

    <? include('php/views/parts/common/free-delivery.php') ?>
    <? include('php/views/parts/common/header.php') ?>
    <main class="main card-page">
        <section class="card">
            <h2 class="visually-hidden">Карточка товара</h2>
            <div class="card-content">
                <div class="card-content__top">
                    <? include('php/views/parts/card-top.php') ?>
                    <? include('php/views/parts/card-bottom.php') ?>
                </div>
            </div>
        </section>
    </main>
    <? include('php/views/parts/common/footer.php') ?>
    <? include('php/views/parts/common/to-top.php') ?>

<? include('php/views/parts/common/html-end.php') ?>