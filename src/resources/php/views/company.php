<? include('php/views/parts/common/html-head.php') ?>

<? include('php/views/parts/common/free-delivery.php') ?>
<? include('php/views/parts/common/header.php') ?>

<main class="main company--page">
    <section class="company" id="company">
        <div class="container company__container">
            <h2 class="company__title main-title page-title">Компания <?include('php/views/parts/common/logo-in-text.php')?></h2>

            <div class="company__block company-about">
                <div class="company__image">
                    <img src="/img/company/company.jpg" alt="Фото офиса с улицы">
                </div>
                <div class="company__text-wrap">
                    <div class="company__text">
                        <p>Компания Сучжоу Гринлиф Дэйли Коммодити Ко. (Suzhou Greenleaf Daily Commodity Co. Ltd ) расположена в индустриальном парке Шу Гуан в национальной высокотехнологичной зоне Сучжоу с уставным капиталом 22 миллиона долларов. Это современная высокотехнологичная группа, объединяющая научные исследования, производство, международное сотрудничество, продвижение бренда и маркетинг. Промышленный косметический парк научно-технической группы Гринлиф составляет общую инвестицию в размере 68 миллионов долларов и занимает площадь в 4800 квадратных метров, а общая площадь застройки составляет 106 800 квадратных метров.</p>
                        <p>Весь производственный центр был сертифицирован по международному стандарту ISO9001, GMP и допустимый стандарт производственного цеха составляет 100 000 и 10 000 разряды.</p>
                        <p>Производственная линия использует современное производственное оборудование из Германии, Франции и Италии, а также международную ведущую систему контроля качества, основываясь на независимых исследованиях и разработках химических продуктов за последние 20 лет, Научно-техническая группа Гринлиф постоянно наращивала темпы своего роста и постепенно превращалась в научного первопроходца науки китайской повседневной химической промышленности. Greenleaf успешно сотрудничает с международным итальянским косметическим гигантом INTERCOS Group и швейцарским CRB - Швейцарским национальным исследовательским центром по уходу за кожей. Научно-техническая группа Гринлиф осуществляла совместную деятельность в области передовых исследований и разработок в сфере международной косметической индустрии. Учитывая передовые международные научные исследования, технологии производства мирового класса и нормы строгой системы управления и контроля качества, производственный центр научно-технической группы Гринлиф запускает такие диверсифицированные серии продуктов, как SEALUXE, iLife, Kardli, Carich, Nilrich, Chanery, Green-Pai и Easy-Love, которые пользуются популярностью у потребителей.</p>
                        <p>16 марта 2016 года Министерство торговли Китайской Народной Республики официально выдало Гринлиф Сучжоу лицензию на прямой маркетинг. При существенной поддержке и содействии Партии и национального правительства, Гринлиф будет действовать в соответствии с законодательством и интенсивно развиваться, начиная свой процветающий путь.</p>
                    </div> <!-- /.company__text -->
                </div> <!-- /.company__text-wrap -->
            </div> <!-- /.company-about -->

            <div class="company__block company-cert">
                <ul class="company-cert__list">
                    <li class="company-cert__item">
                        <div class="company-cert__image">
                            <img class="js-image-zoom" src="/img/certifications/ISO9001_min.jpg" alt="Сертификат ISO9001">
                        </div>
                    </li>
                    <li class="company-cert__item">
                        <div class="company-cert__image">
                            <img class="js-image-zoom" src="/img/certifications/ISO14001_min.jpg" alt="Сертификат ISO14001">
                        </div>
                    </li>
                    <li class="company-cert__item">
                        <div class="company-cert__image">
                            <img class="js-image-zoom" src="/img/certifications/OHSAS18001_min.jpg" alt="Сертификат OHSAS18001">
                        </div>
                    </li>
                    <li class="company-cert__item">
                        <div class="company-cert__image">
                            <img class="js-image-zoom" src="/img/certifications/US_FDA_GMP_min.jpg" alt="Сертификат US_FDA_GMP">
                        </div>
                    </li>
                </ul>
            </div> <!-- /.company-cert -->

            <div class="company__video-wrap">
                <video class="company__video" controls>
                    <source src="/video/company_about-greenleaf.mp4" type="video/mp4">
                    Ваш браузер не поддерживает воспроизведение видео.</video>
            </div>

            <div class="company__block company-production">
                <div class="company__text-wrap">
                    <div class="company__text">
                        <h3 class="company__block-title">Центр производства</h3>
                        <div class="company__image">
                            <img src="/img/company/production-center.jpg" alt="Фото офиса с улицы">
                        </div>
                        <p>Производственный центр научно-технической группы Гринлиф - это комплексный производственный цех, построенный в усовершенствованной рамочной структуре, который в основном отвечает за разработку новых продуктов, производство, анализ проведенных испытаний, контроль качества, хранение сырья и продуктов в Индустриальном парке. Здание 5-этажного производственного центра длиной 206 метров с общей площадью застройки 40 тысяч квадратных метров. Оно включает в себя лаборатории, созданные по мировым стандартам, по производству косметических средств и товаров повседневного использования. Лаборатории содержат 5 автоматических линий по производству косметических средств и 4 линии по производству моющих средств. Стандарт всех лабораторий достиг разряда в 100 тысяч, из которых лаборатория по наполнению достигла 10000 класса, и она не имеет себе равных. </p>
                        <p>Все цеха по производству косметических средств являются важным инвестиционным проектом и оснащены современным производственным оборудованием и современной системой автоматического управления. Немецкая вакуумная гомогенная эмульгирующая машина EKATO и французская автоматическая герметизирующая машина KALIX, установленные в цехе эмульгирования, импортируются международными поставщиками оборудования и в настоящее время являются первоклассным производственным оборудованием в мировой косметической промышленности. В производственном центре специально создан двухуровневый закрытый воздушный канал длиной 300 метров, который обеспечивает удобное, интуитивное и комфортное посещение.</p>
                    </div> <!-- /.company__text -->
                </div> <!-- /.company__text-wrap -->
            </div> <!-- /.company__block -->

            <div class="company__block company-marketing">
                <div class="company__text-wrap">
                    <div class="company__text">
                        <h3 class="company__block-title">Центр маркетинга</h3>
                        <div class="company__image">
                            <img src="/img/company/marketing-center.jpg" alt="Фото офиса с улицы">
                        </div>
                        <p>Маркетинговый центр Грин Технолоджи Груп (Green Technology Group) находится на северной части административного центра. Маркетинговый центр является важным зданием первой постройки в промышленном парке Сучжоу Гринлиф. Он связан с административным центром лестничным проходом. Таким образом, это не только отдельный офисный район, но и связанное с административным центром здание, которое образует полный комплекс зданий, состоящий из администрации, маркетинга и стратегического развития. Здание Маркетингового центра представляет собой 6-этажное здание. На первом и втором этажах расположены рестораны быстрого питания и рестораны самообслуживания, которые являются немаловажным звеном для эффективной работы Индустриального парка. Третий и пятый этажи построены как полный комплекс современных средств бизнес-маркетинга, состоящие из офисов, комнат для деловых переговоров, и учебных залов. Шестой этаж выполнен в виде банкетного зала с банкетным боксом VIP и многофункциональным банкетным залом. Этот этаж является одним из важных мест для приема клиентов и проведения деловых ужинов.</p>
                        <p>Маркетинговый центр выполняет такие важные функции, как развитие бизнеса, маркетинговая стратегия, управление каналами и ведение деловых переговоров. Таким образом, это важная функциональная область для биологической индустрии красоты группы Гринлиф, которая должна завоевать свою долю рынка, занять целевой рынок и возглавить развитие этой отрасли.</p>
                    </div> <!-- /.company__text -->
                </div> <!-- /.company__text-wrap -->
            </div> <!-- /.company-marketing -->

            <div class="company__block company-info">
                <div class="company__text-wrap">
                    <div class="company__text">
                        <h3 class="company__block-title">Еще немного информации о <?include('php/views/parts/common/logo-in-text.php')?></h3>
                        <ul class="company__list">
                            <li>Компания снована в 1998. 18 лет работала только на внутренний китайский рынок</li>
                            <li>Общий объем инвестиций в индустриальный комплекс - 450 млн. юаней</li>
                            <li>Планируемый объем производства - 80 000 тонн продукции в год</li>
                            <li>Имеет международные контракты с компаниями и исследовательскими институтами в Швейцарии, Италии, Германии, Франции, Японии. В производстве используются технологии и оборудование ведущих фирм</li>
                            <li>Экопродукция повседневного спроса, более 3 500 наименований - огромный выбор</li>
                            <li>Премиум качество, цены массмаркета</li>
                            <li>Государственная поддержка Китая, получена лицензия на прямые продажи</li>
                            <li>Занимает 70 место в ТОП 100 мировых MLM компаний за 2018 год. Всего за 2 года - это прорыв в сетевой индустрии</li>
                        </ul> <!-- /.company__list -->
                    </div> <!-- /.company__text -->
                </div> <!-- /.company__text-wrap -->
            </div> <!-- /.company-info -->

        </div> <!-- /.company__container -->
    </section> <!-- /.company -->
</main>

<? include('php/views/parts/common/footer.php') ?>
<? include('php/views/parts/common/to-top.php') ?>

<? include('php/views/parts/common/html-end.php') ?>

