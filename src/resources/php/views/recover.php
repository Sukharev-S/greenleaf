<? include('php/views/parts/common/html-head.php') ?>

<div class="js-global-loader is-delete">
    <? include('php/views/parts/common/loader.php') ?>
</div>

<main class="recover-page">
    <div class="recover-page__left"><div> </div></div>
    <div class="recover-page__inner-wrapper">
        <div class="hero__content">
            <span class="hero__to-shop"><span class="hero__green">Green</span>Leaf</span>
        </div>

        <h1 class="recover-page__title">Восстановление пароля</h1>

        <? if ($isRecovery) { ?>
        <p class="recover-page__paragraph">Вводимый новый пароль должен содержать хотя бы одно число и состоять
            минимум из 8 символов.</p>
        <p class="recover-page__paragraph recover-page__text-delete">После завершения процедуры, Ваш старый пароль будет удален.</p>

            <form class="recover-page__form">
<!--            <h3 class="form__title">Сменить пароль</h3>-->
            <div class="form__item">
                <label class="form__label" for="new_password">Новый пароль</label>
                <input type="password"
                       class="form__input form-field js-required"
                       id="new_password"
                       name="new_password">
                <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
            </div>
            <div class="form__item">
                <label class="form__label" for="confirm_password">Подтвердите новый пароль</label>
                <input type="password"
                       class="form__input form-field js-required"
                       id="confirm_password"
                       name="confirm_password">
                <img class="form-field__not-ok is-reduce" src="/img/is-not-ok.png" alt="галочка - ошибка">
                <img class="form-field__ok is-reduce" src="/img/is-ok.png" alt="галочка - все ок">
            </div>
                <input type="hidden" name="hash" value="<?=$hash?>">
            <button type="submit"
                    class="form__edit-btn btn-reset btn-default js-recover-pass"
                    formaction="#"
                    formmethod="POST">Изменить
            </button>
            <div class="response-message is-reduce">Заполните поля формы</div> <!-- /.auth-message -->
        </form> <!-- /.recover-page__form -->
        <? } else {?>
        <div>
            <p class="recover-page__paragraph">К сожалению, данная ссылка повреждена или срок восстановления пароля уже истек.</p>
            <div class="recover-page__expired">
                <img src="/img/recover/expired.png" alt="срок истек">
            </div>
            <p class="recover-page__paragraph recover-page__text-replay">Пожалуйста, пройдите процедуру восстановления повторно.</p>
        </div>
        <? } ?>
    </div> <!-- /.recover-page__inner-wrapper -->
    <div class="recover-page__right"><div> </div></div>
</main>
    </div> <!-- /.site-container -->
    <script>
        // (?=.*[0-9]) - строка содержит хотя бы одно число
        // (?=.*[a-zA-Z]) - строка содержит хотя бы одну латинскую букву
        // [0-9a-zA-Z!@#$%^& *]{8,} - строка состоит не менее, чем из 8 символов

        // --- сброс и очищение всех полей формы от визуальных классов валидации
        const clearingFormFields = (form) => {
            let formFields = form.querySelectorAll('.form-field'),
                okIcon,
                NotOkIcon

            formFields.forEach(field => {
                if (field.closest('.form__item')) {
                    okIcon = field.closest('.form__item').querySelector('.form-field__ok')
                    NotOkIcon = field.closest('.form__item').querySelector('.form-field__not-ok')
                }
                field.classList.remove('error-field')
                field.classList.remove('success-field')
                if (NotOkIcon && okIcon) {
                    NotOkIcon.classList.add('is-reduce')
                    okIcon.classList.add('is-reduce')
                }
            })

            form.reset()
        }

        const validatePassword = (field) => {
            let password = field.value.trim()
            const re = /(?=.*[a-zA-Z])(?=.*[0-9])[0-9a-zA-Z_\W *]{8,}/
            return re.test(String(password).toLowerCase())
        }

        const validateConfirmPassword = (field) => {
            const newPassField = field.closest('form').querySelector('input[name="new_password"]')
            let password = field.value.trim()
            const re = /(?=.*[a-zA-Z])(?=.*[0-9])[0-9a-zA-Z_\W *]{8,}/
            return re.test(String(password).toLowerCase()) && password === newPassField.value
        }

        // --- валидация поля
        const isValidate = (field) => {
            if (field.value.length > 1) {
                if (field.name === 'password' || field.name === 'new_password' || field.name === 'current_password')
                    return validatePassword(field)
                if (field.name === 'confirm_password')
                    return validateConfirmPassword(field)
                return true
            } else return false
        }

        // --- визуализация поля на невалидность ввода
        const validateShowNot = (field) => {
            let okIcon, NotOkIcon
            if (field.closest('.form__item')) {
                okIcon = field.closest('.form__item').querySelector('.form-field__ok')
                NotOkIcon = field.closest('.form__item').querySelector('.form-field__not-ok')
            }
            field.classList.remove('success-field')
            field.classList.add('error-field')
            if (NotOkIcon && okIcon) {
                NotOkIcon.classList.remove('is-reduce')
                okIcon.classList.add('is-reduce')
            }
        }

        // --- визуализация поля на успешную валидность ввода
        const validateShowOk = (field) => {
            let okIcon, NotOkIcon
            if (field.closest('.form__item')) {
                okIcon = field.closest('.form__item').querySelector('.form-field__ok')
                NotOkIcon = field.closest('.form__item').querySelector('.form-field__not-ok')
            }
            field.classList.remove('error-field')
            field.classList.add('success-field')
            if (NotOkIcon && okIcon) {
                NotOkIcon.classList.add('is-reduce')
                okIcon.classList.remove('is-reduce')
            }
        }

        // --- проверяет валидность поля и подставляет нужные классы. Только для визуализации
        const validateShow = (field) => {
            isValidate(field) ? validateShowOk(field) : validateShowNot(field)
        }

        // ------- визуализация валидации при вводе
        document.querySelectorAll('.form-field').forEach(el => {
            el.addEventListener('input', e => {
                validateShow(e.target)
            })
        })

        if (document.querySelector('.recover-page')) {
            const
                responseMessage = document.querySelector('.recover-page__form .response-message'),
                loader = document.querySelector('.js-global-loader'),
                FILL_ALL_FORM_FIELDS = 'Заполните все поля формы',
                editPasswordForm = document.querySelector('.recover-page__form')

            // для чистоты кода, работа с сообщением и лоадером
            const getResponseMessage = (text, responseMessage) => {
                responseMessage.classList.remove('is-reduce')
                setTimeout(() => responseMessage.classList.add('is-reduce'), 5000)
                responseMessage.textContent = text
            }

            if (editPasswordForm) {
                // submit кнопки изменения пароля на странице восстановления
                editPasswordForm.addEventListener('submit', e => {
                    e.preventDefault()
                    let formFields = document.querySelectorAll('.form-field'),
                        countEmptyFields = 0,
                        countErrorFields = 0
                    loader.classList.remove('is-delete')
                    formFields.forEach(field => {
                        if (field.value === '') countEmptyFields++
                        if (!isValidate(field)) countErrorFields++
                    })
                    if (countEmptyFields !== 0) {       // если хоть одно поле не заполнено
                        loader.classList.add('is-delete')
                        responseMessage.classList.remove('response-message--success')
                        formFields.forEach(field => validateShow(field))
                        getResponseMessage(FILL_ALL_FORM_FIELDS, responseMessage)
                    } else {
                        let formData = new FormData(editPasswordForm),
                            newPass = formData.get('new_password'),
                            confirmPass = formData.get('confirm_password')
                        if (newPass !== confirmPass) {      // если новый пароль и подтвержденный не совпали
                            loader.classList.add('is-delete')
                            responseMessage.classList.remove('response-message--success')
                            getResponseMessage('Подтвержденный пароль не совпадает', responseMessage)
                        } else {
                            if (countErrorFields === 0) {       // если пароли введены по правилам верификации
                                fetch("/change-password", {
                                    method: "POST",
                                    body: formData
                                })
                                    .then((response) => response.json())
                                    .then((result) => {
                                        setTimeout(() => {
                                            if (result['is_ok']) {
                                                responseMessage.classList.add('response-message--success')
                                                clearingFormFields(editPasswordForm)
                                            } else {
                                                responseMessage.classList.remove('response-message--success')
                                            }
                                            loader.classList.add('is-delete')
                                            getResponseMessage(result['message'], responseMessage)
                                            setTimeout(() => {
                                                window.location.href = '/profile'
                                            }, 3000)
                                        }, 1000)
                                    }).catch(err => console.error(err))
                            } else {
                                loader.classList.add('is-delete')
                                getResponseMessage('Пароль должен содержать хотя бы одно число и состоять минимум из 8 символов', responseMessage)
                                responseMessage.classList.remove('response-message--success')
                                formFields.forEach(field => validateShow(field))
                            }
                        }
                    }
                })
            }
        }
    </script>

</body>
</html>