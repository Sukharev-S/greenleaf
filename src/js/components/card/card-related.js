import vars from "../../_vars";
import {fetchProducts, renderProducts} from "../../functions/productActions";
import {showElements} from "../../functions/animate";
import Swiper from "../../vendor/swiper.min";
import {controlLoadElement} from "../../functions/controlLoadElement";
import {disableProducts} from "../../functions/productActions";

let quantityProductShown = 10   // кол-во выводимых продуктов в слайдере

if (vars.$cardRelatedSlider) {
    //  заполнение товарами блок .card-related
    let id_targetProduct = vars.$cardRelatedList.dataset.targetSimilarId
    let url = '/similar/product/' + id_targetProduct
    fetchProducts(url)
        .then((data) => {
            return renderProducts(data, vars.$cardRelatedList, 'swiper-slide card-related__item', 0, quantityProductShown)
        })
        .then((goods) => {
            // только для страницы card, инициация слайдера товаров на новых DOM элементах
            const relatedSlider = new Swiper(vars.$cardRelatedSlider, {
                loop: false,
                slidesPerView: 3,
                allowTouchMove: true,
                spaceBetween: 0,
                pagination: {
                    el: '.related-pag',
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    1400: {
                        slidesPerView: 5,
                    },
                    1200: {
                        slidesPerView: 5,
                    },
                    1024: {
                        slidesPerView: 4,
                    },
                    768: {
                        slidesPerView: 3,
                    },
                    576: {
                        slidesPerView: 2,
                    },
                    320: {
                        slidesPerView: 1,
                    }
                }
            })
            return Promise.all(goods.map(controlLoadElement))
        })
        .then((images) => {
            let newProducts = images.map(el => el.closest('.card-related__item'))
            disableProducts()
            showElements(newProducts, 100, 'emergence')
        }).catch(err => console.error(err))
}
