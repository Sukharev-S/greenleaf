import vars from '../../_vars'
import {productionPrice, priceWithoutSpaces, printTotalPrice} from '../../functions/priceActions'
import {getTotalPrice, getTotalQuantity} from '../../functions/calculate'
import {controlLoadElement} from '../../functions/controlLoadElement'
import {stepper} from '../../functions/stepper'
import {isValidate, validateShow, validateShowNot} from '../../functions/validation'
import {hideElement, hideElementHard, removeElements, showElements, showHiddenElement} from '../../functions/animate'
import {modalWindowOn} from "../../functions/modalWindows";
import {getProductsFromLocalStorage} from "../../functions/productActions";

if (document.querySelector('.cart-page')) {
    const
        DEFAULT_CURRENCY = '₽',
        ACCEPT_TERMS = 'Прочтите и примите условия, чтобы продолжить оформление заказа',
        DELAY_ORDER_MESSAGE = 1200,
        messageUndo = vars.$cartContent.querySelector('.messages__undo'),
        messageEmpty = vars.$cartContent.querySelector('.messages__empty'),
        messageUndoText = messageUndo.querySelector('.messages__undo-text'),
        messageUndoBtn = messageUndo.querySelector('.messages__undo-btn'),
        messageError = vars.$cartContent.querySelector('.messages__error'),
        shoppingForm = vars.$cartContent.querySelector('.shopping__form'),
        shoppingCheck = vars.$cartContent.querySelector('.shopping__check'),
        cartProductsList = vars.$cartContent.querySelector('.shopping-form__cart-list'),
        checkProductList = vars.$cartContent.querySelector('.check__product-list'),
        subtotalSum = vars.$cartContent.querySelectorAll('.check__subtotal-sum'),
        shippingSum = vars.$cartContent.querySelector('.check__shipping-sum'),
        totalSum = vars.$cartContent.querySelectorAll('.check__total-sum'),
        totalQuantityProducts = vars.$cartNavigationList.querySelector('.cart-navigation__link-quantity'),
        placeOrderBtn = vars.$cartContent.querySelector('.check__place-order-btn'),
        formCheckoutPickup = vars.$cartContent.querySelector('#pickup'),
        formCheckoutDelivery = vars.$cartContent.querySelector('#delivery'),
        deliveryBlock = vars.$cartContent.querySelector('.delivery'),
        checkboxDelivery = deliveryBlock.querySelector('input[value="delivery"]'),
        checkoutSumDelivery = vars.$cartContent.querySelector('.js-sum-delivery'),
        postCodeField = vars.$cartContent.querySelector('#delivery-postcode'),
        checkboxDataEdit = vars.$cartContent.querySelector('.js-checkout__data-edit input'),
        weightDeliveryDisplay = vars.$cartContent.querySelectorAll('.js-delivery-weight'),
        checkoutMessages = vars.$cartContent.querySelector('.checkout__messages'),
        checkoutCheck = vars.$cartContent.querySelector('.checkout__check'),
        checkoutFormWrapper = vars.$cartContent.querySelector('.checkout__form-wrapper'),
        checkoutOrderMessage = vars.$cartContent.querySelector('.checkout__order-message'),
        tabNavigationOrder = vars.$cartNavigationList.querySelector('[href="#checkout"]')
    let
        currentCheckoutForm = formCheckoutPickup,
        shippingSumValue = parseFloat(shippingSum.textContent) || 1 ? 0 : parseFloat(shippingSum.textContent).toFixed(2),
        localStore = JSON.parse(localStorage.getItem('cart')) || [],
        deletedProduct = [],
        heightCartItem = 0,
        heightProductsList = 0,
        priceDelivery = 0,
        weightDelivery = 0,
        notesDeliveryForAdmin = '',
        lastValueCheckoutSumDelivery = '₽ 0.00',
        isUserAuthorized = vars.$cartContent.querySelector('.js-checkout__data-wrapper')

    // анимация появления скрытого элемента на экране средствами GSAP
    const showHiddenElementGSAP = (element) => {
        gsap.to(element, {
            duration: 0.1,
            autoAlpha: 1,
            ease: "sine.out",
        })
        gsap.to(element, {
            duration: 0.5,
            height: "auto",
            ease: "sine.out",
        })
    }
    // анимация скрытия элемента с экрана средствами GSAP
    const hiddenElementGSAP = (element) => {
        gsap.to(element, {
            duration: 0.5,
            height: 0,
            autoAlpha: 0,
            ease: "sine.out",
        })
    }

    //  html разметка товара в главной корзине
    const htmlCartItem = function(currency, id, img, title, priceString, quantity, priceAll) {
        return `
        <article class="shopping-form__cart-item cart-item" data-id="${id}">
            <div class="cart-item__thumbnail">
                <button class="cart-item__remove--mobile btn-reset" type="button">
                    <svg class="cart-item__remove-icon">
                        <use xlink:href="/img/sprite.svg#close" aria-label="remove a product from your cart"></use>
                    </svg>
                </button>
                <a class="cart-item__image-wrapper" href="/product/${id}">
                    <img src="${img}" alt="product's picture" loading="lazy">
                </a>
            </div>
            <div class="cart-item__name">
                <a href="#" class="cart-item__name-ref">${title}</a>
            </div>
            <div class="cart-item__price">
                <span class="js-conventional-unit">${currency}</span>
                <span class="cart-item__amount">${priceString}</span>
            </div>
            <div class="cart-item__quantity">
                <div class="cart-item__stepper stepper">
                    <button type="button" class="btn-reset stepper__btn stepper__btn--minus" aria-label="minus">
                        <svg class="">
                            <use xlink:href="/img/sprite.svg#minus"></use>
                        </svg>
                    </button>
                    <input type="text" class="stepper__input" value="${quantity}" maxlength="5">
                    <button type="button" class="btn-reset stepper__btn stepper__btn--plus" aria-label="plus">
                        <svg class="">
                            <use xlink:href="/img/sprite.svg#plus"></use>
                        </svg>
                    </button>
                </div> <!-- /.stepper -->
            </div>
            <div class="cart-item__subtotal">
                <span class="js-conventional-unit">${currency}</span>
                <span class="cart-item__items-sum">${priceAll}</span>
            </div>
            <div class="cart-item__remove">
                <button class="cart-item__remove-btn btn-reset" type="button">
                    <svg class="cart-item__remove-icon">
                        <use xlink:href="/img/sprite.svg#close" aria-label="remove a product from your cart"></use>
                    </svg>
                </button>
            </div>
		</article> <!-- /.cart-item -->
		`
   }

    //  html разметка товара в чеклисте
    const htmlCheckListItem = function(currency, title, quantity, priceAll) {
        return `
            <li class="check__list-item">
                <div class="check__product-name">${title}</div>
                <div class="check__product-quantity">x ${quantity}</div>
                <div class="check__product-price">
                    <span class="js-conventional-unit">${currency}</span>
                    <span class="check__price-sum">${priceAll}</span>
                </div>
            </li>
        `
    }

    //  html разметка сообщения об ошибке валидации полей
    const htmlMessageErrorItem = function(textField) {
        return `
                <li class="messages__error-item">
                    <span class="messages__error-text">${textField}</span>
                </li>
            `
    }

    //  html разметка сообщения об успешном заказе
    const htmlOrderMessage = function(order) {
        let productList = ''
        let deliveryInfo = ''
        if (Object.keys(order['delivery_info']).length > 0) {
            deliveryInfo = `
                <div class="order-message__line-details">
                    <div class="order-message__line-item">
                        <span class="order-message__item-title">Страна: </span>
                        ${order['delivery_info']['country']}
                    </div>
                    <div class="order-message__line-item">
                        <span class="order-message__item-title">Населенный пункт: </span> 
                        ${order['delivery_info']['city']}
                    </div>
                    <div class="order-message__line-item">
                        <span class="order-message__item-title">Улица, дом, кв.: </span>
                        ${order['delivery_info']['street']}
                    </div>
                    <div class="order-message__line-item">
                        <span class="order-message__item-title">ИНДЕКС: </span> 
                        ${order['delivery_info']['postcode']}
                    </div>
                </div>
            `
        }
        order['products_info'].forEach(el => {
            productList += `
                <li class="order-details__product-item">
                    <a class="order-details__product-name" href="/product/${el['id']}" target="_blank">${el['title']}</a>
                    <div class="order-details__product-quantity">x ${el['quantity']}</div>
                    <div class="order-details__product-price">
                    <span class="js-conventional-unit">₽</span>
                    <span class="order-details__price-sum">${productionPrice((el['sum_rub'].toFixed(2)))}</span>
                   </div>
                </li>
            `
        })
        return `
            <div class="order-message__text">
                Заказ принят и уже обрабатывается. Спасибо за то что Вы с нами!
                <svg class="order-message__text-heart">
                    <use xlink:href="/img/sprite.svg#heart"></use>
                </svg>
            </div>
            <div class="order-message__wrap-details">
                <div class="order-message__line-details">
                    <div class="order-message__line-item">
                        <span class="order-message__item-title">Номер заказа:</span> ${order['id_order']}
                    </div>
                    <div class="order-message__line-item">
                        <span class="order-message__item-title">Дата:</span> ${order['date_placed']}
                    </div>
                    <div class="order-message__line-item">
                        <span class="order-message__item-title">Итоговая сумма:</span>
                         ₽ ${productionPrice(order['sum_total_rub'].toFixed(2))}
                    </div>
                    <div class="order-message__line-item">
                        <span class="order-message__item-title">Метод доставки:</span> 
                        ${order['method_delivery'] === 'pickup' ? 'Самовывоз' : 'Почтовая служба'}
                    </div>
                </div>
                ${deliveryInfo}
                <div class="order-message__order-details order-details">
                    <h3 class="order-details__title">Детали заказа</h3>
                    <div class="order-details__wrap-details">
                        <div class="order-details__sub-title">Заказанные товары</div>
                        <ul class="order-details__product-list">
                            ${productList}
                        </ul> <!-- /.check__product-list -->
                        <div class="order-details__line order-details__total-sum">
                            <div class="order-details__sum-wrap">
                                <span class="js-conventional-unit">₽</span>
                                <span class="order-details__sum">
                                    ${productionPrice((order['sum_total_rub'] - order['sum_delivery']).toFixed(2))}
                                </span>
                            </div>
                        </div>
                        <div class="order-details__line">
                            <div class="order-details__sub-title">Доставка</div>
                            <div class="order-details__sum-wrap">
                                <span class="js-conventional-unit">₽</span>
                                <span class="order-details__sum">
                                    ${productionPrice(parseInt(order['sum_delivery']).toFixed(2))}
                                </span>
                            </div>
                        </div>
                        <div class="order-details__line">
                            <div class="order-details__sub-title">Итого</div>
                            <div class="order-details__sum-wrap">
                                <span class="js-conventional-unit">₽</span>
                                <span class="order-details__sum">
                                    ${productionPrice(order['sum_total_rub'].toFixed(2))}
                                </span>
                            </div>
                        </div>
                    </div> <!-- /.order-details__wrap-details -->
                </div> <!-- /.order-message__order-details order-details -->
            </div> <!-- /.order-message__wrap-details -->
        `
    }

    //  удаление из главной корзины выбранного товара
    const deleteCartItem = (product) => {
        let id = product.dataset.id
        localStore = JSON.parse(localStorage.getItem('cart')) || []
        localStore.forEach((a, i) => {
            if (a.id === id) deletedProduct = localStore.splice(i, 1)
        })
        localStorage.setItem('cart', JSON.stringify(localStore))
        product.classList.add('is-reduce')
        removeElements([product], 100 , 'emergence')
            .then(() => {
                totalQuantityProducts.textContent = getTotalQuantity()
                subtotalSum.forEach(el => printTotalPrice(el))
                totalSum.forEach(el => el.textContent = productionPrice((getTotalPrice() + shippingSumValue).toFixed(2)))
                cartProductsList.style.height = heightCartItem * localStore.length + 'px'
                controlEmptyCart(localStore.length)
                messageUndoText.textContent = deletedProduct[0].title
                showHiddenElementGSAP(messageUndo)
            }).catch(err => console.error(err))
        renderCheckListItems(localStore)
        calculatePriceDelivery()
    }

    //  контроль наличия товаров в главной корзине на пустоту
    const controlEmptyCart = (quantity) => {
        if (quantity === 0) {
            shoppingForm.classList.add('is-reduce')
            shoppingCheck.classList.add('is-reduce')
            setTimeout(() => messageEmpty.classList.remove('is-reduce'), 10)
            totalQuantityProducts.textContent = '0'
            vars.$cartNavigationList.querySelector('[href="#checkout"]').classList.add('is-disabled')
        } else {
            messageEmpty.classList.add('is-reduce')
            setTimeout(() => {
                shoppingForm.classList.remove('is-reduce')
                shoppingCheck.classList.remove('is-reduce')
            }, 510)
            vars.$cartNavigationList.querySelector('[href="#checkout"]').classList.remove('is-disabled')
        }
    }

    //  функция для обработчика событий на stepper продуктах внутри главной корзины
    const stepperEvent = e => {
        const product = e.target.closest('.cart-item')
        const itemsSum = product.querySelector('.cart-item__items-sum')
        let inputValue = parseInt(product.querySelector('.stepper__input').value),
            price = parseFloat(priceWithoutSpaces(product.querySelector('.cart-item__amount').textContent)),

        localStore = JSON.parse(localStorage.getItem('cart')) || []
        let idProduct = product.dataset.id
        localStore.forEach(a => {
            if (a.id === idProduct) {
                a.quantity = inputValue
                // itemsSum.dataset.usdValue = (inputValue * a.priceUsdValue).toFixed(2)
            }
        })
        localStorage.setItem('cart', JSON.stringify(localStore))

        totalQuantityProducts.textContent = getTotalQuantity()
        subtotalSum.forEach(el => printTotalPrice(el))
        totalSum.forEach(el => el.textContent = productionPrice((getTotalPrice() + shippingSumValue).toFixed(2)))
        itemsSum.textContent = productionPrice((inputValue * price).toFixed(2)).toString()
        renderCheckListItems(localStore)
        calculatePriceDelivery()
    }

    //  функция для обработчика событий изменения checkBox блока доставки
    let eventChange = new Event('change') // создание события для программного вызова метода change на checkbox js-checkout__data-edit
    const onChangeDelivery = (hiddenForm, shownForm) => {
        deliveryBlock.style.pointerEvents = 'none'
        // для width < 1200 экран прокручивается на блок с информацией пользователя(если он авторизован) или на начало формы
        if (window.innerWidth <= 1200) {
            window.scrollTo({
                top: -100 + window.pageYOffset + document.querySelector('.checkout__content-wrapper').getBoundingClientRect().top,
                behavior: "smooth",
            })
        }
        hideElement(hiddenForm, 'checkout-form--active')
            .then(() => {
                showHiddenElement(shownForm, 'checkout-form--active')
                deliveryBlock.style.pointerEvents = 'auto'
            })
        currentCheckoutForm = shownForm
        // если пользователь авторизован и данный чекбокс сгенерирован php
        if (isUserAuthorized) {
            checkboxDataEdit.checked = false    // сброс чекбокса js-checkout__data-edit в начальную позицию (визуально)
            checkboxDataEdit.dispatchEvent(eventChange) // сброс чекбокса js-checkout__data-edit в начальную позицию (программно)
            // смена отображения текста и чекбокса при наличии данных у пользователя
            let labelData = vars.$cartContent.querySelector('.js-checkout__data-edit')
            let labelDataNone = vars.$cartContent.querySelector('.js-checkout__data-edit-none')
            if ((!vars.$cartContent.querySelector('.address-data') && (currentCheckoutForm === formCheckoutDelivery))
            || (!vars.$cartContent.querySelector('.account-data') && (currentCheckoutForm === formCheckoutPickup))) {
                labelData.classList.add('is-reduce-mh')
                setTimeout(() => labelDataNone.classList.remove('is-reduce-mh'), 750)
            } else {
                labelDataNone.classList.add('is-reduce-mh')
                setTimeout(() => labelData.classList.remove('is-reduce-mh'), 750)
            }
        }
        resetErrorFields(currentCheckoutForm.querySelectorAll('.form__item input'))
        // изменения суммы доставки в чеке при переключении метода доставки
        if (currentCheckoutForm === formCheckoutDelivery) {
            if (postCodeField.value.length < 6) {
                checkoutSumDelivery.textContent = 'Введите почтовый ИНДЕКС, чтобы узнать стоимость доставки'
            } else {
                shippingSumValue = priceDelivery
                checkoutSumDelivery.textContent = lastValueCheckoutSumDelivery
                shippingSum.textContent = lastValueCheckoutSumDelivery
            }
        } else {
            shippingSumValue = 0
            checkoutSumDelivery.textContent = '₽ 0.00'
            shippingSum.textContent = '₽ 0.00'
        }
        totalSum.forEach(el => el.textContent = productionPrice((getTotalPrice() + shippingSumValue).toFixed(2)))
        document.querySelector('.checkout__check .check__shipping').classList.toggle('is-reduce-mh')
    }

    //  рендер товаров для главной корзины из localstorage
    const renderCartItems = (products) => {
        let newImagesArray = []
        let currency = DEFAULT_CURRENCY
        return new Promise(resolve => {
            products.forEach(a => {
                let price = productionPrice(a.priceNumber),
                    priceAll = productionPrice((a.priceNumber * a.quantity).toFixed(2))
                cartProductsList.insertAdjacentHTML('beforeend', htmlCartItem(currency, a.id, a.img, a.title, price, a.quantity, priceAll))
                newImagesArray.push(cartProductsList.querySelector(`[data-id="${a.id}"] img`))
            })
            resolve(newImagesArray)
        })
    }

    //  рендер товаров для чеклиста из localstorage
    const renderCheckListItems = (products) => {
        let currency = DEFAULT_CURRENCY
        checkProductList.innerHTML = ''
        products.forEach(a => {
            let priceAll = productionPrice((a.priceNumber * a.quantity).toFixed(2))
            checkProductList.insertAdjacentHTML('beforeend', htmlCheckListItem(currency, a.title, a.quantity, priceAll))
        })
    }

    //  генерирование продуктов и расчет сумм внутри главной корзины
    const renderCartContent = (products = []) => {
        renderCartItems(products)
            .then((images) => Promise.all(images.map(controlLoadElement)))
            .then((images) => {
                let cartItems = []
                heightCartItem = parseInt(getComputedStyle(cartProductsList.querySelector('.cart-item')).height)
                heightProductsList = parseInt(getComputedStyle(cartProductsList).height)
                cartProductsList.style.height = heightCartItem * localStore.length + 'px'
                images.forEach(el => {
                    const product = el.closest('.cart-item'),
                          stepperInput = product.querySelector('.stepper__input'),
                          stepperMinus = product.querySelector('.stepper__btn--minus'),
                          stepperPlus = product.querySelector('.stepper__btn--plus')
                    stepper(stepperInput, stepperMinus, stepperPlus)
                    stepperPlus.addEventListener('click', stepperEvent)
                    stepperMinus.addEventListener('click', stepperEvent)
                    stepperInput.addEventListener('blur', stepperEvent)
                    product.style.height = heightCartItem + 'px'
                    cartItems.push(product)
                })
                totalQuantityProducts.textContent = getTotalQuantity()
                showElements(cartItems, 100, 'emergence')
            }).catch(err => console.error(err))
        renderCheckListItems(localStore)
        subtotalSum.forEach(el => printTotalPrice(el))
        totalSum.forEach(el => el.textContent = productionPrice((getTotalPrice() + shippingSumValue).toFixed(2)))
    }

    //  сброс(очистка) ошибочного ввода на полях
    const resetErrorFields = (fields) => {
        fields.forEach(el => {
            el.classList.remove('error-field')
            el.classList.remove('success-field')
            el.closest('.form__item').querySelectorAll('img').forEach(el => el.classList.add('is-reduce'))
        })
    }

    //  расчет стоимости доставки (сторонний API)
    let controlValuePostcodeField = ''
    let controlValueProductsWeight = ''
    const calculatePriceDelivery = () => {
        if (checkboxDelivery.checked) {                                                     // если выбрана доставка
            if ((postCodeField.value.length === 6 && isValidate(postCodeField)) &&
                (controlValuePostcodeField !== postCodeField.value || JSON.stringify(getProductsFromLocalStorage()) !== controlValueProductsWeight)) {
                controlValuePostcodeField = postCodeField.value                             // для контроля поторного запроса с тем же значением
                controlValueProductsWeight = JSON.stringify(getProductsFromLocalStorage())  // для контроля поторного запроса с тем же весом
                let formData = new FormData()
                formData.append('postcode', postCodeField.value)
                formData.append('cart_items', JSON.stringify(getProductsFromLocalStorage()))
                fetch("/delivery-price", {
                    method: "POST",
                    body: formData
                })
                    .then((response) => response.json())
                    .then((result) => {
                        if (result['is_ok']) {
                            priceDelivery = result['response']['price']
                            weightDelivery = result['response']['weight']
                            shippingSumValue = priceDelivery
                            totalSum.forEach(el => el.textContent = productionPrice((getTotalPrice() + shippingSumValue).toFixed(2)))
                            lastValueCheckoutSumDelivery = '₽ ' + productionPrice(priceDelivery.toFixed(2))
                            checkoutSumDelivery.textContent = lastValueCheckoutSumDelivery
                            shippingSum.textContent = lastValueCheckoutSumDelivery
                            weightDeliveryDisplay.forEach(el => {
                                el.textContent = '(' + weightDelivery + ' кг.)'
                            })
                        } else {
                            shippingSumValue = 0
                            notesDeliveryForAdmin = result['response']
                            lastValueCheckoutSumDelivery = 'По данному ИНДЕКСУ требуются уточнения. Мы вам перезвоним!'
                            checkoutSumDelivery.textContent = lastValueCheckoutSumDelivery
                        }
                    })
                    .catch(err => console.error(err))
            } else checkoutSumDelivery.textContent = lastValueCheckoutSumDelivery
        }
    }

    // ------- наведение курсора на блок товаров в главной корзине
    cartProductsList.addEventListener('mouseover', e => {
        if (e.target.closest('.cart-item__remove-btn')) {
            let current = e.target.closest('.cart-item')
            current.querySelector('.cart-item__thumbnail').classList.add('cart-item--remove-hover')
            current.querySelector('.cart-item__name').classList.add('cart-item--remove-hover')
            current.querySelector('.cart-item__price').classList.add('cart-item--remove-hover')
            current.querySelector('.cart-item__quantity').classList.add('cart-item--remove-hover')
            current.querySelector('.cart-item__subtotal').classList.add('cart-item--remove-hover')
        }
    })

    // ------- покидание курсора с блока товаров в главной корзине
    cartProductsList.addEventListener('mouseout', e => {
        if (e.target.closest('.cart-item__remove-btn')) {
            let current = e.target.closest('.cart-item')
            current.querySelector('.cart-item__thumbnail').classList.remove('cart-item--remove-hover')
            current.querySelector('.cart-item__name').classList.remove('cart-item--remove-hover')
            current.querySelector('.cart-item__price').classList.remove('cart-item--remove-hover')
            current.querySelector('.cart-item__quantity').classList.remove('cart-item--remove-hover')
            current.querySelector('.cart-item__subtotal').classList.remove('cart-item--remove-hover')
        }
    })

    // ------- клик по кнопке удалить в главной корзине
    cartProductsList.addEventListener('click', e => {
        if (e.target.closest('.cart-item__remove-btn') || e.target.closest('.cart-item__remove--mobile')) {
            deleteCartItem(e.target.closest('.cart-item'))
        }
    })

    // ------- нажатие кнопки вернуть удаленный товар из главной корзины
    messageUndoBtn.addEventListener('click', () => {
        localStore = JSON.parse(localStorage.getItem('cart')) || []
        localStore.push(deletedProduct[0])
        localStorage.setItem('cart', JSON.stringify(localStore))
        hiddenElementGSAP(messageUndo)
        renderCartContent(deletedProduct)
        controlEmptyCart(localStore.length)
        deletedProduct = []
        calculatePriceDelivery()
    })

    // ------- нажатие кнопки продолжить оформление заказа
    vars.$cartContent.querySelector('.check__to-checkout-btn').addEventListener('click', e => {
        e.preventDefault()
        tabNavigationOrder.click()
    })

    // ------- нажатие на вкладку ЗАКАЗ в корзине
    tabNavigationOrder.addEventListener('click', e => calculatePriceDelivery())

    // ------- выбор способа оплаты в блоке transfer
    // vars.$cartContent.querySelectorAll('.transfer__item input').forEach(el => {
    //     el.addEventListener('change', e => {
    //         let method = vars.$cartContent.querySelector('input[name="transfer"]:checked').value
    //         if (method === 'payments') {
    //             placeOrderBtn.textContent = 'Proceed to payment'
    //         } else {
    //             placeOrderBtn.textContent = 'Place order'
    //         }
    //     })
    // })

    // ------- нажатие на сообщение "have a coupon"
    vars.$cartContent.querySelector('.js-messages__coupon').addEventListener('click', () => {
        vars.$cartContent.querySelector('.form-coupon').classList.toggle('form-coupon--visible')
    })

    // ------- нажатие на сообщение "returning customer"
    if (vars.$cartContent.querySelector('.js-messages__customer')) {
      vars.$cartContent.querySelector('.js-messages__customer').addEventListener('click', e => {
        modalWindowOn(vars.$authorizationModal, 'authorization-modal--visible')
        vars.$authorizationModal.querySelector('.authorization-modal__tab--active').focus()
      })
    }

    // ------- submit формы отправки купона во вкладке shopping cart (заглушка)
    vars.$cartContent.querySelector('.shopping__form').addEventListener('submit', e => {
        const couponMessageEl = vars.$cartContent.querySelector('.shopping__form .coupon__message')
        let enteredText = vars.$cartContent.querySelector('.shopping__form .coupon__text').value
        let messageText = enteredText ? `"${enteredText}" такого купона не существует!` : 'Введите номер купона'
        e.preventDefault()
        couponMessageEl.style.height = '0px'
        couponMessageEl.innerHTML = ''
        setTimeout(() => {
            couponMessageEl.style.height = '16px'
            couponMessageEl.innerHTML = `<div class="coupon__message">${messageText}</div>`
            couponMessageEl.classList.remove('is-reduce')
        }, 500)
    })

    // ------- submit формы отправки купона во вкладке checkout "have a coupon" (заглушка)
    vars.$cartContent.querySelector('.checkout__form-coupon').addEventListener('submit', e => {
        let enteredText = vars.$cartContent.querySelector('.checkout__form-coupon .coupon__text').value
        e.preventDefault()
        gsap.to(messageError, {
            duration: 0.5,
            height: 0,
            autoAlpha: 0,
            ease: "sine.out",
            onComplete: () => {
                vars.$messageErrorList.innerHTML = ''
                let messageText = enteredText ? `"${enteredText}" такого купона не существует!` : 'Введите номер купона'
                vars.$messageErrorList.insertAdjacentHTML('beforeend' , htmlMessageErrorItem(messageText))
                showHiddenElementGSAP(messageError)
            }
        })
    })

    // ------- нажатие на опцию САМОВЫВОЗ
    vars.$cartContent.querySelector('.delivery__pickup input').addEventListener('change', e => {
        onChangeDelivery(formCheckoutDelivery, formCheckoutPickup)
    })

    // ------- нажатие на опцию ДОСТАВКА
    let eventInput = new Event('input') // создание события для программного вызова метода input на поле postCode
    vars.$cartContent.querySelector('.delivery__delivery input').addEventListener('change', e => {
        const postCodeDataInfo = document.querySelector('.address-data__info[data-info-set="postcode"]')
        onChangeDelivery(formCheckoutPickup, formCheckoutDelivery)
        if (postCodeDataInfo && postCodeDataInfo.value !== '') postCodeField.dispatchEvent(eventInput)
    })

    // ------- нажатие на опцию (checkbox) изменить данные доставки
    if (checkboxDataEdit) {
        checkboxDataEdit.addEventListener('change', e => {
            if (checkboxDataEdit.checked) {
                currentCheckoutForm.classList.remove('is-reduce-mh')
            } else {
                if (((currentCheckoutForm === formCheckoutPickup) && (vars.$cartContent.querySelector('.account-data')))
                    || ((currentCheckoutForm === formCheckoutDelivery) && (vars.$cartContent.querySelector('.address-data')))) {
                    currentCheckoutForm.classList.add('is-reduce-mh')
                }
            }
        })
    }

    // ------- API обработка стоимости доставки в зависимости от почтового ИНДЕКСА
    postCodeField.addEventListener('input', e => calculatePriceDelivery())

    // ------- submit основная форма(конечная) оформления товаров
    placeOrderBtn.addEventListener('click', e => {
        const
            formFields = [...currentCheckoutForm.querySelectorAll('.js-required')],
            checkPolicy = vars.$cartContent.querySelector('.check__policy input'),
            checkoutMessageError = vars.$cartContent.querySelector('.checkout__messages .messages__error')
        let
            countErrorFields = 0,
            errorFields = []
        e.preventDefault()
        gsap.to(messageError, {
            duration: 0.5,
            height: 0,
            autoAlpha: 0,
            ease: "sine.out",
            onComplete: () => {
                vars.$messageErrorList.innerHTML = ''
                if (checkPolicy.checked) {                   //  если выбрана политика конфиденциальности
                    formFields.forEach(field => {
                        if (!isValidate(field)) {
                            countErrorFields++
                            errorFields.push(field)
                        }
                    })
                    if (countErrorFields > 0) {            //  если некоторые поля заполнены с ошибками (или пустые)
                        formFields.forEach(field => validateShow(field))
                        messageError.classList.remove('is-reduce')
                        errorFields.forEach( (e, i) => {
                            let textField = e.previousElementSibling.textContent
                            e.classList.add('error-field')
                            setTimeout(() => {
                                let messageText = `Заполните поле <b>${textField}</b> правильно`
                                vars.$messageErrorList.insertAdjacentHTML('beforeend' , htmlMessageErrorItem(messageText))
                                showHiddenElementGSAP(messageError)
                            }, i * 100 + 50)
                        })
                        // если ошибка ввода, то экран прокручивается на блок с сообщениями об ошибках
                        window.scrollTo({
                            top: window.pageYOffset - 150 + checkoutMessageError.getBoundingClientRect().top,
                            behavior: "smooth",
                        })
                    } else {                             //  если все заполнено правильно
                        vars.$loader.classList.remove('is-delete')
                        hiddenElementGSAP(messageError)
                        let formData = new FormData(currentCheckoutForm)
                        formData.append('cart_items', JSON.stringify(getProductsFromLocalStorage()))
                        formData.append('method_delivery', currentCheckoutForm.id)
                        formData.append('sum_delivery', priceDelivery)
                        formData.append('notes_admin', notesDeliveryForAdmin)
                        // если все введено правильно, то экран прокручивается на самый верх
                        window.scrollTo({
                            top: vars.$header.getBoundingClientRect().top,
                            behavior: "smooth",
                        })
                        setTimeout(() => {
                            fetch("/new-order", {
                                method: "POST",
                                body: formData
                            })
                                .then((response) => response.json())
                                .then((result) => {
                                    if (result['is_ok']) {
                                        checkoutOrderMessage.insertAdjacentHTML('beforeend', htmlOrderMessage(result['order_info']))
                                    } else {
                                        checkoutOrderMessage.innerHTML = ''
                                        checkoutOrderMessage.insertAdjacentHTML('beforeend', `
                                    <h3>${result['message']} Пожалуйста, сообщите нам об этой проблеме!</h3>`)
                                        console.error(result['message'])
                                    }
                                    // визуализация появления сообщения о создании заказа
                                    hideElementHard(checkoutCheck, DELAY_ORDER_MESSAGE)
                                    hideElementHard(checkoutFormWrapper, DELAY_ORDER_MESSAGE)
                                    hideElementHard(checkoutMessages, DELAY_ORDER_MESSAGE)
                                    setTimeout(() => {
                                        checkoutOrderMessage.classList.remove('is-delete')
                                        setTimeout(() => checkoutOrderMessage.style.opacity = '1', 50)
                                        vars.$loader.classList.add('is-delete')
                                    }, DELAY_ORDER_MESSAGE)
                                    // полная очистка корзины и всего localStorage
                                    localStorage.clear()
                                    localStore = JSON.parse(localStorage.getItem('cart')) || []
                                    hiddenElementGSAP(messageUndo)
                                    renderCartContent()
                                    controlEmptyCart(localStore.length)
                                })
                                .catch(err => console.error(err))
                        }, 1000)
                    }
                } else {            //  если не выбрана политика конфиденциальности
                    checkPolicy.closest('.check__policy').classList.add('check__policy--error')
                    vars.$messageErrorList.insertAdjacentHTML('beforeend' , htmlMessageErrorItem(ACCEPT_TERMS))
                    showHiddenElementGSAP(messageError)
                    // экран прокручивается на блок с сообщениями об ошибках
                    window.scrollTo({
                        top: window.pageYOffset - 150 + checkoutMessageError.getBoundingClientRect().top,
                        behavior: "smooth",
                    })
                }
            },
        })
    })
    vars.$cart.style.display = 'none'
    controlEmptyCart(localStore.length)
    if (localStore.length > 0) renderCartContent(localStore)
}