import vars from "../_vars";

// --- прилипание к верху header по скроллу
window.addEventListener('scroll', () => {
    if (pageYOffset > 50) {
        vars.$header.classList.add('header--sticky')
    } else {
        vars.$header.classList.remove('header--sticky')
    }
})

if (vars.$shopNavUser.querySelector('.js-shop-nav__user--dropdown')) {
    // --- при клике на иконку пользователя (для мобильных)
    vars.$shopNavUser.addEventListener('click', e => {
        document.querySelector('.js-shop-nav__user--dropdown').classList.toggle('custom-select--open')
    })
    // --- наведение на иконку авторизации при авторизованном пользователе
    vars.$shopNavUser.addEventListener('mouseenter', e => {
        document.querySelector('.js-shop-nav__user--dropdown').classList.add('custom-select--open')
    })
    // --- обработчик при покидании курсора кнопки полной очистки выбранных фильтров
    vars.$shopNavUser.addEventListener('mouseleave', e => {
        document.querySelector('.js-shop-nav__user--dropdown').classList.remove('custom-select--open')
    })
}