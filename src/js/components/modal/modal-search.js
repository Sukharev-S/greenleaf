import vars from '../../_vars';
import {modalWindowOn, closeModalWindows} from '../../functions/modalWindows'
import {controlLoadElement} from '../../functions/controlLoadElement'
import {renderProducts} from '../../functions/productActions'
import {removeElements, showElements} from '../../functions/animate'

if (vars.$searchModal) {

    const searchForm = vars.$searchModal.querySelector('.form-search')
    const searchField = vars.$searchModal.querySelector('.form-search__field')
    const resultsContainer = vars.$searchModal.querySelector('.form-search__results-container')
    const resultsBlock = vars.$searchModal.querySelector('.form-search__results-inner')
    const searchEmpty = vars.$searchModal.querySelector('.form-search__empty')
    const modalSearchLoader = vars.$searchModal.querySelector('.js-search-loader')
    const DELAY_INPUT = 2000             // задержка отрабатывания ввода в поле поиска
    const DELAY_REMOVE_PRODUCTS = 30      // задержка очищения блока поиска от старых продуктов
    const DELAY_SHOW_PRODUCTS = 70        // задержка появления новых продуктов в блоке поиска
    const RESULT_BLOCK_HEIGHT = '60px'       // высота блока результатов поиска
    let inputFlag   //  технический флаг для троттлинга ввода

    // обработчик открытия модального окна поиска
    vars.$search.addEventListener('click', e => {
        modalWindowOn(vars.$searchModal, 'search-modal--visible')
        searchField.focus()
    })

    // обработчик закрытия модального окона поиска по клику
    vars.$searchModal.addEventListener('click', e => {
        if (e.target.closest('.search-modal__close')) {
            closeModalWindows()
            resultsContainer.classList.remove('form-search__results-container--visible')
        }
    })

    //  событие ввода в поисковое поле
    searchField.addEventListener('input', e => {
        if (!inputFlag) {
            if (searchField.value.length > 1) modalSearchLoader.classList.remove('is-delete')
            inputFlag = setTimeout(function () {
                inputFlag = null
                searchProducts()
            }, DELAY_INPUT)
        }
    })

    const searchProducts = function () {
        if (searchField.value.length >= 2) {
            let formData = new FormData(searchForm)
            resultsContainer.classList.add('form-search__results-container--visible')
            removeElements(resultsBlock.querySelectorAll('.form-search__product'), DELAY_REMOVE_PRODUCTS , 'emergence')
                .then(() => fetch('/search/product', {
                    method: "POST",
                    body: formData
                })
                    .then((response) => response.json())
                    .then((data) => {
                        let dataStore = [...data]
                        let out = []
                        dataStore.forEach((el, i) =>
                            out[i] = {
                                'id'            : el['id_product'],
                                'title'         : el['name'],
                                'price'         : el['price'],
                                'old_price'     : el['old_price'],
                                'main_image'    : el['main_image'],
                                'article'       : el['article'],        // пока не используется
                                'short'         : el['short'],          // пока не используется
                                'is_new'        : el['is_new'],         // пока не используется
                                'is_popular'    : el['is_popular'],     // пока не используется
                                'is_sale'       : el['is_sale']         // пока не используется
                        })
                        searchEmpty.classList.add('form-search__empty--hidden')
                        return renderProducts(out, resultsBlock, 'form-search__product', 0, out.length)
                    })
                    .then((goods) => Promise.all(goods.map(controlLoadElement)))
                    .then((images) => {
                        modalSearchLoader.classList.add('is-delete')
                        if (images.length > 0) {
                            let foundProducts = images.map(el => el.closest('.form-search__product')),
                                productItem = foundProducts[0].closest('.form-search__product'),
                                heightProductItem = parseInt(getComputedStyle(productItem).height) +
                                    parseInt(getComputedStyle(productItem).marginBottom) + 16, // если в названии продукта две строчки
                                widthProductItem = parseInt(getComputedStyle(productItem).width) +
                                    parseInt(getComputedStyle(productItem).marginLeft) +
                                    parseInt(getComputedStyle(productItem).marginRight),
                                widthBlockResult = parseInt(getComputedStyle(resultsBlock).width),
                                quantityPerLine = Math.floor(widthBlockResult / widthProductItem),
                                quantityPerColumn = Math.ceil(foundProducts.length / quantityPerLine),
                                maxHeightBlockResult = parseInt(getComputedStyle(resultsBlock).maxHeight),
                                calcHeightBlockResult = quantityPerColumn * heightProductItem +
                                    parseInt(getComputedStyle(resultsBlock).marginTop) +
                                    parseInt(getComputedStyle(resultsBlock).marginBottom) +
                                    parseInt(getComputedStyle(resultsBlock).paddingTop) +
                                    parseInt(getComputedStyle(resultsBlock).paddingBottom)
                            resultsBlock.style.height = calcHeightBlockResult > maxHeightBlockResult ? maxHeightBlockResult + 'px'
                                : calcHeightBlockResult + 'px'
                            return showElements(foundProducts, DELAY_SHOW_PRODUCTS, 'emergence')
                        } else {
                            searchEmpty.classList.remove('form-search__empty--hidden')
                            resultsBlock.style.height = RESULT_BLOCK_HEIGHT
                        }
                    })
                    .catch(err => console.error(err)))
        } else {
            resultsContainer.classList.remove('form-search__results-container--visible')
        }
    }
}