import vars from "../../_vars";
import {closeModalWindows, modalWindowOn} from "../../functions/modalWindows";

if (vars.$imageModal) {

    let imagesTarget = document.querySelectorAll('.js-image-zoom'),
        imageZoom = vars.$imageModal.querySelector('.image-modal__img'),
        refDistinctWindow = vars.$imageModal.querySelector('.image-modal__distinct-window'),
        certificates = document.querySelectorAll('.company-cert__item')


    // открытие модального окна с увеличенным изображением
    imagesTarget.forEach(el => {
        el.addEventListener('click', e => {
            let srcImageAttr = e.target.getAttribute("src"),
                altImageAttr = e.target.getAttribute("alt")
            srcImageAttr = srcImageAttr.replace('_min', '')
            imageZoom.setAttribute("src", srcImageAttr)
            imageZoom.setAttribute("alt", altImageAttr)
            refDistinctWindow.setAttribute("href", srcImageAttr)
            if (vars.$freeDeliveryBtn) vars.$freeDeliveryBtn.click()
            // блокировка сертификатов, чтобы успел очиститься путь
            if (certificates) certificates.forEach(el => el.style.pointerEvents = "none")
            modalWindowOn(vars.$imageModal, 'image-modal--visible')
        })
    })


    // закрытие модального окна с увеличенным изображением
    vars.$imageModal.addEventListener('click', e => {
        closeModalWindows()
        setTimeout(() => {
            imageZoom.setAttribute("src", '')
            if (certificates) certificates.forEach(el => el.style.pointerEvents = "auto")
        }, 750)
    })
}