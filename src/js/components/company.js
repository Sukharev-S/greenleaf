
// если находимся на странице КОМПАНИЯ

if (document.querySelector('#company')) {

    // let certificateItems = document.querySelectorAll('.company-cert__item')
    // certificateItems.forEach(el => {
    //     el.addEventListener('click' , e => {
    //         let currentItem = document.querySelectorAll('.company-cert__item--active')
    //         if (e.currentTarget.classList.contains('company-cert__item--active')) {
    //             e.currentTarget.classList.remove('company-cert__item--active')
    //         } else {
    //             certificateItems.forEach(el => el.classList.remove('company-cert__item--active'))
    //             e.currentTarget.classList.add('company-cert__item--active')
    //         }
    //     })
    // })

    // --- scrollTrigger
    const scrollImages = function () {
        let windowWidth = window.innerWidth
        if (windowWidth > 1024) {
            // ----- на изображения страницы КОМПАНИЯ
            gsap.to('.company-about .company__image', {
                scrollTrigger: {
                    scrub: 2
                },
                yPercent: 20,
            })
        }
        if (windowWidth > 768 && windowWidth < 1024) {
            // ----- на изображения страницы КОМПАНИЯ
            gsap.to('.company-about .company__image', {
                scrollTrigger: {
                    scrub: 2
                },
                yPercent: 100,
            })
        }
    }

    // --- Инициация движение изображений по скроллу
    scrollImages()

    // ------- при изменении размера экрана изменять и скорость движения изображений
    window.addEventListener("resize", () => scrollImages())
}
