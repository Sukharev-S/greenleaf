import {clearingFormFields, isValidate, validateShow} from "../functions/validation";
import {showCallbackMessage} from "./callback-message";

if (document.querySelector('.footer-form')) {
    const
        loader = document.querySelector('.js-global-loader'),
        questionRef = document.querySelector('.js-link-question'),
        callbackRef = document.querySelector('.js-link-callback'),
        questionForm = document.querySelector('[name="question"]'),
        callbackForm = document.querySelector('[name="callback"]'),
        formBlock = document.querySelector('.js-forms-callback'),
        callBackMessageFooter = document.querySelector('.footer__callback .callback-message'),
        callbackMessageFooterTextBlock = document.querySelector('.footer .js-callback-message__text'),
        ANSWER_CALLBACK_FORM = 'Заявка принята успешно!<br>Ожидайте звонка!',
        ANSWER_QUESTION_FORM = 'Ваш вопрос принят успешно!<br>Мы уже работаем над ответом!',
        ANSWER_ERROR = 'Произошла непредвиденная ошибка.<br>Пожалуйста, попробуйте позже!'

    // ------- анимация переключения форм обратной связи
    const animateForm = (currentForm, activeForm, formBlock) => {
        currentForm.classList.remove('footer-form--active')
        formBlock.style.height = getComputedStyle(formBlock).height
        setTimeout(() => {
            currentForm.classList.add('is-delete')
            activeForm.classList.remove('is-delete')
            let heightQuestionForm = getComputedStyle(activeForm).height
            formBlock.style.height = parseInt(heightQuestionForm) + 73 + 'px' // 73 это падинг, маржин и заголовок
            setTimeout(() => {
                activeForm.classList.add('footer-form--active')
            }, 100)
        }, 500)
        if (window.innerWidth < 1024) {
            currentForm.scrollIntoView({
                behavior: "smooth",
                block: "start"
            })
        }
    }

    // ------- для чистоты кода, валидация полей и submit формы
    const submitCallbackForm = (form, url, answer) => {
        let formFields = form.querySelectorAll('.form-field'),
            countEmptyFields = 0,
            countErrorFields = 0
        loader.classList.remove('is-delete')
        formFields.forEach(field => {
            if (field.value === '') countEmptyFields++
            if (!isValidate(field)) countErrorFields++
        })
        if (countEmptyFields !== 0) {       // -- проверка на пустоту полей
            formFields.forEach(field => validateShow(field))
            loader.classList.add('is-delete')
        } else {
            if (countErrorFields === 0) {       // -- проверка на валидность полей
                let formData = new FormData(form)
                grecaptcha.ready(() => {
                    grecaptcha.execute('6LfmHZcdAAAAAP8iwCzedccy2M6LzpRDD1PdaCcs', {action: 'homepage'})
                        .then(function(token) {
                            formData.append('token', token.toString());
                            fetch(url, {
                                method: "POST",
                                body: formData
                            })
                            .then((response) => response.json())
                            .then((result) => {
                                let messageText
                                if (result['is_ok']) {
                                    messageText = answer
                                    callbackMessageFooterTextBlock.classList.remove('callback-message--error')
                                } else {
                                    messageText = ANSWER_ERROR
                                    callbackMessageFooterTextBlock.classList.add('callback-message--error')
                                }
                                setTimeout(() => {
                                    loader.classList.add('is-delete')
                                    showCallbackMessage(form, 'footer__form--hidden', callBackMessageFooter, messageText)
                                        .then(() => {
                                            questionRef.classList.remove('is-disabled')
                                            callbackRef.classList.remove('is-disabled')
                                            clearingFormFields(form)
                                        })
                                        .catch(err => console.error(err))
                                }, 500)
                            }).catch(err => console.error(err))
                    })
                })
            } else {
                formFields.forEach(field => validateShow(field))
                loader.classList.add('is-delete')
            }
        }
    }

    // ------- клик по ссылке "Задать вопрос"
    questionRef.addEventListener('click', e => {
        e.preventDefault()
        animateForm(callbackForm, questionForm, formBlock)
    })

    // ------- клик по ссылке "Заказать звонок"
    callbackRef.addEventListener('click', e => {
        e.preventDefault()
        animateForm(questionForm, callbackForm, formBlock)
    })

    // ------- submit формы "обратный звонок"
    callbackForm.addEventListener('submit', e => {
        e.preventDefault()
        submitCallbackForm(callbackForm, '/captcha/phone', ANSWER_CALLBACK_FORM)
    })

    // ------- submit формы "задать вопрос"
    questionForm.addEventListener('submit', e => {
        e.preventDefault()
        submitCallbackForm(questionForm, '/captcha/question', ANSWER_QUESTION_FORM)
    })
}