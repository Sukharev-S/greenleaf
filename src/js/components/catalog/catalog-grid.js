import vars from '../../_vars'
import {fetchProducts, renderProducts, disableProducts} from "../../functions/productActions"
import {controlLoadElement} from '../../functions/controlLoadElement'
import {pagination, listenerPagination} from "../../functions/pagination"
import {removeElement, removeElements, showElements} from '../../functions/animate'
import {throttler as resizeThrottler} from "../../functions/trottler";

if (document.querySelector('.catalog')) {

    const categorySwiper = document.querySelector('.hero-catalog__slider').swiper
    const gridEmptyBlock = document.querySelector('.js-catalog-grid-empty')

    const selectQuantity = document.querySelector('.js-catalog-props__quantity')
    const selectQuantityCurrentItem = selectQuantity.querySelector('.custom-select__top')
    const selectQuantityItems = selectQuantity.querySelectorAll('.custom-select__item')

    let totalProducts = 0                           // сколько продуктов по AJAX запросу (общая переменная для функций)

    const animateClass = 'emergence'                //  класс для анимации повяления, исчезновения продуктов и подкатегорий
    const delayShowProducts = 75                    //  задержки анимации
    const delayRemoveProducts = 75                  //  появления товаров
    const delayShowSubCategories = 75               //  и
    const delayRemoveSubCategories = 75             //  подкаталогов
    const catalogGridListHeightDefault = 200        //  минимальная высота блока catalogGridList

    // ------- Функции рассчета (Геттеры) -----------------------------
    // ------- функция получения количества товара на странице
    const getProductsPerPage = () => {
        return parseInt(selectQuantityCurrentItem.textContent)
    }
    // ------- функция получения количества столбцов товара на странице
    const getColumnsCount = () => {
        return window.innerWidth <= 1200    // для мобил всего 2 столбца всегда
            ? 2
            : vars.$catalogColumns.querySelector('.catalog-columns__btn--current').dataset.columns
    }
    // ------- функция получения количества страниц запрошенного товара
    const getQuantityPage = () => {
        let productsPerPage = getProductsPerPage()
        return Math.ceil(totalProducts / productsPerPage)
    }
    // ------- функция получения количества ожидаемых к показу товаров(готовых к показу)
    const getParamsDisplayedGoods = () => {
        let activeBtn = vars.$customPagination.querySelector('.custom-pagination__link--current'),
            productsPerPage = getProductsPerPage(),
            displayedGoods = parseInt(activeBtn.textContent) * productsPerPage - productsPerPage,
            expectedGoods = displayedGoods + getProductsPerPage()
        return [displayedGoods, expectedGoods]
    }


    // ------- функция обнуления счетчика показанных товаров
    const resetPagination = function () {
        vars.$customPagination.querySelector('.custom-pagination__link--current').classList
            .remove('custom-pagination__link--current')
        vars.$customPagination.querySelector('.js-custom-pagination__link--begin').classList
            .add('custom-pagination__link--current')
        vars.$customPagination.querySelector('.js-custom-pagination__link--one').textContent = '2'
        vars.$customPagination.querySelector('.js-custom-pagination__link--two').textContent = '3'
        vars.$customPagination.querySelector('.js-custom-pagination__link--tree').textContent = '4'
    }

    // ------- функция загрузки товаров на странице каталога
    const loadProductsCatalog = function(delayShowProducts) {
        return fetchProducts(getUrlSubCatSortProducts())
            .then(data => {
                let responseProducts = [...data['products']]
                totalProducts = responseProducts.length
                vars.$catalogGridList.style.height = '1950px'
                let [displayedGoods, expectedGoods] = getParamsDisplayedGoods()
                return renderProducts(responseProducts, vars.$catalogGridList, 'catalog-grid__item', displayedGoods, expectedGoods)
            })
            .then(goods => Promise.all(goods.map(controlLoadElement)))
            .then((images) => {
                let newProducts = images.map(el => el.closest('.catalog-grid__item')),
                    heightProduct = 0,
                    marginBottomProduct = 0,
                    catalogGridListHeight = 0
                if (newProducts[0]) {       // если продукты найдены по данному запросу
                    gridEmptyBlock.classList.remove('catalog-grid__empty--visible')
                    heightProduct = parseInt(getComputedStyle(newProducts[0]).height)
                    marginBottomProduct = parseInt(getComputedStyle(newProducts[0]).marginBottom)
                    catalogGridListHeight = (heightProduct + marginBottomProduct) * Math.ceil(newProducts.length / getColumnsCount()) - marginBottomProduct
                } else {                    // если продукты не найдены по данному запросу
                    gridEmptyBlock.classList.add('catalog-grid__empty--visible')
                }
                if (catalogGridListHeight < catalogGridListHeightDefault) catalogGridListHeight = catalogGridListHeightDefault
                vars.$catalogGridList.style.height = catalogGridListHeight + 'px'
                disableProducts()
                showElements(newProducts, delayShowProducts, animateClass)
            })
    }

    // ------- функция(промис) удаления товаров на странице каталога
    const prepareProductsCatalog = function() {
        return removeElements(vars.$catalogGridList.querySelectorAll('.catalog-grid__item'), delayRemoveProducts , animateClass)
            .then(() => {
                vars.$catalogGridList.style.height = catalogGridListHeightDefault + 'px'
                vars.$catalogGridList.dataset.gridColumns = getColumnsCount()
            })
    }

    //------------------- Работа с фильтрами (подкатегории, choice, загрузка товаров по фильтрам ------------------

    // ------- функция рендеринга фильтра подкатегорий на странице каталога
    const renderSubCategories = function (elementList, subCategories) {
        elementList.insertAdjacentHTML('beforeend', `
            <li class="catalog-filter__item catalog-filter__all custom-checkbox--active">
                <label class="custom-checkbox" data-text="Все подкатегории">
                    <input type="checkbox" 
                            class="custom-checkbox__input visually-hidden" 
                            data-name="all"
                            checked 
                            disabled>
                    <span class="custom-checkbox__text">Все подкатегории</span>
                </label>
            </li>                      
        `)
        for (let i = 0; i < subCategories.length; i++) {
            elementList.insertAdjacentHTML('beforeend', `
               <li class="catalog-filter__item">
                    <label class="custom-checkbox" data-text="${subCategories[i]['name']}">
                        <input type="checkbox" 
                               class="custom-checkbox__input visually-hidden"
                               data-name="${subCategories[i]['name_translit']}">
                        <span class="custom-checkbox__text">${subCategories[i]['name']}</span>
                    </label>
                </li>                        
            `)
        }
    }

    // ------- функция создания элемента с названием выбранного фильтра в строку над товарами
    const renderChoiceItem = (text) => {
        vars.$catalogChoice.insertAdjacentHTML('afterbegin', `
              <button class="btn-reset catalog-choice__item" data-choice-text="${text}">
                ${text}
                <svg aria-hidden="true">
                  <use xlink:href="/img/sprite.svg#close"></use>
                </svg>
              </button>
            `)
        // return vars.$catalogChoice.querySelector(`[data-choice-text="${text}"]`)
    }

    // ------- функция генерации url адреса запроса по выбранной подкатегории
    const getUrlSubCatSortProducts = () => {
        let checkboxesOn = vars.$catalogFilterList.querySelectorAll('input:checked')
        let listName = []
        let selectedSort = document.querySelector('.catalog-props__sort .custom-select__top')
        checkboxesOn.forEach(el => listName.push(el.dataset.name))
        let subCategories = listName[0] !== undefined ? '/' + listName.join('-') : '/all'
        // window.history.pushState("object or string", "Title", "/catalog" + url)
        return '/category/' + vars.$catalogGridList.dataset.categoryId + subCategories + '/sort/' + selectedSort.dataset.sort
    }

    // ------- функция очистки выбранных фильтров
    const choiceCleaning = (choiceItems, filterItems) => {
        choiceItems.forEach(el => {
            if (!el.classList.contains('catalog-choice__clear')) {
                removeElement(el, 'catalog-choice__item--is-visible')
                    .catch(err => console.error(err))
            }
            document.querySelectorAll('.catalog-filter__quantity').forEach(el => el.textContent = '0')
            filterItems.forEach(elem => {
                elem.querySelector('input').checked = false
                elem.querySelector('input').disabled = false
                elem.classList.remove('custom-checkbox--active')
            })
        })
        vars.$catalogChoice.classList.remove('catalog-choice--is-visible')
    }

    // ------- функция подсчета и вывода кол-ва выбранных фильтров в подразделе и показ всего блока choice
    const countingSelectedFilters = (checkBoxWrap) => {
        let filterQuantity =  checkBoxWrap.closest('.catalog-filter').querySelector('.catalog-filter__quantity')
        let activeFiltersAfter =
            vars.$catalogFilters.querySelectorAll('.custom-checkbox--active:not(.catalog-filter__all)')
        filterQuantity.textContent = String(activeFiltersAfter.length)
        activeFiltersAfter.length > 0 ? vars.$catalogChoice.classList.add('catalog-choice--is-visible')
            : vars.$catalogChoice.classList.remove('catalog-choice--is-visible')
    }

    // ------- функции отключения и включения интеративных элементов выбора загрузки товара
    let showCategorySliderBtns = document.querySelectorAll('.catalog-slide__btn-show')
    let categoryItems = document.querySelectorAll('.catalog-categories__item')
    let catalogFiltersBlock = document.querySelector('.catalog-filters')
    let catalogGridPropsBlock = document.querySelector('.catalog-grid__props')
    let allInteractiveElements = [...showCategorySliderBtns, ...categoryItems, catalogFiltersBlock, catalogGridPropsBlock]
    const disableLoadProducts = () => {
        allInteractiveElements.forEach(el => el.classList.add('is-disabled'))
    }
    const enableLoadProducts = () => {
        allInteractiveElements.forEach(el => el.classList.remove('is-disabled'))
    }

    // ------- функция перезагрузки списка товара
    const reloadListProducts = () => {
        disableLoadProducts()
        resetPagination()
        prepareProductsCatalog()
            .then(() => loadProductsCatalog(delayShowProducts))
            .then(() => {
                pagination.renderPagination(getQuantityPage())
                enableLoadProducts()
            })
            .catch(err => console.error(err))
    }

    // ------- функция обработчика фильтров (подкаталогов) на изменение чекбокса
    const filtersHandler = (filterItems) => {
        filterItems.forEach(checkBoxWrap => {
            checkBoxWrap.querySelector('input').addEventListener('change', e => {
                let isChecked = checkBoxWrap.querySelector('input').checked
                isChecked ? boxChecked(checkBoxWrap) : boxUnChecked(checkBoxWrap)
                countingSelectedFilters(checkBoxWrap)
                reloadListProducts()
            })
        })
    }

    // ------- функция если выбор снят
    const boxUnChecked = (checkBoxWrap) => {
        let mainCheckBoxWrap = checkBoxWrap.closest('.catalog-filter__items').firstElementChild
        checkBoxWrap.classList.remove('custom-checkbox--active')
        checkBoxWrap.querySelector('input').checked = false
        if (!checkBoxWrap.closest('.catalog-filter__all')) {
            let dataText = checkBoxWrap.querySelector('.custom-checkbox').dataset.text,
                activeFilters =
                vars.$catalogFilters.querySelectorAll('.custom-checkbox--active:not(.catalog-filter__all)')
            vars.$catalogChoice.querySelector(`[data-choice-text="${dataText}"]`)
                .classList.remove('catalog-choice__item--is-visible')
            if (activeFilters.length === 0) {
                boxChecked(mainCheckBoxWrap)
            }
        }
    }

    // ------- функция если выбор поставлен
    const boxChecked = (checkBoxWrap) => {
        let mainCheckBoxWrap = checkBoxWrap.closest('.catalog-filter__items').firstElementChild
        checkBoxWrap.classList.add('custom-checkbox--active')
        checkBoxWrap.querySelector('input').checked = true
        if (!checkBoxWrap.closest('.catalog-filter__all')) {
            let dataText = checkBoxWrap.querySelector('.custom-checkbox').dataset.text
            renderChoiceItem(dataText)
            let newElement = vars.$catalogChoice.querySelector(`[data-choice-text="${dataText}"]`)
            mainCheckBoxWrap.querySelector('input').disabled = false
            boxUnChecked(mainCheckBoxWrap)
            setTimeout(() => newElement.classList.add('catalog-choice__item--is-visible'), 100)
        } else {
            mainCheckBoxWrap.querySelector('input').disabled = true
            //  Если главный фильтр checked, то убрать checked с остальных
            checkBoxWrap.closest('.catalog-filter__items').querySelectorAll('.custom-checkbox--active:not(.catalog-filter__all)')
                .forEach(checkBoxWrap => boxUnChecked(checkBoxWrap))
        }
    }
    //------------------- Конец работы с фильтрами (подкатегории, choice, загрузка товаров по фильтрам ----------------

    // ------- функция сборка, загрузка и вывод подкатегорий (для чистоты кода)
    const loadSubCategories = (catalogProductUrl) => {
        return fetch(catalogProductUrl)
                .then((response) => response.json())
                .then(data => [...data['subCategories']])
                .then((responseSubCategories) => {
                    let catalogFilterList = document.querySelector('.catalog-filter__items'),
                        paddingTopList = parseInt(getComputedStyle(catalogFilterList).paddingTop),
                        paddingBottomList = parseInt(getComputedStyle(catalogFilterList).paddingBottom),
                        headerListHeight = 44,   // высота абзаца ВСЕ ПОДКАТЕГОРИИ
                        sumHeight = paddingTopList + paddingBottomList + headerListHeight
                    const heightItem = 18 + 15
                    vars.$catalogFilterList.style.height = (heightItem * responseSubCategories.length + sumHeight) + 'px'
                    renderSubCategories(vars.$catalogFilterList, responseSubCategories)
                })
                .then(() => {
                    let filterItems = document.querySelectorAll('.catalog-filter__item')
                    showElements(filterItems, delayShowSubCategories, animateClass)
                    return filterItems
                })
                .then((filterItems) => filtersHandler(filterItems))
    }

    // ------- функция сборка, обновления всех данных на основе выбранной категории
    const reLoadCategory = (currentCategory) => {
        let catalogProductUrl = '/category/' + currentCategory
        window.history.pushState("object or string", "Title", "/catalog" + catalogProductUrl)
        vars.$catalogGridList.dataset.categoryId = currentCategory
        // метка активности на банерах категорий
        document.querySelectorAll('.catalog-categories__item').forEach(el => {
            el.classList.remove('catalog-categories__item--active')
        })
        document.querySelector(`.catalog-categories__item[data-category="${currentCategory}"]`).classList.add('catalog-categories__item--active')
        // очистка блока(полосы) выбранных фильтров
        choiceCleaning(document.querySelectorAll('.catalog-choice__item'), document.querySelectorAll('.catalog-filter__item'))
        // запрос на товары категории и их анимированный вывод
        reloadListProducts()
        // запрос на подкатегории и их анимированный вывод
        removeElements(document.querySelectorAll('.catalog-filter__item'), delayRemoveSubCategories, animateClass)
            .then(() => loadSubCategories(catalogProductUrl))
            .catch(err => console.error(err))
    }

    // --- обработчик изменения размера экрана, с использованием троттлинга
    window.addEventListener("resize", () => resizeThrottler(reloadListProducts, 3000), false)

    // --- обработчик банеров выбора категории
    document.querySelectorAll('.catalog-categories__item').forEach(el => {
        el.addEventListener('click', e => {
            let currentCategory = e.currentTarget.dataset.category
            categorySwiper.slideTo(currentCategory - 1)
            reLoadCategory(currentCategory)
        })
    })

    // --- обработчик кнопоки ПОКАЗАТЬ КАТЕГОРИЮ в слайдерах
    if (vars.$catalogSlider) {
        const allBtnShowCategory = vars.$catalogSlider.querySelectorAll('.catalog-slide__btn-show')
        allBtnShowCategory.forEach(btn => {
            btn.addEventListener('click', e => {
                let currentCategory = document.querySelector('.catalog-categories__item--active').dataset.category
                let newCategory = e.target.closest('.catalog-slide').dataset.category
                if (currentCategory !== newCategory) reLoadCategory(newCategory)
                let headerHeight = parseInt(getComputedStyle(document.querySelector('.header')).height)
                let elementCoordinates = document.querySelector('.catalog-categories__list').getBoundingClientRect().top
                window.scrollTo(0 , elementCoordinates - headerHeight)
            })
        })
    }

    // --- обработчик кнопок выбора кол-ва столбцов товара
    vars.$catalogColumns.addEventListener('click', e => {
        if (e.target.closest('.catalog-columns__item') && !e.target.closest('.catalog-columns__btn--current')) {
            let $columnsBtn = document.querySelectorAll('.catalog-columns__btn')
            let productsPerPage = parseInt(e.target.dataset.columns) * 3
            $columnsBtn.forEach(el => {el.classList.remove('catalog-columns__btn--current')})
            e.target.classList.add('catalog-columns__btn--current')
            selectQuantityCurrentItem.textContent = productsPerPage.toString()    //-- расчет опции селекта кол-во товаров на страницу
            selectQuantityItems.forEach((el, i) => {
                el.textContent = (productsPerPage + (i * e.target.dataset.columns)).toString()
            })
            reloadListProducts()
        }
    })

    // --- обработчик селекта сортировки по ...
    let selectSort = document.querySelector('.catalog-props__sort')
    let sortItems = selectSort.querySelectorAll('.custom-select__item')
    sortItems.forEach(select => {
        select.addEventListener('click', e => {
            let selectedSort = document.querySelector('.catalog-props__sort .custom-select__top')
            if (e.target.dataset.sort !== selectedSort.dataset.sort) {   // если выбрано что и было
                selectedSort.dataset.sort = e.target.dataset.sort
                reloadListProducts()
            }
        })
    })

    // --- обработчик селекта количества товаров на странице
    selectQuantityItems.forEach(select => {
        select.addEventListener('click', e => {
            if (selectQuantityCurrentItem.textContent !== e.target.textContent) {   // если выбрано что и было
                selectQuantityCurrentItem.textContent = e.target.textContent
                reloadListProducts()
            }
        })
    })

    // --- обработчик при наведении курсора на кнопку полной очистки выбранных фильтров
    document.querySelector('.catalog-choice__clear').addEventListener('mouseenter', e => {
        document.querySelectorAll('.catalog-choice__item').forEach(el => {
            el.classList.add('catalog-choice__item--hover')
        })
    })
    // --- обработчик при покидании курсора кнопки полной очистки выбранных фильтров
    document.querySelector('.catalog-choice__clear').addEventListener('mouseleave', e => {
        document.querySelectorAll('.catalog-choice__item').forEach(el => {
            el.classList.remove('catalog-choice__item--hover')
        })
    })

    listenerPagination(pagination)  // выполниться первым, чтобы назначился класс current на кнопки

    // --- обработчик клика на кнопоки пагинации (срабатывает вторым после listenerPagination)
    vars.$customPagination.addEventListener('click', e => {
        if ((e.target.classList.contains('custom-pagination__link'))
            && (!e.target.closest('.js-custom-pagination__item--more-prev'))
            && (!e.target.closest('.js-custom-pagination__item--more-next'))) {  // если рабочая кнопка
            prepareProductsCatalog()
                .then(() => loadProductsCatalog(delayShowProducts))
                .catch(err => console.error(err))
        }
    })

// --------------------  Стартовая загрузка данных
    loadProductsCatalog(delayShowProducts)
        .then(() => {
            pagination.renderPagination(getQuantityPage())
            return '/' + getUrlSubCatSortProducts().split('/').slice(1, 3).join('/') // подготавливаем url для запроса подкатегорий
        })
        .then((url) => loadSubCategories(url))
        .then(() => {
            let subCategory = window.location.href.split('/').reverse()[0]  // если адрес содержал поддиректорию
            let targetEl = document.querySelector(`[data-name="${subCategory}"]`)
            if (targetEl) {
                let targetElWrap = targetEl.closest('.catalog-filter__item')
                boxChecked(targetElWrap)
                countingSelectedFilters(targetElWrap)
            }
            // --- обработчик выбранных фильтров (строка отображения, какие фильтра выбраны (над списком товаров))
            vars.$catalogChoice.addEventListener('click', (e) => {
                if (e.target.classList.contains('catalog-choice__clear')) {
                    let mainFilterInputs = vars.$catalogFilterList.querySelectorAll('.catalog-filter__all input')
                    let eventChange = new Event("change")
                    choiceCleaning(Array.from(e.currentTarget.children), document.querySelectorAll('.catalog-filter__item'))
                    mainFilterInputs.forEach(input => input.checked = true)
                    mainFilterInputs.forEach(input => input.dispatchEvent(eventChange))
                }
                if (e.target.classList.contains('catalog-choice__item')) {
                    let text = e.target.textContent.trimLeft().trimRight()
                    let currentCheckBox = document.querySelector(`[data-text="${text}"]`)
                    if (e.target.classList.contains('catalog-choice__item')) {
                        removeElement(e.target, 'catalog-choice__item--is-visible')
                            .then(() => {
                                if (vars.$catalogChoice.children.length === 1) {
                                    vars.$catalogChoice.classList.remove('catalog-choice--is-visible')
                                }
                            })
                        currentCheckBox.querySelector('input').checked = false
                        let eventChange = new Event("change")
                        currentCheckBox.querySelector('input').dispatchEvent(eventChange)
                        currentCheckBox.closest('.catalog-filter__item').classList.remove('custom-checkbox--active')
                    }
                }
            })
        })
        .catch(err => console.error(err))
    // метка активности на банерах категорий
    document.querySelector(`.catalog-categories__item[data-category="${vars.$catalogGridList.dataset.categoryId}"]`)
        .classList.add('catalog-categories__item--active')
}