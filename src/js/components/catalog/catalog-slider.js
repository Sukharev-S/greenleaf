import Swiper from '../../vendor/swiper.min.js';
import vars from '../../_vars';

if (vars.$catalogSlider) {
    const catalogSlider = new Swiper(vars.$catalogSlider, {
        initialSlide: vars.$catalogSlider.dataset.startSlide - 1,
        loop: false,
        slidesPerView: 1,
        allowTouchMove: true,
        speed: 1500,
        freeMode: true,
        freeModeSticky: true,
        freeModeMomentum: true,
        freeModeMomentumRatio: 2,
        freeModeMomentumVelocityRatio: 0.2,
        freeModeMinimumVelocity: 0.01,
        freeModeMomentumBounce: 2,
        containerModifierClass: 'hero-catalog-',
        navigation: {
            nextEl: '.hero-next-btn',
            prevEl: '.hero-prev-btn'
        },
        pagination: {
            el: '.hero-pag',
            type: 'bullets',
            clickable: true
        }
    })
    const moreDetailsAll = vars.$catalogSlider.querySelectorAll('.js-catalog-slide__more')
    const hiddenDetailsAll = vars.$catalogSlider.querySelectorAll('.js-catalog-slide__hidden')

    // обработчик клика по кнопке показать больше в слайде
    moreDetailsAll.forEach( el => {
        el.addEventListener('click', e => {
            const current = e.target
            const descriptionBlock = current.closest('.catalog-slide__description')
            const detailsBlock = descriptionBlock.querySelector('.catalog-slide__details')
            const shortBlock = descriptionBlock.querySelector('.catalog-slide__short-desc')
            const hiddenDetails = descriptionBlock.querySelector('.js-catalog-slide__hidden')
            const maxHeightDetailsBlock = getComputedStyle(descriptionBlock).maxHeight
            descriptionBlock.style.height = getComputedStyle(descriptionBlock).height
            shortBlock.style.height = getComputedStyle(shortBlock).height
            detailsBlock.classList.remove('is-delete')
            current.classList.add('is-reduce')
            setTimeout(() => {
                descriptionBlock.style.height = maxHeightDetailsBlock
                detailsBlock.classList.remove('is-reduce')
                shortBlock.classList.add('is-reduce')
                hiddenDetails.classList.remove('is-delete')
            } ,100)
        })
    })

    // обработчик клика по кнопке скрыть в слайде
    hiddenDetailsAll.forEach( el => {
        el.addEventListener('click', e => {
            const current = e.target
            const descriptionBlock = current.closest('.catalog-slide__description')
            const detailsBlock = descriptionBlock.querySelector('.catalog-slide__details')
            const shortBlock = descriptionBlock.querySelector('.catalog-slide__short-desc')
            const moreDetails = descriptionBlock.querySelector('.js-catalog-slide__more')
            const minHeightDetailsBlock = getComputedStyle(descriptionBlock).minHeight
            descriptionBlock.style.height = parseInt(minHeightDetailsBlock) + 10 + "px"
            detailsBlock.classList.add('is-reduce')
            current.classList.add('is-delete')
            setTimeout(() => {
                detailsBlock.classList.add('is-delete')
                shortBlock.classList.remove('is-reduce')
                moreDetails.classList.remove('is-reduce')
                moreDetails.classList.remove('is-delete')
            } ,100)
        })
    })

    // обработчик кнопки ПОКАЗАТЬ КАТЕГОРИЮ в слайде находиться в файле catalog-grid.js
}