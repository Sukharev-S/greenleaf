import vars from '../_vars'

if (vars.$marketing) {
    let counter = 0
    let delay = 10000   // Появление блока marketing

    const data = [
        {
            title: 'Title of product 1',
            where: 'Moscow, Russia'
         },
        {
            title: 'Title of product 2',
            where: 'Kiev, Ukraine'
        },
        {
            title: 'Title of product 3',
            where: 'Rome, Italy'
        }
    ]

    const closeMarketing = () => {
        vars.$marketing.classList.remove('marketing--visible')
    }

    const changeMarketingData = () => {
        vars.$marketing.classList.add('marketing--visible')
        // Через 5 сек(10-5) удаляется из виду блок marketing
        setTimeout(() => {
            vars.$marketing.classList.remove('marketing--visible')
        }, delay - 5000)
        const stringTitle = `${data[counter].title}`
        const stringWhere = `15 minutes ago ${data[counter].where}`
        vars.$marketing.querySelector('.marketing__title').textContent = stringTitle
        vars.$marketing.querySelector('.marketing__when-from').textContent = stringWhere
        counter++
        if (counter === data.length) {
            counter = 0
        }
    }

    setInterval(changeMarketingData, delay)

    vars.$marketing.addEventListener('click', e => {
        if (e.target.closest('.marketing__close')) {
            closeMarketing()
        }
        if (e.target.closest('.marketing__eye')) {
            vars.$marketing.classList.remove('marketing--visible')
            vars.$marketing.classList.add('is-hidden')
            setTimeout(() => vars.$marketing.remove(), delay - 2000)
        }
    })
}