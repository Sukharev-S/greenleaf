import vars from '../_vars'
import {throttler as resizeThrottler} from '../functions/trottler'
import {controlLoadElement} from '../functions/controlLoadElement'
import {disableProducts} from '../functions/productActions'
import {fetchProducts, renderProducts} from '../functions/productActions'
import {removeElements, showElements} from '../functions/animate'

if (vars.$mainProductsList) {  // если находимся на главной странице

    // --- scrollTrigger
    // ------- подключение паралакса на логотип при скролле
    const scrollLogo = function () {
            gsap.to('.hero__content', {
                scrollTrigger: {
                    trigger: '.hero',
                    start: 'top',
                    end: 'bottom',
                    scrub: true,
                },
                y: (i, target) => ScrollTrigger.maxScroll(window) * target.dataset.speed,
                ease: 'none',
            })
        }
    // --- Инициация паралаксного скролла Логотипа
    scrollLogo()

    // ------- получает кол-во элементов в линию в зависимости от ширины экрана
    const getQuantityPerLine = function() {
        return window.innerWidth > 1200 ? 5 : window.innerWidth < 576 ? 2 : 3
    }

    let currentWidth = window.innerWidth,
        elementsPerLine = getQuantityPerLine(),             // кол-во отображаемых товаров в строке списка
        expectedProducts = elementsPerLine * 1,             // кол-во запрошенных на показ товаров (два ряда)
        displayedProducts = 0,                              // кол-во уже отображенных на экране товаров
        mainProductTabUrl = '/main-page/popular',           // адрес выборки данных по товарам (зависит от табов)
        dataStore = []                                      // данные из fetch запроса по товарам

    const delayShowProducts = 75
    const delayRemoveProducts = 75
    const mainProductsTabs = vars.$mainProducts.querySelectorAll('.main-products__btn')

    // ------- перезагрузка товаров на главной странице (при изменении размеров окна)
    const reloadProductMainPage = function() {
        elementsPerLine = getQuantityPerLine()
        if (vars.$mainProductsList) {
            if (currentWidth !== window.innerWidth) {
                currentWidth = window.innerWidth
                displayedProducts = 0
                expectedProducts = elementsPerLine * 2
                removeElements(vars.$mainProducts.querySelectorAll('.products-grid__item'), delayRemoveProducts, 'emergence')
                    .then(() => loadProductsMainPage())
            }
        }
    }

    // ------- загрузка товаров на главной странице
    const loadProductsMainPage = function() {
        vars.$mainProductsMore.style.display = 'inline-flex'
        fetchProducts(mainProductTabUrl)
            .then((data) => {
                dataStore = [...data]
                return renderProducts(data, vars.$mainProductsList, 'products-grid__item', displayedProducts, expectedProducts)
            })
            .then((goods) => Promise.all(goods.map(controlLoadElement)))
            .then((images) => {
                let newProducts = images.map(el => el.closest('.products-grid__item'))
                let heightProductItem = parseInt(getComputedStyle(newProducts[0].closest('.products-grid__item')).height) +
                    parseInt(getComputedStyle(newProducts[0].closest('.products-grid__item')).marginBottom)
                if (expectedProducts >= dataStore.length) {
                    expectedProducts = dataStore.length
                    vars.$mainProductsMore.classList.add('is-reduce')
                    vars.$mainProductsMore.classList.add('is-disabled')
                } else {
                    vars.$mainProductsMore.classList.remove('is-reduce')
                    vars.$mainProductsMore.classList.remove('is-disabled')
                }
                vars.$mainProductsList.style.height = heightProductItem * Math.ceil(expectedProducts / elementsPerLine) + 'px'
                disableProducts()
                showElements(newProducts, delayShowProducts, 'emergence')
            }).catch(err => console.error(err))
    }

    // ------- обработчик изменения размера экрана, с использованием троттлинга
    window.addEventListener("resize", () => resizeThrottler(reloadProductMainPage, 3000), false)

    // ------- клик по кнопке загрузить еще товары на главной странице
    vars.$mainProductsMore.addEventListener('click', e => {
        displayedProducts = expectedProducts
        expectedProducts += elementsPerLine
        loadProductsMainPage()
    })

    // ------- клик по категориям (табам)
    mainProductsTabs.forEach( btn => {
        btn.addEventListener('click', e => {
            if (!e.currentTarget.classList.contains('main-products__btn--current')) {
                vars.$mainProducts.querySelectorAll('.main-products__btn').forEach( btn => {
                    btn.classList.remove('main-products__btn--current')
                })
                e.currentTarget.classList.add('main-products__btn--current')
                switch (e.currentTarget.dataset.filter) {
                    case 'best-sellers':
                        mainProductTabUrl = '/main-page/popular'
                        break;
                    case 'new-products':
                        mainProductTabUrl = '/main-page/new'
                        break;
                    case 'sale-products':
                        mainProductTabUrl = '/main-page/sale'
                        break;
                }
                displayedProducts = 0
                expectedProducts = elementsPerLine * 2
                mainProductsTabs.forEach(el => el.classList.add('is-disabled'))
                removeElements(vars.$mainProducts.querySelectorAll('.products-grid__item'), delayRemoveProducts , 'emergence')
                    .then(() => {
                        loadProductsMainPage()
                        setTimeout(() => {
                            mainProductsTabs.forEach(el => el.classList.remove('is-disabled'))
                        }, 1500)
                    })
                    .catch(err => console.error(err))
            }
        })
    })
    loadProductsMainPage()
}