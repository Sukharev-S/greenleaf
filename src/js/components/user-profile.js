import {isValidate, clearingFormFields, validateShow} from "../functions/validation";

if (document.querySelector('.profile-page')) {
    const
        labelAccountEdit = document.querySelector('.js-account__data-edit'),
        labelPassEdit = document.querySelector('.js-account__pass-edit'),
        labelAddressEdit = document.querySelector('.js-address__data-edit'),
        checkBoxAddressEdit = document.querySelector('.js-address__data-edit input'),
        checkBoxPassEdit = document.querySelector('.js-account__pass-edit input'),
        checkBoxAccountEdit = document.querySelector('.js-account__data-edit input'),
        editAddressForm = document.querySelector('.address-form'),
        editPasswordForm = document.querySelector('.form__edit-password'),
        editAccountForm = document.querySelector('.form__edit-data-acc'),
        loader = document.querySelector('.js-global-loader'),
        orders = document.querySelectorAll('.order'),
        delayProfileMessageText = 5000,
        FILL_ALL_FORM_FIELDS = 'Заполните все поля формы',
        FILL_ALL_FORM_FIELDS_CORRECTLY = 'Заполните поля формы правильно',
        PROF_MESSAGE_TEXT_ADDRESS = 'Следующий адрес будет использоваться на странице оформления заказа по умолчанию',
        PROF_MESSAGE_TEXT_ACCOUNT = 'Следующий контакт будет использоваться на странице оформления заказа по умолчанию'


    // для чистоты кода, работа с сообщением и лоадером
    const getResponseMessage = (text, responseMessage) => {
        responseMessage.classList.remove('is-reduce')
        setTimeout(() => responseMessage.classList.add('is-reduce'), delayProfileMessageText)
        responseMessage.textContent = text
    }

    // для чистоты кода, работа с изменением чекбоксов
    const checkBoxChanged = (form) => {
        form.classList.toggle('is-reduce')
        clearingFormFields(form)
    }

    // для чистоты кода, проверка валидации форм и отправка на сервер
    const submitAddressOrAccountForm = (form, responseMessage, infoFields, url) => {
        let formFields = form.querySelectorAll('.form-field'),
            countEmptyFields = 0,
            countErrorFields = 0

        loader.classList.remove('is-delete')
        formFields.forEach(field => {
            if (field.value === '') countEmptyFields++
            if (!isValidate(field)) countErrorFields++
        })

        if (countEmptyFields !== 0) {
            loader.classList.add('is-delete')
            // responseMessage.classList.remove('response-message--success')
            formFields.forEach(field => validateShow(field))
            // getResponseMessage(FILL_ALL_FORM_FIELDS, responseMessage)
        } else {
            if (countErrorFields === 0) {
                let formData = new FormData(form)
                fetch(url, {
                    method: "POST",
                    body: formData
                })
                    .then((response) => response.json())
                    .then((result) => {
                        let dataInfo = result['dataInfo']
                        setTimeout(() => {
                            if (result['is_ok']) {
                                responseMessage.classList.add('response-message--success')
                                // Изменение данных в блоке информации по умолчанию
                                infoFields.forEach(fieldInfo => {
                                    let info = fieldInfo.dataset.infoSet
                                    if (dataInfo[info]) fieldInfo.textContent = dataInfo[info]
                                })
                                // сброс визуализации с кнопки формы, если ввод данных был первый раз
                                form.querySelector('[type="submit"]').classList.remove('waves')

                                const currentTab = form.closest('.tab-content__tab')
                                const profMessageText = currentTab.querySelector('.prof-messages__text')
                                // изменение текста сообщения, после ввода данных
                                if (currentTab.classList.contains('address')) {
                                    profMessageText.textContent = PROF_MESSAGE_TEXT_ADDRESS
                                    checkBoxAddressEdit.click()
                                }
                                if (currentTab.classList.contains('account')) {
                                    profMessageText.textContent = PROF_MESSAGE_TEXT_ACCOUNT
                                    checkBoxAccountEdit.click()
                                }
                            } else {
                                // responseMessage.classList.remove('response-message--success')
                            }
                            loader.classList.add('is-delete')
                            // getResponseMessage(result['message'], responseMessage)
                            clearingFormFields(form)
                        }, 1000)
                    }).catch(err => console.error(err))
            } else {
                loader.classList.add('is-delete')
                // getResponseMessage(FILL_ALL_FORM_FIELDS_CORRECTLY, responseMessage)
                // responseMessage.classList.remove('response-message--success')
                formFields.forEach(field => validateShow(field))
            }
        }
    }

    // клик по чекбоксу изменить данные адреса на странице профиля
    checkBoxAddressEdit.addEventListener('change', e => {
        checkBoxChanged(editAddressForm, labelAddressEdit)
    })

    // клик по чекбоксу смены пароля на странице профиля
    checkBoxPassEdit.addEventListener('change', e => {
        checkBoxChanged(editPasswordForm, labelPassEdit)
    })

    // клик по чекбоксу изменить данные аккаунта на странице профиля
    checkBoxAccountEdit.addEventListener('change', e => {
        checkBoxChanged(editAccountForm, labelAccountEdit)
    })

    // клик по ордеру из списка имеющихся ордеров в профиле
    orders.forEach(order => {
        order.addEventListener('click', e => {
            if (!e.target.closest('.order-details__product-item')) {
                let currentOrderDetails = document.querySelector(`.order-details[data-id-order="${e.currentTarget.dataset.idOrder}"]`)
                currentOrderDetails.classList.toggle('is-reduce-mh')
            }
        })
    })

    // submit кнопки изменения/добавления адреса в профиле пользователя
    editAddressForm.addEventListener('submit', e => {
        e.preventDefault()
        const responseMessageAddress = editAddressForm.querySelector('.address .response-message')
        const addressInfoFields = document.querySelectorAll('.address-data__info')
        const url = "/address-update"
        submitAddressOrAccountForm(editAddressForm, responseMessageAddress, addressInfoFields, url)
    })

    // submit кнопки изменения пароля в профиле пользователя
    editPasswordForm.addEventListener('submit', e => {
        e.preventDefault()
        const responseMessagePassword = editPasswordForm.querySelector('.form__edit-password .response-message')
        let formFields = editPasswordForm.querySelectorAll('.form-field'),
            countEmptyFields = 0,
            countErrorFields = 0

        loader.classList.remove('is-delete')
        formFields.forEach(field => {
            if (field.value === '') countEmptyFields++
            if (!isValidate(field)) countErrorFields++
        })
        if (countEmptyFields !== 0) {       // если хоть одно поле не заполнено
            loader.classList.add('is-delete')
            responseMessagePassword.classList.remove('response-message--success')
            formFields.forEach(field => validateShow(field))
            getResponseMessage(FILL_ALL_FORM_FIELDS, responseMessagePassword)
        } else {
            let formData = new FormData(editPasswordForm),
                newPass = formData.get('new_password'),
                confirmPass = formData.get('confirm_password')
            if (newPass !== confirmPass) {      // если новый пароль и подтвержденный не совпали
                loader.classList.add('is-delete')
                responseMessagePassword.classList.remove('response-message--success')
                getResponseMessage('Подтвержденный пароль не совпадает', responseMessagePassword)
            } else {
                if (countErrorFields === 0) {       // если пароли введены по правилам верификации
                    fetch("/change-password", {
                        method: "POST",
                        body: formData
                    })
                        .then((response) => response.json())
                        .then((result) => {
                            setTimeout(() => {
                                if (result['is_ok']) {
                                    responseMessagePassword.classList.add('response-message--success')
                                    clearingFormFields(editPasswordForm)
                                } else {
                                    responseMessagePassword.classList.remove('response-message--success')
                                }
                                loader.classList.add('is-delete')
                                getResponseMessage(result['message'], responseMessagePassword)
                            }, 1000)
                        }).catch(err => console.error(err))
                } else {
                    loader.classList.add('is-delete')
                    getResponseMessage('Пароль должен содержать хотя бы одно число и состоять минимум из 8 символов', responseMessagePassword)
                    responseMessagePassword.classList.remove('response-message--success')
                    formFields.forEach(field => validateShow(field))
                }
            }
        }
    })

    // submit кнопки изменения/добавления данных учетной записи в профиле пользователя
    editAccountForm.addEventListener('submit', e => {
        e.preventDefault()
        const responseMessageAccount = editAccountForm.querySelector('.form__edit-data-acc .response-message')
        const accountInfoFields = document.querySelectorAll('.account-data__info')
        const url = "/account-update"
        submitAddressOrAccountForm(editAccountForm, responseMessageAccount, accountInfoFields, url)
    })
}