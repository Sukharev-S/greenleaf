import vars from "../_vars";
import {removeElements} from "../functions/animate";

if (vars.$adminPanel) {
    const choiceItems = document.querySelectorAll('.main-choice__item')
    const promotionForm = vars.$adminPanel.querySelector('.js-promotion-form')
    const adminPanelMessage = document.querySelector('.admin-panel__message')

    const admProductSearchBlock = vars.$adminPanel.querySelector('.js-adm-search-product')
    const admProductResultCountBlock = vars.$adminPanel.querySelector('.adm-product .js-adm-result__count-block')
    const admProductResultCount = vars.$adminPanel.querySelector('.adm-product .js-adm-result__count')
    const admSearchFormProduct = vars.$adminPanel.querySelector('.js-adm-search-product__form')
    const admProductSearchFormField = vars.$adminPanel.querySelector('.js-adm-search-product__field')
    const admResultSearchBlockProduct = vars.$adminPanel.querySelector('.adm-result__product-list')
    const admProductSearchLoader = vars.$adminPanel.querySelector('.js-adm-search-product-loader')
    const admProductBlancForm = vars.$adminPanel.querySelector('.adm-product__blanc')

    const admOrderSearchBlock = vars.$adminPanel.querySelector('.js-adm-search-order')
    const admOrderResultCountBlock = vars.$adminPanel.querySelector('.adm-order .js-adm-result__count-block')
    const admOrderResultCount = vars.$adminPanel.querySelector('.adm-order .js-adm-result__count')
    const admSearchFormOrder = vars.$adminPanel.querySelector('.js-adm-search-order__form')
    const admOrderSearchFormField = vars.$adminPanel.querySelector('.js-adm-search-order__field')
    const admResultSearchBlockOrder = vars.$adminPanel.querySelector('.adm-result__order-list')
    const admOrderSearchLoader = vars.$adminPanel.querySelector('.js-adm-search-order-loader')

    const delayInput = 2000      // задержка отрабатывания ввода в поле поиска
    let inputFlag                //  технический флаг для троттлинга ввода в поле поиска
    let headerHeight = parseInt(getComputedStyle(vars.$header).height)
    let adminContentCoordinates = vars.$adminPanel.querySelector('.admin-panel__content').getBoundingClientRect().top
//-------------------------- Редактирование продукта
    const blancOne_id = admProductBlancForm.querySelector('.js-blanc-id-product')
    const blancOne_name = admProductBlancForm.querySelector('.js-blanc__name')
    const blancOne_isActive = admProductBlancForm.querySelector('.blanc__active .custom-checkbox__input')
    const blancOne_similarList = admProductBlancForm.querySelector('.blanc__similar-list')
    const blancOne_similarAddBtn = admProductBlancForm.querySelector('.blanc__similar-add-btn')
    const blancOne_price = admProductBlancForm.querySelector('.js-blanc__price')
    const blancOne_oldPrice = admProductBlancForm.querySelector('.js-blanc__old-price')
    const blancOne_stock = admProductBlancForm.querySelector('.js-blanc__stock')
    const blancOne_quantityOrdered = admProductBlancForm.querySelector('.js-blanc__quantity_ordered')
    const blancOne_dateDelivery = admProductBlancForm.querySelector('.js-blanc__date_delivery')
    const blancOne_life = admProductBlancForm.querySelector('.js-blanc__life')
    const blancOne_qPackage = admProductBlancForm.querySelector('.js-blanc__package')
    const blancOne_weight = admProductBlancForm.querySelector('.js-blanc__weight')
    const blancOne_volume = admProductBlancForm.querySelector('.js-blanc__volume')
    const blancOne_height = admProductBlancForm.querySelector('.js-blanc__height')
    const blancOne_width = admProductBlancForm.querySelector('.js-blanc__width')
    const blancOne_depth = admProductBlancForm.querySelector('.js-blanc__depth')
    const blancOne_article = admProductBlancForm.querySelector('.js-blanc__article')
    const blancOne_isPopular = admProductBlancForm.querySelector('.blanc__article [name="is_popular"]')
    const blancOne_isNew = admProductBlancForm.querySelector('.blanc__article [name="is_new"]')
    const blancOne_isSale = admProductBlancForm.querySelector('.blanc__article [name="is_sale"]')
    const blancOne_indexPopular = admProductBlancForm.querySelector('.js-blanc__index-popular')
    const blancOne_categoriesList = admProductBlancForm.querySelector('.blanc__categories-list')
    const blancOne_short = admProductBlancForm.querySelector('.js-blanc__short')
    const blancOne_description = admProductBlancForm.querySelector('.js-blanc__description')
    const blancOne_structure = admProductBlancForm.querySelector('.js-blanc__structure')
    const blancOne_recommendation = admProductBlancForm.querySelector('.js-blanc__recommendation')
    const blancOne_scope = admProductBlancForm.querySelector('.js-blanc__scope')
    const blancOne_submit_btn = admProductBlancForm.querySelector('.blanc__save-btn')
//-------------------------------

    // --- функция для обработки полученного ответа с сервера
    const showResponseMessage = (result) => {
        setTimeout(() => {
            if (result['is_ok']) adminPanelMessage.classList.add('admin-panel__message--success')
            else adminPanelMessage.classList.remove('admin-panel__message--success')
            vars.$loader.classList.add('is-delete')
            adminPanelMessage.textContent = result['message']
            adminPanelMessage.classList.add('admin-panel__message--show')
            setTimeout(() => adminPanelMessage.classList.remove('admin-panel__message--show'),5000)
        }, 750)
    }

    // --- функция для обработчика submit GET формы
    const submitGetForm = (url, resultGetFunction) => {
        fetch(url, {
            method: "GET"
        })
            .then((response) => response.json())
            .then((result) => {
                resultGetFunction(result)
                setTimeout(() => vars.$loader.classList.add('is-delete'), 300)
            })
            .catch(err => console.error(err))
    }

    // --- функция для обработчика submit POST формы
    const submitPostForm = (form, url, checkCategories = '', checkSimilar = '') => {
        let formData = new FormData(form)
        if (checkCategories !== '') formData.append('id_categories', checkCategories);
        if (checkSimilar !== '') formData.append('id_similar', checkSimilar);
        vars.$loader.classList.remove('is-delete')
        // for (let pair of formData.entries()) {
        //     console.log(pair[0]+ ', ' + pair[1]);
        // }
        fetch(url, {
            method: "POST",
            body: formData
        })
            .then((response) => response.json())
            .then((result) => showResponseMessage(result))
            .catch(err => console.error(err))
    }

    // --- Функция рендеринга HTML для формы изменения описания категорий (короткого и полного)
    const renderHTMLCategoryDescription = (descriptionType, descriptionData, activeContent) => {
        activeContent.insertAdjacentHTML('beforeend', `
            <div class="adm-content__row">
                <div class="adm-content__header adm-content__col-1">Категория</div>
                <div class="adm-content__header adm-content__col-2">${descriptionType}</div>
                <div class="adm-content__header adm-content__col-3"></div>
            </div>
        `)
        descriptionData['dataInfo'].forEach(el => {
            activeContent.insertAdjacentHTML('beforeend', `
                <form class="adm-content__row js-category-form" action="#">
                   <div class="adm-content__cell adm-content__col-1 adm-content__name">${el.name}</div>
                   <div class="adm-content__cell adm-content__col-2">
                        <textarea class="adm-content__text"
                                type="text"
                                name="text"
                                wrap="soft"
                                rows="5">${descriptionType === 'Краткое описание' ? el.short : el.description}</textarea>
                    </div>
                    <div class="adm-content__cell adm-content__col-3">
                        <button class="btn-reset btn-default adm-content__btn" 
                                type="submit" 
                                data-id-category="${el.id}">Изменить</button>
                    </div>
                    <input class="js-category__id-element" type="hidden" name="id_element" value="${el.id}">
                </form>
            `)
        })
    }

    // --- Функция обработчика submit форм редактирования описания категорий (короткого и полного)
    const handlerThisCategoryEdit = (postUrl) => {
        document.querySelectorAll('.js-category-form').forEach(el => {
            el.addEventListener('submit', e => {
                e.preventDefault()
                submitPostForm(e.target, postUrl)
            })
        })
    }

    // --- Функция рендеринга HTML для блока результатов поиска товаров для редактирования
    const renderHTMLResultProduct = (products) => {
        products.forEach((product, i) => admResultSearchBlockProduct.insertAdjacentHTML('beforeend', `
            <li class="adm-result__product-item adm-result__item adm-result__item--show is-reduce-mh">
                <div class="adm-result__product-article adm-result__product-number">${++i}</div>
                <div class="adm-result__product-img">
                    <img src="${product['main_image']}" alt="${product['name']}">
                </div>
                <div class="adm-result__product-article">${product['article']}</div>
                <div class="adm-result__product-name">${product['name']}</div>
                <form class="adm-result__product-edit" data-id-product-edit="${product['id_product']}">
                    <button class="adm-result__product-btn btn-reset btn-default">Редактировать</button>
                </form>
            </li>
        `))
    }

    // --- Функция рендеринга HTML для блока результатов поиска ордеров для редактирования статуса
    const renderHTMLResultOrder = (orders, statusList) => {
        let HTMLStringProductList = ''
        let HTMLStringOrderAddress = ''
        let HTMLStringOrderClient = ''
        let HTMLStringOrderStatusList = ''

        statusList.forEach((status, i) => {             //  сборка HTML строки списка возможных статусов заказа
            HTMLStringOrderStatusList += `
                <li class="custom-select__item" data-status-id="${statusList[i]['id_status']}">${statusList[i]['status_name']}</li>
            `
        })

        orders.forEach(order => {
            let userInfo = JSON.parse(order['user_info']),
                userFirstName = '', userLastName = '', userMiddleName = '',
                userCountry, userCity, userStreet, userPostcode,
                userPhone, userEmail,
                userDateReg = ''
            HTMLStringProductList = ''
            HTMLStringOrderAddress = ''
            HTMLStringOrderClient = ''

            //  сборка HTML строки инофрмации по контактам клиента с учетом авторизации
            if (userInfo === null) {               // если юзер авторизован и данные не изменяли при заказе
                userFirstName = order['first_name']
                userLastName = order['last_name']
                userMiddleName = order['middle_name']
                userPhone = order['phone']
                userEmail = order['email']
                userDateReg = order['date_reg']
                if (order['method_delivery'] === 'delivery') {
                    userCountry = order['country']
                    userCity = order['city']
                    userStreet = order['street']
                    userPostcode = order['postcode']
                }
            } else if (order['last_name'] != null) { // если юзер авторизован и заполнен, но изменил данные в ордере
                userFirstName = userInfo['firstName']
                userLastName = userInfo['lastName']
                userMiddleName = userInfo['middleName']
                userPhone = userInfo['phone']
                userEmail = userInfo['email']
                userDateReg = 'Клиент изменил данные!'
                if (order['method_delivery'] === 'delivery') {
                    userCountry = userInfo['country']
                    userCity = userInfo['city']
                    userStreet = userInfo['street']
                    userPostcode = userInfo['postcode']
                }
            } else {                                // если юзер неавторизован
                userPhone = userInfo['phone']
                userEmail = userInfo['email']
                userDateReg = 'Клиент не авторизован!'
                if (order['method_delivery'] === 'delivery') {
                    userFirstName = userInfo['firstName']
                    userLastName = userInfo['lastName']
                    userMiddleName = userInfo['middleName']
                    userCountry = userInfo['country']
                    userCity = userInfo['city']
                    userStreet = userInfo['street']
                    userPostcode = userInfo['postcode']
                } else {
                    userFirstName = userInfo['full_name']
                }
            }

            order['products'].forEach(product => {          //  сборка HTML строки списка товара к каждому заказу
                HTMLStringProductList += `
                    <li class="order-details__product-item">
                        <a class="order-details__product-name" 
                           href="/product/${product['id_product']}"
                           target="_blank">${product['name']}</a>
                        <div class="order-details__product-quantity">x ${product['quantity']}</div>
                        <div class="order-details__product-price">
                            <span class="js-conventional-unit">₽</span>
                            <span class="order-details__price-sum">${product['sum_rub']}</span>
                        </div>
                    </li>
                `
            })

            if (order['method_delivery'] === 'delivery') {   //  сборка HTML строки инофрмации по адресу клиента
                HTMLStringOrderAddress = `
                    <div class="order__line-details">
                        <div class="order__line-item">
                            <span class="order__item-title">Страна: </span> ${userCountry}
                        </div>
                        <div class="order__line-item">
                            <span class="order__item-title">Населенный пункт: </span> ${userCity}
                        </div>
                        <div class="order__line-item">
                            <span class="order__item-title">Улица, дом, кв.: </span> ${userStreet}
                        </div>
                        <div class="order__line-item">
                            <span class="order__item-title">ИНДЕКС: </span> ${userPostcode}
                        </div>
                    </div>
                `
            }

            HTMLStringOrderClient = `
                <div class="order__line-details">
                    <div class="order__line-item">
                        <span class="order__item-title"></span> 
                        ${userFirstName} ${userLastName} ${userMiddleName}
                    </div>
                    <div class="order__line-item">
                        <span class="order__item-title"></span>
                        ${userPhone}
                    </div>
                   <div class="order__line-item">
                        <span class="order__item-title"></span>
                        ${userEmail}
                    </div>
                   <div class="order__line-item">
                        <span class="order__item-title"></span>
                        ${userDateReg}
                    </div>
                </div>
            `

            //  рендер самого заказа со всеми данными
            admResultSearchBlockOrder.insertAdjacentHTML('beforeend', `
                <li class="adm-result__order-item adm-result__item adm-result__item--show is-reduce-mh">
                    <article class="order" data-status-id="${order['id_status']}" data-id-order="${order['id_order']}">
                        <div class="order__status">
                            <div class="order__status-wrap">
                                <div class="order__status-label">Статус заказа:</div>
                                <div class="custom-select" tabindex="0">
                                    <div class="custom-select__top">${order['status_name']}</div>
                                    <div class="custom-select__dropdown">
                                        <ul class="custom-select__list">
                                            ${HTMLStringOrderStatusList}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <form class="js-adm-order__status-form">
                                <input class="js-adm-order__order-field" type="hidden" name="id_order">
                                <input class="js-adm-order__status-field" type="hidden" name="id_status">
                            </form>
                            <div class="order__status-wrap">
                                <button class="btn-reset btn-default adm-content__btn order__status-change-btn" 
                                        type="submit">Применить изменения</button>
                            </div>
                        </div>
                        <div class="order__line-details">
                            <div class="order__line-item">
                                <span class="order__item-title">Номер заказа:</span> ${order['id_order']}
                            </div>
                            <div class="order__line-item">
                                <span class="order__item-title">Дата:</span> ${order['date_placed']}
                            </div>
                            <div class="order__line-item">
                                <span class="order__item-title">Итоговая сумма:</span>
                                ₽ ${order['sum_total_rub']}
                            </div>
                            <div class="order__line-item">
                                <span class="order__item-title">Метод доставки:</span>
                                ${order['method_delivery'] === 'pickup' ? "Самовывоз" : "Почтовая служба"}
                            </div>
                        </div>
                        ${HTMLStringOrderAddress}
                        ${HTMLStringOrderClient}
                        <div class="order__order-details order-details is-reduce-mh" data-id-order="${order['id_order']}">
                            <h3 class="order-details__title">Детали заказа</h3>
                            <div class="order-details__wrap-details">
                                <div class="order-details__sub-title">Заказанные товары</div>
                                <ul class="order-details__product-list">
                                    ${HTMLStringProductList}
                                </ul> <!-- /.check__product-list -->
                                <div class="order-details__line order-details__total-sum">
                                    <div class="order-details__sum-wrap">
                                        <span class="js-conventional-unit">₽</span>
                                        <span class="order-details__sum">${order['sum_total_rub'] - order['sum_delivery']}.00</span>
                                    </div>
                                </div>
                                <div class="order-details__line">
                                    <div class="order-details__sub-title">Доставка</div>
                                    <div class="order-details__sum-wrap">
                                        <span class="js-conventional-unit">₽</span>
                                        <span class="order-details__sum">
                                        ${order['sum_delivery']}
                                    </span>
                                    </div>
                                </div>
                                <div class="order-details__line">
                                    <div class="order-details__sub-title">Итого</div>
                                    <div class="order-details__sum-wrap">
                                        <span class="js-conventional-unit">₽</span>
                                        <span class="order-details__sum">
                                        ${order['sum_total_rub']}
                                    </span>
                                    </div>
                                </div>
                            </div> <!-- /.order-details__wrap-details -->
                        </div>
                    </article>
                </li>
            `)
        })
    }

    // --- Функция рендеринга HTML для блока результатов поиска товаров для редактирования при отсутствии результатов
    const renderHTMLResultEmpty = (block) => {
        block.insertAdjacentHTML('beforeend', `
            <li class="adm-result__product-item adm-result__order-item adm-result__item--show is-reduce-mh">
                <span class="adm-result__empty">По этому запросу ничего не найдено..</span>
            </li>
        `)
    }

    // --- Функция обработки результатов поиска товаров для редактирования
    const handlerProductSearchResults = (products) => {
        if (products.length > 0) {
            admProductResultCountBlock.style.opacity = "1"            //  отображение кол-ва найденных записей
            renderHTMLResultProduct(products)
        } else {
            admProductResultCountBlock.style.opacity = "0"
            admProductResultCount.textContent = "0"
            renderHTMLResultEmpty(admResultSearchBlockProduct)
        }
        let resultItems = vars.$adminPanel.querySelectorAll('.adm-result__product-item')
        let productEditForms = vars.$adminPanel.querySelectorAll('.adm-result__product-edit')
        // анимированный показ найденых элементов
        resultItems.forEach((item, i) => {
            let count = i + 1
            setTimeout(() => {
                admProductResultCount.textContent = count            //  счетчик кол-ва найденных записей
                item.classList.remove('is-reduce-mh')
            }, i * 100 + 500)
        })
        // если что-то нашли, то формам внутри вешаем обработчик
        if (productEditForms.length > 0) {
            productEditForms.forEach(form => {
                // --- обработчик нажатия кнопки выбора товара для редактирования
                form.addEventListener('submit', (e) => {
                    e.preventDefault()
                    handlerProductEdit(form)
                })
            })
        }
        admProductSearchLoader.classList.add('is-delete')
    }

    // --- Функция обработки результатов поиска ордеров для редактирования
    const handlerOrderSearchResults = (response) => {
        if (response['orders'].length > 0) {
            admOrderResultCountBlock.style.opacity = "1"            //  отображение кол-ва найденных записей
            renderHTMLResultOrder(response['orders'], response['statusList'])
        } else {
            admOrderResultCountBlock.style.opacity = "0"
            admOrderResultCount.textContent = "0"
            renderHTMLResultEmpty(admResultSearchBlockOrder)
        }
        let resultItems = vars.$adminPanel.querySelectorAll('.adm-result__order-item')
        let foundOrders = document.querySelectorAll('.order')
        // анимированный показ найденых элементов
        resultItems.forEach((item, i) => {
            let count = i + 1
            setTimeout(() => {
                admOrderResultCount.textContent = count            //  счетчик кол-ва найденных записей
                item.classList.remove('is-reduce-mh')
            }, i * 100 + 500)
        })
        // если что-то нашли, то ордерам внутри вешаем обработчик
        if (foundOrders.length > 0) {
            foundOrders.forEach(order => {
                // --- обработчик нажатия на ордер для просмотра его деталей
                order.addEventListener('click', e => {
                    // -- обработчик клика по заказу для разворота подробностей
                    if (!e.target.closest('.order-details__product-item') && !e.target.closest('.order__status')) {
                        let currentOrderDetails = document.querySelector(`.order-details[data-id-order="${e.currentTarget.dataset.idOrder}"]`)
                        currentOrderDetails.classList.toggle('is-reduce-mh')
                    }
                    // -- обработчик клика по селекту статуса заказа
                    if (e.target.closest('.custom-select')) {
                        e.currentTarget.classList.toggle('custom-select--open')
                        if (e.target.classList.contains('custom-select__item')) {
                            if (e.currentTarget.querySelector('.custom-select__top').textContent !== e.target.textContent) {
                                e.currentTarget.querySelector('.custom-select__top').textContent = e.target.textContent
                                e.currentTarget.classList.remove('custom-select--open')
                                e.currentTarget.querySelector('.custom-select__top').dataset.statusId = e.target.dataset.statusId
                            }
                        }
                    }
                    // -- обработчик клика по кнопке изменить статус заказа
                    if (e.target.closest('.order__status-change-btn')) {
                        e.preventDefault()
                        let currentOrder = e.target.closest('.order')
                        let  currentOrderForm = currentOrder.querySelector('.js-adm-order__status-form')
                        let  orderFormField = currentOrder.querySelector('.js-adm-order__order-field')
                        let  statusFormField = currentOrder.querySelector('.js-adm-order__status-field')
                        currentOrder.dataset.statusId = currentOrder.querySelector('.custom-select__top').dataset.statusId
                        orderFormField.value = currentOrder.dataset.idOrder
                        statusFormField.value = currentOrder.dataset.statusId
                        submitPostForm(currentOrderForm, '/admin-panel/order/status')
                    }

                })
            })
        }
        admOrderSearchLoader.classList.add('is-delete')
    }

    // --- Функция обработчика ввода в поле поиска товаров и заказов
    const handlerSearchField = (searchForm, resultItems, url, handlerFunction) => {
        // Проверка на последнее нажатие (ограничение срабатываний ввода в поле)
        if (!inputFlag) {
            inputFlag = setTimeout(function () {
                inputFlag = null
                let formData = new FormData(searchForm)
                // если есть элементы в поиске найденые ранее, то удаляем
                removeElements(resultItems, 50, 'adm-result__item--show',500)
                    .then(() => fetch(url, {            // запрос инфо по поисковой строке
                        method: "POST",
                        body: formData
                    })
                    .then((response) => response.json())
                    .then((result) => handlerFunction(result)))
                    .catch(err => console.error(err))
            }, delayInput)
        }
    }

    // --- Функция обработчика нажатия кнопки выбора товара для редактирования
    const handlerProductEdit = (form) => {
        let idProductEdit = form.dataset.idProductEdit
        admProductSearchBlock.classList.add('is-reduce-mh')
        admProductBlancForm.classList.remove('is-reduce')
        blancOne_categoriesList.querySelectorAll('.blanc__categories-subitem input')
            .forEach(el => el.checked = false)
        fetch(`/admin-panel/product/one_product/${idProductEdit}`, {
            method: "GET"
        })
            .then(response => response.json())
            .then(result => {
                let product = result['dataInfo']
                blancOne_id.value = product['id']
                blancOne_name.value = product['title']
                blancOne_isActive.checked = parseInt(product['is_active'])
                blancOne_similarList.innerHTML = ''
                product['similar'].forEach(similar => renderHTMLSimilar(similar))
                blancOne_price.value = product['price']
                blancOne_oldPrice.value = product['old_price']
                blancOne_stock.value = product['stock']
                blancOne_quantityOrdered.value = product['quantity_ordered']
                blancOne_dateDelivery.value = (product['date_delivery']) ? product['date_delivery'].split('-').reverse().join('-') : ''
                blancOne_life.value = product['life']
                blancOne_qPackage.value = product['package']
                blancOne_weight.value = product['weight']
                blancOne_volume.value = product['volume']
                blancOne_height.value = product['height']
                blancOne_width.value = product['width']
                blancOne_depth.value = product['depth']
                blancOne_article.value = product['article']
                blancOne_isPopular.checked = parseInt(product['is_popular'])
                blancOne_isNew.checked = parseInt(product['is_new'])
                blancOne_isSale.checked = parseInt(product['is_sale'])
                blancOne_indexPopular.value = product['index_popular']
                product['categories_by_product'].forEach( existCategory => {
                    const selector = `.blanc__categories-subitem input[value="${existCategory['id_category']}"]`
                    let targetItem = blancOne_categoriesList.querySelector(selector)
                    if (targetItem) targetItem.checked = true
                })
                blancOne_short.innerHTML = product['short']
                blancOne_description.innerHTML = product['description']
                blancOne_structure.innerHTML = product['structure']
                blancOne_recommendation.innerHTML = product['recommendation']
                blancOne_scope.innerHTML = product['scope']
                admProductBlancForm.classList.remove('is-reduce')
            }).catch(err => console.error(err))
    }

    // --- Функция рендеринга HTML для similar товаров в форме изменения товара (blanc)
    const renderHTMLSimilar = (similar) => {
        blancOne_similarList.insertAdjacentHTML('beforeend', `
            <li class="blanc__similar-item">
                <div class="blanc__similar-form">
                    <div class="blanc__similar-img">
                        <img src="${similar['main_image']}" alt="${similar['name']}">
                    </div>
                    <div class="blanc__similar-article">${similar['article']}</div>
                    <div class="blanc__similar-name">${similar['name']}</div>
                    <button class="blanc__similar-del-btn btn-reset btn-default js-similar-del">Убрать</button>
                    <input type="hidden" class="js-blanc__similar_id" value="${similar['id_product']}">
                </div>
            </li>
        `)
    }

    // --- Функция обработчика ввода в поля цен, для красоты ввода остатка .00
    const input00 = (input) => {
        let value = input.value;
        if (value) {
            if (value.match(/^[0-9]$/) !== null) input.value = value + '.00'
            if (input.value.split('.')[1].length > 2) input.value = value.split('.')[0] + '.00'
            input.setSelectionRange(input.value.length - 3, input.value.length - 3);
        }
    }

    // --- обработчик нажатия выбранной опции админ панели
    choiceItems.forEach(item => {
        item.addEventListener('click', e => {
            e.preventDefault()
            window.scrollTo(0 , adminContentCoordinates - headerHeight)
            document.querySelectorAll('.adm-content').forEach(el => el.classList.add('is-reduce-mh'))
            admProductBlancForm.classList.add('is-reduce')
            admProductSearchBlock.classList.add('is-reduce-mh')
            vars.$loader.classList.remove('is-delete')

            let activeOption = e.target.dataset.option
            let activeItem = e.target.dataset.item
            let activeContent = vars.$adminPanel.querySelector(`.adm-content[data-option="${activeOption}"]`)
            let admContentDurationAnimation =
                parseFloat(getComputedStyle(document.querySelector('.adm-content')).transitionDuration) * 1000
            let resultGetFunction = () => {}

            setTimeout(() => {
                if (activeOption === 'promotion') {     // При выборе опций из раздела РЕКЛАМА
                    resultGetFunction = (result) => {
                        let activeElementName = activeContent.querySelector('.js-promotion-element-name')
                        let activeElementText = activeContent.querySelector('.js-promotion-element-text')
                        let hiddenInput = activeContent.querySelector('.js-promotion__id-element')
                        activeContent.classList.remove('is-reduce-mh')
                        activeElementText.value = result['dataInfo']['element_text']
                        activeElementName.textContent = result['dataInfo']['element_description']
                        hiddenInput.value = result['dataInfo']['id_element']
                    }
                    submitGetForm(`/admin-panel/${activeOption}/${activeItem}`, resultGetFunction)
                }         // конец работы с рекламой
                if (activeOption === 'category') {      // При выборе опций из раздела КАТЕГОРИИ
                    if (activeItem === 'short_desc') {          // Если выбрано КОРОТКОЕ описание категорий
                        resultGetFunction = (result) => {
                            activeContent.innerHTML = ''
                            renderHTMLCategoryDescription('Краткое описание', result, activeContent)
                            handlerThisCategoryEdit('/admin-panel/category/short_desc') // --- обработчик submit
                            activeContent.classList.remove('is-reduce-mh')
                        }
                    }   // конец короткого описания категорий
                    if (activeItem === 'full_desc') {           // Если выбрано ПОЛНОЕ описание категорий
                        resultGetFunction = (result) => {
                            activeContent.innerHTML = ''
                            renderHTMLCategoryDescription('Полное описание', result, activeContent)
                            handlerThisCategoryEdit('/admin-panel/category/full_desc') // --- обработчик submit
                            activeContent.classList.remove('is-reduce-mh')
                        }
                    }   // конец полного описания категорий
                    submitGetForm(`/admin-panel/${activeOption}/${activeItem}`, resultGetFunction)
                }
                if (activeOption === 'product') {               // При выборе опций из раздела ТОВАРЫ
                    if (activeItem === 'one_product') {         // Если выбрана опция работы с одним товаром
                        vars.$loader.classList.add('is-delete')
                        activeContent.classList.remove('is-reduce-mh')
                        admProductSearchBlock.classList.remove('is-reduce-mh')
                        admProductBlancForm.classList.add('is-reduce')
                        admProductSearchFormField.focus()
                        admProductBlancForm.dataset.action = 'edit'
                        blancOne_submit_btn.textContent = 'Сохранить изменения'
                    }   // конец работы с одним товаром
                    if (activeItem === 'all_products') {    // Если выбрана опция работы со всеми товарами
                        resultGetFunction = (result) => {
                            // activeContent.innerHTML = ''
                            activeContent.insertAdjacentHTML('beforeend', `
                                    <div class="adm-content__row">
                                        <div class="adm-content__header adm-content__col-1">Название товара</div>
                                        <div class="adm-content__header adm-content__col-2">Артикль</div>
                                        <div class="adm-content__header adm-content__col-3">Цена</div>
                                        <div class="adm-content__header adm-content__col-4">Старая цена</div>
                                        <div class="adm-content__header adm-content__col-11">Кол-во в наличии</div>
                                        <div class="adm-content__header adm-content__col-12">Кол-во заказано</div>
                                        <div class="adm-content__header adm-content__col-13">Дата поставки</div>
                                        <div class="adm-content__header adm-content__col-14">Срок годности</div>
                                        <div class="adm-content__header adm-content__col-15">Вес</div>
                                        <div class="adm-content__header adm-content__col-16">Обьем</div>
                                        <div class="adm-content__header adm-content__col-17">В упаковке шт.</div>
                                        <div class="adm-content__header adm-content__col-18">Высота</div>
                                        <div class="adm-content__header adm-content__col-19">Ширина</div>
                                        <div class="adm-content__header adm-content__col-20">Глубина</div>
                                        <div class="adm-content__header adm-content__col-21">Популярный?</div>
                                        <div class="adm-content__header adm-content__col-22">Новинка?</div>
                                        <div class="adm-content__header adm-content__col-23">Акционный?</div>
                                        <div class="adm-content__header adm-content__col-24">Индекс популярности</div>
                                        <div class="adm-content__header adm-content__col-25">Активный?</div>
                                        <div class="adm-content__header adm-content__col-26"></div>
                                     </div>
                               `)
                            result['dataInfo'].forEach(el => {
                                activeContent.insertAdjacentHTML('beforeend', `
                                       <form class="adm-content__row js-product-form" action="#">
                                            <div class="adm-content__cell adm-content__col-1">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="article"
                                                       value="${el.title}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-2">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="title"
                                                       value="${el.article}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-3">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="price"
                                                       value="${el.price}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-4">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="old_price"
                                                       value="${el.old_price}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-11">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="stock"
                                                       value="${el.stock}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-12">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="quantity_ordered"
                                                       value="${el.quantity_ordered}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-13">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="date_delivery"
                                                       value="${el.date_delivery}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-14">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="life"
                                                       value="${el.life}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-15">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="weight"
                                                       value="${el.weight}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-16">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="volume"
                                                       value="${el.volume}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-17">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="package"
                                                       value="${el.package}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-18">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="height"
                                                       value="${el.height}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-19">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="width"
                                                       value="${el.width}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-20">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="depth"
                                                       value="${el.depth}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-21">
                                                <input class="adm-content__text"
                                                       type="checkbox"
                                                       name="is_popular"
                                                       value="${el.is_popular}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-22">
                                                <input class="adm-content__text"
                                                       type="checkbox"
                                                       name="is_new"
                                                       value="${el.is_new}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-23">
                                                <input class="adm-content__text"
                                                       type="checkbox"
                                                       name="is_sale"
                                                       value="${el.is_sale}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-24">
                                                <input class="adm-content__text"
                                                       type="text"
                                                       name="index_popular"
                                                       value="${el.index_popular}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-25">
                                                <input class="adm-content__text"
                                                       type="checkbox"
                                                       name="is_active"
                                                       value="${el.is_active}">
                                            </div>
                                            <div class="adm-content__cell adm-content__col-26">
                                                <button class="btn-reset btn-default adm-content__btn" type="submit" data-id-category="${el.id}">Изменить</button>
                                            </div>
                                            <input class="js-category__id-element" type="hidden" name="id_element" value="${el.id}">
                                        </form>
                                    `)
                            })
                            // --- обработчик submit форм product выбора all_products
                            document.querySelectorAll('.js-product-form').forEach(el => {
                                el.addEventListener('submit', e => {
                                    e.preventDefault()
                                    submitPostForm(e.target, '/admin-panel/product/all_products')
                                })
                            })
                            activeContent.classList.remove('is-reduce-mh')
                        }
                        submitGetForm(`/admin-panel/${activeOption}/${activeItem}`, resultGetFunction)
                    }   // конец работы сразу со всеми товарами
                    if (activeItem === 'add_product') {
                        vars.$loader.classList.add('is-delete')
                        activeContent.classList.remove('is-reduce-mh')
                        admProductSearchBlock.classList.add('is-reduce-mh')
                        admProductBlancForm.classList.remove('is-reduce')
                        admProductBlancForm.reset()
                        blancOne_similarList.innerHTML = ''
                        blancOne_short.innerHTML = ''
                        blancOne_description.innerHTML = ''
                        blancOne_structure.innerHTML = ''
                        blancOne_recommendation.innerHTML = ''
                        blancOne_scope.innerHTML = ''
                        blancOne_isActive.checked = true
                        admProductBlancForm.dataset.action = 'add'
                        blancOne_id.value = ''
                        blancOne_submit_btn.textContent = 'Добавить новый товар'
                    }
                }
                if (activeOption === 'order') {     // При выборе опций из раздела РАБОТА С ОРДЕРАМИ
                    if (activeItem === 'change_status_order') {         // Если выбрана опция изменения статуса заказа
                        vars.$loader.classList.add('is-delete')
                        activeContent.classList.remove('is-reduce-mh')
                        admOrderSearchBlock.classList.remove('is-reduce-mh')
                        admOrderSearchFormField.focus()
                    }
                }       // конец работы с опцией из раздела РАБОТА С ОРДЕРАМИ
            }, admContentDurationAnimation)
        })
    }) //-- конец обработчика нажатия выбранной опции

    // --- обработчик input-ов цен, чтобы оставались всегда .00
    blancOne_price.addEventListener('keyup', e => input00(e.target))
    blancOne_oldPrice.addEventListener('keyup', e => input00(e.target))

    // --- обработчик на клик по чекбоксу категорий в blanc редактирования товара,
    //      из-за глюка overflow: hidden при клике прыжок экрана
    blancOne_categoriesList.querySelectorAll('.blanc__categories-subitem').forEach(el => {
        el.addEventListener('click', e => {
            e.preventDefault()
            let checkBox = e.target.closest('.custom-checkbox').querySelector('.custom-checkbox__input')
            let status = checkBox.checked
            checkBox.checked = !status
        })
    })

    // --- Назначение обработчика на ввод в поле поиска перед редактированием одного товара
    admProductSearchFormField.addEventListener('input', e => {
        if (admProductSearchFormField.value.length > 2) {
            let resultItems = vars.$adminPanel.querySelectorAll('.adm-result__product-item')
            admProductSearchLoader.classList.remove('is-delete')
            handlerSearchField(admSearchFormProduct, resultItems, `/search/product`, handlerProductSearchResults)
        }
    })

    // --- Назначение обработчика на ввод в поле поиска перед редактированием статуса заказа
    admOrderSearchFormField.addEventListener('input', e => {
        // если число, то ищем с первого введенного символа
        let startSearch = Number.isInteger(parseInt(admOrderSearchFormField.value)) ? 0 : 2
        if (admOrderSearchFormField.value.length > startSearch) {
            let resultItems = vars.$adminPanel.querySelectorAll('.adm-result__order-item')
            admOrderSearchLoader.classList.remove('is-delete')
            handlerSearchField(admSearchFormOrder, resultItems, `/search/order`, handlerOrderSearchResults)
        }
    })

    // --- обработчик кнопки добавить similar
    blancOne_similarAddBtn.addEventListener('click', e => {
        e.preventDefault()
        let searchField = vars.$adminPanel.querySelector('.js-blanc-similar-input')
        let data = new FormData();
        data.append("search_word", searchField.value);
        // vars.$loader.classList.remove('is-delete')
        fetch(`/admin-panel/product/add-similar`, {
            method: "POST",
            body: data
        })
            .then((response) => response.json())
            .then((result) => {
                if (result['is_ok']) {
                    let isExist = false
                    let similar = result['dataInfo']
                    let checkSimilarProducts = blancOne_similarList.querySelectorAll('.js-blanc__similar_id')
                    checkSimilarProducts.forEach(el => {
                        if (similar['id_product'] === el.value) isExist = true
                    })
                    if (isExist) {
                        result = {
                            'is_ok': 0,
                            'message': 'Ошибка! Данный товар уже присутствует в этом списке похожих товаров'
                        }
                    }
                    else if (blancOne_id.value === similar['id_product']) {
                        result = {
                            'is_ok': 0,
                            'message': 'Ошибка! Попытка добавить редактируемый товар в свой же раздел похожие товары'
                        }
                    } else renderHTMLSimilar(similar)
                }
                showResponseMessage(result)
            })
            .catch(err => console.error(err))
    })

    // --- обработчик кнопок удалить в списке similar
    blancOne_similarList.addEventListener('click', e => {
        if (e.target.classList.contains('js-similar-del')) {
            let targetItem = e.target.closest('.blanc__similar-item')
            targetItem.classList.add('is-reduce-mh')
            setTimeout(() => targetItem.remove(), 500)
        }
    })

    // --- обработчик submit формы promotion
    promotionForm.addEventListener('submit', e => {
        e.preventDefault()
        submitPostForm(promotionForm, '/admin-panel/promotion/text')
    })

    // --- обработчик submit главной формы редактирования одного продукта
    admProductBlancForm.addEventListener('submit', e => {
        e.preventDefault()
        let url = ''
        let checkBoxesCategories = blancOne_categoriesList.querySelectorAll('.custom-checkbox__input')
        let checkSimilarProducts = blancOne_similarList.querySelectorAll('.js-blanc__similar_id')
        let checkCategories = []
        let checkSimilar = []
        checkBoxesCategories.forEach(el => {
            if (el.checked) checkCategories.push(el.value)
        })
        checkSimilarProducts.forEach(el => checkSimilar.push(el.value))
        if (admProductBlancForm.dataset.action === 'add') url = '/admin-panel/product/add_product'
        if (admProductBlancForm.dataset.action === 'edit') url = '/admin-panel/product/one_product'
        submitPostForm(admProductBlancForm, url, checkCategories, checkSimilar)
    })
}