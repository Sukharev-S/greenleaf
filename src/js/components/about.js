
// если находимся на странице О НАС

if (document.querySelector('#about')) {

    // --- scrollTrigger
    const scrollImages = function () {
        let windowWidth = window.innerWidth

        if (windowWidth > 768) {
            // ----- на изображения страницы КОМПАНИЯ
            gsap.to('.about__block-2 .about__image', {
                scrollTrigger: {
                    scrub: 2
                },
                xPercent: -15,
            })
        }
    }

    // --- Инициация движение изображений по скроллу
    scrollImages()
}