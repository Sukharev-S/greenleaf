// --- закрытие полоски рекламы вверху
import vars from "../_vars";

if (vars.$freeDelivery) {
    vars.$freeDeliveryBtn.addEventListener('click', e => {
        const main = document.querySelector('main:not(.main-index)')
        const freeDeliveryHeight = parseInt(getComputedStyle(vars.$freeDelivery).height)
        vars.$freeDelivery.classList.add('free-delivery--hidden')
        vars.$header.style.paddingTop = '0'
        main.style.marginTop = parseInt(getComputedStyle(main).marginTop) - freeDeliveryHeight + 'px'
        setTimeout(() => vars.$freeDeliveryBtn.remove(), 300)
    })
}