import {getTotalPrice} from "./calculate";

// убирает пробелы и символы валют
export const priceWithoutSpaces = (str = '') => {
    return str.replace(/\s|\$|€|₽/g, '')
}

// добавление читабельности (пробелов) в конечном числе
export const productionPrice = (price) => {
    let parts = price.toString().split(".")
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ")
    return parts.join(".")
}

// вывод конечной суммы в заданное место
export const printTotalPrice = (place) => {
    if (place) place.innerHTML = `${productionPrice(getTotalPrice().toFixed(2))}`
}