import {clearingFormFields} from "./validation";

// ------- удаление лесенкой элементов с экрана и HTML разметки -- promise
export const removeElements = function (elements, delay, animateClass, delayBetween = 300) {
    Array.from(elements).forEach( (e, i) => {
        setTimeout(() => e.classList.remove(animateClass), i * delay)
    })
    return new Promise((resolve) => {
        setTimeout(() => {
            elements.forEach(e => e.remove())
            resolve()
        }, elements.length * delay + delayBetween)
    })
}

// ------- удаление элемента с экрана и HTML разметки -- promise
export const removeElement = (element, animateClass, delay = 500) => {
    element.classList.toggle(animateClass)
    return new Promise((resolve) => {
        setTimeout(() => {
            element.remove()
            resolve()
        }, delay)
    })
}

// ------- анимированный показ элементов на экране уже полученных ранее из базы
export const showElements = function (elements, delay, animateClass) {
    elements.forEach( (e, i) => {
        setTimeout(() => e.classList.add(animateClass), i * delay + delay)
    })
}

// ------- жесткое(свойства через JS) сокрытие элемента с экрана и удаление его с потока HTML
export const hideElementHard = (element, delay = 1000) => {
    element.style.transition = delay + 'ms'
    element.style.opacity = '0'
    setTimeout(() => element.classList.add('is-delete'), delay)
}

// ------- сокрытие элемента с экрана и удаление его с потока HTML -- promise
export const hideElement = (element, animateActiveClass, delay) => {
    delay = delay || parseFloat(getComputedStyle(element)['transitionDuration']) * 1000
    element.classList.toggle(animateActiveClass)
    return new Promise((resolve) => {
        setTimeout(() => {
            element.classList.add('is-delete')
            resolve()
        }, delay)
    })
}

// ------- появление скрытого элемента на экране и в потоке HTML
export const showHiddenElement = (element, animateActiveClass) => {
    element.classList.remove('is-delete')
    setTimeout(() => element.classList.add(animateActiveClass), 50)
}