
// подсчет итоговой суммы товара в корзине с учетом выбранной валюты
export const getTotalPrice = () => {
    let totalSum = 0,
        localStore = JSON.parse(localStorage.getItem('cart')) || []
    localStore.forEach(el => {
        totalSum += parseInt(el.quantity) * el.priceNumber
    })
    return totalSum
}

// подсчет количества товаров в корзине
export const getTotalQuantity = () => {
    let totalQuantity = 0,
        localStore = JSON.parse(localStorage.getItem('cart')) || []
    localStore.forEach(el => {
        totalQuantity += parseInt(el.quantity)
    })
    return totalQuantity
}