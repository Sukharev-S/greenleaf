import {productionPrice, priceWithoutSpaces, printTotalPrice} from './priceActions'
import {getTotalPrice} from "./calculate";
import vars from "../_vars";

export const RUB_PER_USD = 74.19,
             EUR_PER_USD = 0.81,
             DEFAULT_CURRENCY = '₽'

export const setCurrency = (mutationRecords) => {
    let currency = 'USD'
    // let previousValue = mutationRecords[0].removedNodes[0].data
    const currencyMap = {
        USD: '$',
        EUR: '€',
        RUB: '₽'
    }
    if (document.querySelector('.js-select-currency .custom-select__top')) {
        currency = document.querySelector('.js-select-currency .custom-select__top').textContent
    }
    localStorage.setItem('currency', currencyMap[currency])
    document.querySelectorAll('.js-conventional-unit').forEach(el => {
        el.textContent = currencyMap[currency]
    })
    currencyConversion()
    // currencyConversion(currencyMap[previousValue])
}

export const currencyConversion = () => {
    let currency = localStorage.getItem('currency') || DEFAULT_CURRENCY
    document.querySelectorAll('.js-conventional-unit').forEach(el => {
        if (el.nextElementSibling.dataset.usdValue) {
            let usdValue = priceWithoutSpaces(el.nextElementSibling.dataset.usdValue)
            if (currency === '₽') el.nextElementSibling.textContent = productionPrice((usdValue * RUB_PER_USD).toFixed(2))
            if (currency === '€') el.nextElementSibling.textContent = productionPrice((usdValue * EUR_PER_USD).toFixed(2))
            if (currency === '$') el.nextElementSibling.textContent = productionPrice(usdValue)
        }
    })
    const cartModalTotal = document.querySelector('.cart-modal__counter')
    printTotalPrice(cartModalTotal)

    if (vars.$cartContent) {
        const subtotalSum = vars.$cartContent.querySelectorAll('.check__subtotal-sum'),
            totalSum = vars.$cartContent.querySelectorAll('.check__total-sum'),
            shippingSum = vars.$cartContent.querySelector('.check__shipping-sum')
        let shippingSumValue = parseFloat(shippingSum.textContent) || 1 ? 0 : parseFloat(shippingSum.textContent).toFixed(2)
        subtotalSum.forEach(el => printTotalPrice(el))
        totalSum.forEach(el => el.textContent = productionPrice((getTotalPrice() + shippingSumValue).toFixed(2)))
    }
}

// export const getCurrencyFactor = () => {
//     let currency = localStorage.getItem('currency') || DEFAULT_CURRENCY
//     let currencyFactor = 1
//     if (currency === '€') currencyFactor = EUR_PER_USD  //  0.81
//     if (currency === '₽') currencyFactor = RUB_PER_USD  //  74.19
//     return currencyFactor
// }