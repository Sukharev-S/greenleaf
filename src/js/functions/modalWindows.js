import vars from "../_vars";
import {clearingFormFields} from "./validation";

let arrayModalWindows = [
    [vars.$navModal, 'nav-modal--visible'],
    [vars.$authorizationModal, 'authorization-modal--visible'],
    [vars.$cartModal, 'cart-modal--visible'],
    [vars.$searchModal, 'search-modal--visible'],
    [vars.$policyModal, 'policy-modal--visible'],
    [vars.$imageModal, 'image-modal--visible'],
]

// -------  подсчет открытых модальных окон
const getActiveModalWindowsCount = function() {
    let countActiveWindows = 0
    arrayModalWindows.forEach(window => {
        if (window[0].classList.contains(window[1])) countActiveWindows++
    })
    return countActiveWindows
}

// -------  общая функция открытия модального окна с расчетом ширины скролла при body.overflow
export const modalWindowOn = function(element, animateClass) {
    let bodyWidthBefore = vars.$body.clientWidth
    element.classList.add(animateClass)
    if (getActiveModalWindowsCount() === 1) {
        vars.$overlay.classList.add('overlay--visible')
        vars.$body.style.overflow = 'hidden'
        vars.$body.style.paddingRight = vars.$body.clientWidth - bodyWidthBefore + 'px'
        vars.$header.style.paddingRight = vars.$body.style.paddingRight
        if (vars.$freeDelivery) {
            vars.$freeDelivery.style.paddingRight = vars.$body.style.paddingRight
            vars.$freeDeliveryBtn.style.marginRight = vars.$body.style.paddingRight
        }
    }
}

// -------  общая функция закрытия модального окна с расчетом ширины скролла при body.overflow
const modalWindowOff = function(element, animateClass) {
    element.classList.remove(animateClass)
    if (getActiveModalWindowsCount() === 0) {
        vars.$overlay.classList.remove('overlay--visible')
        setTimeout(() => {  // для устранения рывков при удалении паддингов
            vars.$body.style.overflow = 'visible'
            vars.$body.style.paddingRight = '0'
            vars.$header.style.paddingRight = '0'
            if (vars.$freeDelivery) {
                vars.$freeDelivery.style.paddingRight = '0'
                vars.$freeDeliveryBtn.style.marginRight = '0'
            }
        }, 100)
    }
}

// -------  функция для обработчиков закрытия модальных окон
export const closeModalWindows = function() {
    if (vars.$policyModal.classList.contains('policy-modal--visible')) {
        modalWindowOff(vars.$policyModal, 'policy-modal--visible')
    } else {
        arrayModalWindows.forEach(window => {
            if (window[0].classList.contains(window[1])) {
                modalWindowOff(window[0], window[1])
                let forms = window[0].querySelectorAll('form')
                if (forms) forms.forEach(form => clearingFormFields(form))
            }
        })
    }
    vars.$searchModal.querySelector('.form-search__results-container')
        .classList.remove('form-search__results-container--visible')
}

// -------  обработчик клика на overlay (для модальных окон)
vars.$overlay.addEventListener('click', e => {
    closeModalWindows()
})

// -------  обработчик закрытия модальных окон по нажатию ESCAPE
vars.$body.addEventListener('keydown', e => {
    if (e.code  === 'Escape') {
        closeModalWindows()
    }
})