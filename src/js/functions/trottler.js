let timeout
export const throttler = function(func, delay) {
    // ожидание выполнения кода пока в очереди есть задача
    if (!timeout) {
        timeout = setTimeout(function () {
            timeout = null
            func()
        }, delay)
    }
}