const validateEmail = (field) => {
    let email = field.value.trim()
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
}

const validateName = (field) => {
    let name = field.value.trim()
    const re = /^[a-zёЁа-я\-]+$/
    return re.test(String(name).toLowerCase())
}

const validateFullName = (field) => {
    let name = field.value.trim()
    const re = /^[a-zёЁа-я\- ]+$/
    return re.test(String(name).toLowerCase())
}

const validatePhone = (field) => {
    let phone = field.value.trim()
    // const re = /^((\+?7|8)[ \-]?)?([ \-])?(\d{6}|(\d{3}([ \-])?\d{3})?[\- ]?\d{2}[\- ]?\d{2})$/   до подключения inputMask
    const re = /^(\+7 \(\d{3}\) \d{3} \d{2}-\d{2})$/
    return re.test(String(phone).toLowerCase())
}

const validatePostCode = (field) => {
    let zipCode = field.value.trim()
    const re = /^\d{6}$/
    return re.test(String(zipCode).toLowerCase())
}

// (?=.*[0-9]) - строка содержит хотя бы одно число
// (?=.*[a-zA-Z]) - строка содержит хотя бы одну латинскую букву
// [0-9a-zA-Z!@#$%^& *]{8,} - строка состоит не менее, чем из 8 символов
const validatePassword = (field) => {
    let password = field.value.trim()
    const re = /(?=.*[a-zA-Z])(?=.*[0-9])[0-9a-zA-Z_\W *]{8,}/
    return re.test(String(password).toLowerCase())
}

const validateConfirmPassword = (field) => {
    const newPassField = field.closest('form').querySelector('input[name="new_password"]')
    let password = field.value.trim()
    const re = /(?=.*[a-zA-Z])(?=.*[0-9])[0-9a-zA-Z_\W *]{8,}/
    return re.test(String(password).toLowerCase()) && password === newPassField.value
}

const validateUsername = (field) => {
    let password = field.value.trim()
    const re = /^[a-z0-9\-_]+$/
    return re.test(String(password).toLowerCase())
}

const validateStreet = (field) => {
    let street = field.value.trim()
    const re = /^[a-z0-9а-яёЁ\-_\/.: \\,]{8,}$/
    return re.test(String(street).toLowerCase())
}

const validateCountry = (field) => {
    let name = field.value.trim()
    const re = /^[a-zа-яёЁ\- ]{4,}$/
    return re.test(String(name).toLowerCase())
}

const validateCity = (field) => {
    let name = field.value.trim()
    const re = /^[a-zа-яёЁ\-. ]{4,}$/
    return re.test(String(name).toLowerCase())
}

const validateTextArea = (field) => {
    let text = field.value.trim()
    const re = /(.+[\s].+)/
    return re.test(String(text).toLowerCase()) && text.length >= 15
}

// --- визуализация поля на невалидность ввода
export const validateShowNot = (field) => {
    let okIcon, NotOkIcon
    if (field.closest('.form__item')) {
        okIcon = field.closest('.form__item').querySelector('.form-field__ok')
        NotOkIcon = field.closest('.form__item').querySelector('.form-field__not-ok')
    }
    field.classList.remove('success-field')
    field.classList.add('error-field')
    if (NotOkIcon && okIcon) {
        NotOkIcon.classList.remove('is-reduce')
        okIcon.classList.add('is-reduce')
    }
}

// --- визуализация поля на успешную валидность ввода
export const validateShowOk = (field) => {
    let okIcon, NotOkIcon
    if (field.closest('.form__item')) {
        okIcon = field.closest('.form__item').querySelector('.form-field__ok')
        NotOkIcon = field.closest('.form__item').querySelector('.form-field__not-ok')
    }
    field.classList.remove('error-field')
    field.classList.add('success-field')
    if (NotOkIcon && okIcon) {
        NotOkIcon.classList.add('is-reduce')
        okIcon.classList.remove('is-reduce')
    }
}

// --- проверяет валидность поля и подставляет нужные классы. Только для визуализации
export const validateShow = (field) => {
    isValidate(field) ? validateShowOk(field) : validateShowNot(field)
}

// --- сброс и очищение всех полей формы от визуальных классов валидации
export const clearingFormFields = (form) => {
    let formFields = form.querySelectorAll('.form-field'),
        okIcon,
        NotOkIcon

    formFields.forEach(field => {
        if (field.closest('.form__item')) {
            okIcon = field.closest('.form__item').querySelector('.form-field__ok')
            NotOkIcon = field.closest('.form__item').querySelector('.form-field__not-ok')
        }
        field.classList.remove('error-field')
        field.classList.remove('success-field')
        if (NotOkIcon && okIcon) {
            NotOkIcon.classList.add('is-reduce')
            okIcon.classList.add('is-reduce')
        }
    })

    form.reset()
}

// --- валидация поля
export const isValidate = (field) => {
    if (field.value.length > 1) {
        if (field.name === 'country') return validateCountry(field)
        if (field.name === 'city') return validateCity(field)
        if (field.name === 'street') return validateStreet(field)
        if (field.name === 'username') return validateUsername(field)
        if (field.name === 'password' || field.name === 'new_password' || field.name === 'current_password') return validatePassword(field)
        if (field.name === 'confirm_password') return validateConfirmPassword(field)
        if (field.name === 'postcode') return validatePostCode(field)
        if (field.name === 'phone') return validatePhone(field)
        if (field.name === 'email') return validateEmail(field)
        if (field.name === 'first_name' || field.name === 'last_name'
            || field.name === 'middle_name') return validateName(field)
        if (field.name === 'full_name') return validateFullName(field)
        if (field.name === 'text') return validateTextArea(field)
        return true
    } else return false
}