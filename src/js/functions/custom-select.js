import vars from '../_vars'
import {setCurrency} from './convert-currency'

// --- если существует кастомный селект
if (vars.$customSelect.length > 0) {
    vars.$customSelect.forEach(el => {
        el.addEventListener('click', e => {
            e.currentTarget.classList.toggle('custom-select--open')
            if (e.target.classList.contains('custom-select__item')) {
                e.currentTarget.querySelector('.custom-select__top').textContent = e.target.textContent
                e.currentTarget.classList.remove('custom-select--open')
            }
        })
        el.addEventListener('blur', e => {
            e.currentTarget.classList.remove('custom-select--open')
        })
    })
}

if (document.querySelector('.js-select-currency')) {
    let currency = localStorage.getItem('currency'),
        currencyLetter = 'USD',
        currencyObserver = new MutationObserver(setCurrency)
    const currencySelect = document.querySelector('.js-select-currency .custom-select__top')
    currencyObserver.observe(currencySelect, {
        childList: true, // наблюдать за непосредственными детьми
        characterDataOldValue: true
    })
    if (currency === '€') currencyLetter = 'EUR'
    if (currency === '₽') currencyLetter = 'RUB'
    currencySelect.textContent = currencyLetter
}