import './vendor/focus-visible.min.js'
import './components/header'
import './components/catalog/catalog-slider'
import './components/catalog/catalog-grid'
import './components/marketing'
import './functions/stepper'
import './functions/pagination'
import './functions/custom-select'
import './components/card/card-product'
import './components/card/card-related'
import './components/main-products'
import './components/mobile-filter'
import './components/modal/modal-nav'
import './components/modal/modal-cart'
import './components/modal/modal-search'
import './components/modal/modal-authorization'
import './components/modal/modal-policy'
import './components/modal/modal-image'
import './components/cart/cart'
import './components/footer'
import './components/free-delivery'
import './components/tab-navigation'
import './components/user-profile'
import './components/admin-panel'
import './components/faq'
import './components/company'
import './components/about'
import './components/callback-message'
import Inputmask from './vendor/inputmask.min'
import {validateShow} from "./functions/validation"
// import SimpleBar from 'simplebar'

let phoneFields = document.querySelectorAll('input[name="phone"]')

// ------- подключение и валидация полей телефонов
let im = new Inputmask("+7 (999) 999 99-99", {
    "onincomplete": (e) => {
        e.target.classList.add('error-field')
        e.target.classList.remove('success-field')
    },
    "oncomplete": (e) => {
        e.target.classList.add('success-field')
        e.target.classList.remove('error-field')
    },
});
im.mask(phoneFields);

// ------- визуализация валидации при вводе
document.querySelectorAll('.form-field').forEach(el => {
    el.addEventListener('input', e => {
        validateShow(e.target)
    })
})

// ------- визуализация валидации поля ИНДЕКС
// document.querySelectorAll('.form-field[name="postcode"]').forEach(field =>
//     field.addEventListener('input', e => {
        // let element = e.target
        // if (element.value.length > 5) validateShow(element)
//     })
// )

// ------- визуализация валидации полей на потерю фокуса
document.querySelectorAll('.form-field').forEach(field =>
    field.addEventListener('blur', e => {
        let element = e.target
        if (!element.closest('.authorization-modal') && !element.closest('.footer')
            && !element.closest('.form__edit-password') && !element.closest('.form-search__field'))
            validateShow(element)
    })
)

// -------  кастомный скролл в разделе описание товара на странице карточки товара
// if (document.querySelector('[data-bar_description]')) {
//     new SimpleBar(document.querySelector('.card-description__navigation'))
// }

// -------  кастомный скролл в блоке модальной карзины
// if (document.querySelector('[data-bar_cart_modal]')) {
//     new SimpleBar(document.querySelector('.cart-modal__list'))
// }

// -------  кастомный скролл во всех селектах
// if (document.querySelector('[data-bar_select]')) {
//     document.querySelectorAll('.custom-select__list').forEach(e => new SimpleBar(e))
// }

// -------  кастомный скролл в модальном окне поиска
// if (document.querySelector('[data-bar_search]')) {
//     new SimpleBar(document.querySelector('.form-search__results-inner'))
// }

// -------  плавный скрол на всех якорях
const anchors = document.querySelectorAll('a[href^="#"]')
for(let anchor of anchors) {
    anchor.addEventListener("click", function(e) {
        e.preventDefault() // Предотвратить стандартное поведение ссылок
        // Атрибут href у ссылки, если его нет то перейти к body (наверх не плавно)
        let goto = anchor.hasAttribute('href') ? anchor.getAttribute('href') : 'body'
        if (goto !== "#") {//{ goto = '#top'}
            if (document.querySelector(goto)) {
                // Плавная прокрутка до элемента с id = href у ссылки
                document.querySelector(goto).scrollIntoView({
                    behavior: "smooth",
                    block: "start"
                })
            }
        }
    })
}

// --- отключает нажатие на ENTER на всем документе каждой страницы
window.document.addEventListener('keypress', (e) => {
    if(e.key === 'Enter') {
        e.preventDefault();
        return false;
    }
})

// --- отключает перетаскивание на всем документе каждой страницы
document.body.ondragstart = () => false