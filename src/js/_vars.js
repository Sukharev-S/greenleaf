export default {
  $window: window,
  $document: document,
  $html: document.documentElement,
  $body: document.body,
  $header: document.querySelector('.header'),
  $headerContainer: document.querySelector('.header__container'),
  $overlay: document.querySelector('.overlay'),
  $bannerSlider: document.querySelector('.banner-slider'),
  $marketing: document.querySelector('.marketing'),
  $burger: document.querySelector('.burger'),
  $nav: document.querySelector('.nav'),
  $cart: document.querySelector('.cart'),
  $search: document.querySelector('.shop-nav__link--search'),
  $shopNavUser: document.querySelector('.js-shop-nav__user'),
  $authorization: document.querySelector('.shop-nav__link--user'),
  $customSelect: document.querySelectorAll('.custom-select'),
  $customPagination: document.querySelector('.custom-pagination'),
  $stepper: document.querySelector('.stepper'),
  $freeDelivery: document.querySelector('.free-delivery'),
  $freeDeliveryBtn: document.querySelector('.free-delivery__btn'),
  $footer: document.querySelector('.footer'),
  $tabContent: document.querySelector('.tab-content'),
  $tabNavigationList: document.querySelector('.tab-navigation__list'),
  $tabNavigationLinks: document.querySelectorAll('.tab-navigation__link'),
  $adminPanel: document.querySelector('.admin-panel'),
  $loader: document.querySelector('.js-global-loader'),

//-- модальные окна
  $navModal: document.querySelector('.nav-modal'),
  $authorizationModal: document.querySelector('.authorization-modal'),
  $policyModal: document.querySelector('.policy-modal'),
  $cartModal: document.querySelector('.cart-modal'),
  $searchModal: document.querySelector('.search-modal'),
  $imageModal: document.querySelector('.image-modal'),

//-- страница main.php
  $mainProducts: document.querySelector('.main-products'),
  $mainProductsList: document.querySelector('.main-products__list'),
  $mainProductsMore: document.querySelector('.main-products__more'),

//-- страница catalog.php
  $catalogSlider: document.querySelector('.hero-catalog__slider'),
  $catalogFiltersTop: document.querySelectorAll('.catalog-filter__top'),
  $catalogColumns: document.querySelector('.catalog-columns__list'),
  $catalogGridList: document.querySelector('.catalog-grid__list'),
  $catalogFilterList: document.querySelector('.catalog-filter__items'),
  $catalogChoice: document.querySelector('.catalog-choice'),
  $mobileFiltersOpen: document.querySelector('.catalog-mobile-filters'),
  $catalogFilters: document.querySelector('.catalog-filters'),

//-- страница card.php
  $cardInfo: document.querySelector('.card-info'),
  $cardSlider: document.querySelector('.card-slider'),
  $colorSelect: document.querySelector('.color-select'),
  $sizeSelect: document.querySelector('.size-select'),
  $cardDescription: document.querySelector('.card-description'),
  $cardDescriptionLink: document.querySelectorAll('.card-description__link'),
  $cardRelatedSlider: document.querySelector('.card-related__slider'),
  $cardRelatedList: document.querySelector('.card-related__list'),

//-- страница cart.php
  $cartContent: document.querySelector('.cart-content'),
  $cartNavigationList: document.querySelector('.cart-navigation__list'),
  $messageErrorList: document.querySelector('.messages__error-list'),

//-- страница profile.php
//   $profilePageContent: document.querySelector('.profile-page'),
//   $profileNavigationList: document.querySelector('.profile-navigation__list')

}